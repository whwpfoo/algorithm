public class LinkedListForOneWay {
    static class Node {
        int data;
        Node next;
    }

    Node header;

    LinkedListForOneWay() {
        header = new Node();
    }

    void append(int data) {
        Node end = new Node();
        end.data = data;
        Node curr = header;

        while (curr.next != null) {
            curr = curr.next;
        }
        curr.next = end;
    }

    void delete(int data) {
        Node curr = header;
        while (curr.next != null) {
            if (curr.next.data == data) {
                header.next = curr.next.next;
            } else {
                curr = curr.next;
            }
        }
    }

    void print() {
        Node curr = header.next;
        while (curr.next != null) {
            System.out.println(curr.data);
            curr = curr.next;
        }
        System.out.println(curr.data);
    }

    void removeDuplicateData() { // 중복된 값이 있는 노드가 있으면 지워주는 메소드
        Node curr = header;
        while (curr != null && curr.next != null) {
            Node runner = curr;

            while (runner.next != null) {
                if (curr.data == runner.next.data) {
                    runner.next = runner.next.next;
                } else {
                    runner = runner.next;
                }
            }

            curr = curr.next;
        }
    }

    Node kTolast(Node first, Integer k) { // 뒤에서 k번째 노드를 반환하는 함수
        Node curr = first;
        int total = 1;

        while (curr.next != null) {
            total++;
            curr = curr.next;
        }
        curr = first;

        for (int i = 1; i < total - k + 1; i++) {
            curr = curr.next;
        }

        return  curr;
    }



    public static void main(String[] args) {
        LinkedListForOneWay ll = new LinkedListForOneWay();
//        ll.append(1);
//        ll.append(2);
//        ll.append(3);
//        ll.append(4);
//        ll.append(5);
//        ll.print();
//
//        ll.delete(1);
//        ll.delete(2);
//
//        ll.print();
        ll.append(1);
        ll.append(1);
        ll.append(1);
        ll.append(1);
        ll.append(1);
        ll.append(1);
        ll.append(1);
        ll.append(1);
        ll.removeDuplicateData();
        ll.print();
    }
}
