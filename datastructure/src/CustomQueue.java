import java.util.NoSuchElementException;

/**
 * FIFO (First in First out)
 */
public class CustomQueue<T> {
    class Node<T> {
        private T data;
        private Node<T> next;

        public Node(T data) {
            this.data = data;
        }
    }

    private Node<T> first;
    private Node<T> last;

    public void add(T item) {
        Node<T> t = new Node<>(item);

        if (last != null) {
            last.next = t;
        }
        last = t;

        if (first == null) {
            first = last;
        }
    }

    public T remove() {
        if (first == null) {
            throw new NoSuchElementException();
        }

        T data = first.data;
        first = first.next;

        if (first == null) {
            last = null;
        }

        return data;
    }

    public T peek() {
        if (first == null) {
            throw new NoSuchElementException();
        }

        return first.data;
    }

    public boolean isEmpty() {
        return first == null;
    }
}

class Test {
    public static void main(String[] args) {
        CustomQueue<String> cq = new CustomQueue<>();

        for (int i = 0; i < 100; i++) {
            cq.add("" + i);
        }

        while (!cq.isEmpty()) {
            System.out.println(cq.remove());
        }
    }
}
