public class LinkedNodeForOneWay {

    public static void main(String[] args) {
        Node<Integer> head = new Node<Integer>(1);
        head.add(2);
        head.add(3);
        head.add(4);
        head.add(5);
        head.print();
        head.delete(3);
        head.print();
    }

    public static class Node<T> {
        public T data;
        public Node<T> next; // 다음

        public Node(T data) {
            this.data = data;
        }

        public void add(T data) {
            Node next = new Node(data);
            Node curr = this;

            while (curr.next != null) {
                curr = curr.next;
            }

            curr.next = next;
        }

        public void delete(T data) {
            Node curr = this;
            while (curr.next != null) {
                if (curr.next.data == data) {
                    Node n = curr.next.next;
                    curr.next = n;
                } else {
                    curr = curr.next;
                }
            }
        }

        public void print() {
            Node curr = this;
            while (curr.next != null) {
                System.out.println(curr.data);
                curr = curr.next;
            }
            System.out.println(curr.data);
        }
    }

}
