import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {
        BubbleSort bubbleSort = new BubbleSort();
        int[] array = {5, 3, 2, 4, 1, 10, 6, 8, 7, 9};

        bubbleSort.sort(array);

        System.out.println(Arrays.toString(array));

    }

    public void sort(int[] data) {
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data.length - 1 - i; j++) {
                if (data[j] > data[j + 1]) {
                    swap(data, j, j + 1);
                }
            }
        }
    }

    public void swap(int[] data, int source, int target) {
        int temp = data[source];
        data[source] = data[target];
        data[target] = temp;
    }
}
