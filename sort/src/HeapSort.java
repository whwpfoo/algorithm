import java.util.Arrays;

/**
 * 힙이란?
 * 최소 값 또는 최대 값을 빠르게 찾기위한
 * 완전 이진 트리를 기본으로한 자료구조.
 *
 * 최소 힙 / 최대 힙이 존재
 * 최소 힙: 가장 작은 값이 맨 위에 오도록.
 * 최대 힙: 가장 큰 값이 맨 위에 오도록.
 *
 */
public class HeapSort {
    public static void main(String[] args) {
        int[] arr = {7, 6, 5, 8, 3, 5, 9, 1, 6};
        int[] result = heapSort(arr);

        System.out.println(Arrays.toString(result));
    }

    public static int[] heapSort(int[] arr) {
        int arrLength = arr.length;

        // 먼저 힙 트리 구조를 생성
        for (int i = 1; i < arrLength; i++) {
            int j = i;

            do {
                int root = (j - 1) / 2;

                if (arr[root] < arr[j]) {
                    int temp = arr[root];
                    arr[root] = arr[j];
                    arr[j] = temp;
                }

                j = root;

            } while (j != 0);
        }

        // 크기를 줄여가며 반복적으로 힙 구성
        for (int i = arrLength - 1; i >= 0; i--) {
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;
            int root = 0;
            int j = 1;

            do {
                j = 2 * root + 1;

                if (arr[j] < arr[j + 1] && j < i - 1) {
                    j++;
                }

                if (arr[root] < arr[j] && j < i) {
                    int temp2 = arr[root];
                    arr[root] = arr[j];
                    arr[j] = temp2;
                }

                root = j;
            } while (j < i);
        }

        return arr;
    }
}
