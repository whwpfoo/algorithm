import java.util.Arrays;

public class QuickSort {

    public static void main(String[] args) {
        int[] data = {10, 1, 3, 4, 6, 5, 2, 9, 8, 7};

        QuickSort quickSort = new QuickSort();
        quickSort.quickSort(data, 0, data.length - 1);

        System.out.println(Arrays.toString(data));
    }

    public void quickSort(int[] data, int start, int end) {
        int pivot = partition(data, start, end);

        if (start < pivot - 1) {
            quickSort(data, start, pivot - 1);
        }
        if (pivot < end) {
            quickSort(data, pivot, end);
        }
    }

    public int partition(int[] data, int start, int end) {
        int left = start;
        int right = end;
        int pivot = data[(left + right) / 2];

        while (left <= right) {
            while (data[left] < pivot) {
                left++;
            }
            while (data[right] > pivot) {
                right--;
            }
            if (left <= right) {
                int temp = data[right];
                data[right] = data[left];
                data[left] = temp;
                left++;
                right--;
            }
        }
        return left;
    }
}
