import java.util.Arrays;

public class SelectionSort {
    public static void main(String[] args) {
        SelectionSort selection = new SelectionSort();
        int[] array = {5, 3, 2, 4, 1, 10, 6, 8, 7, 9};

        selection.sort(array);
        selection.selectionSort(array, 0);

        System.out.println(Arrays.toString(array));
    }

    /** 반복문을 이용한 방식 */
    public void sort(int[] data) {
        int dataSize = data.length;

        for (int i = 0; i < dataSize - 1; i++) {
            for (int j = i + 1; j < dataSize; j++) {
                if (data[i] > data[j]) {
                    swap(data, i, j);
                }
            }
        }
    }

    /** 재귀호출을 이용한 방식 */
    public void selectionSort(int[] arr, int start) {
        if (start < arr.length - 1) {
            int minValueIdx = start;

            for (int i = start + 1; i < arr.length; i++) {
                if (arr[i] < arr[minValueIdx]) {
                    minValueIdx = i;
                }
            }

            swap(arr, start, minValueIdx);
            selectionSort(arr, start + 1);
        }
    }

    public void swap(int[] data, int source, int target) {
        int temp = data[source];
        data[source] = data[target];
        data[target] = temp;
    }
}