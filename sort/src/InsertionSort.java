import java.util.Arrays;

public class InsertionSort {
    public static void main(String[] args) {
        InsertionSort insertionSort = new InsertionSort();
        int[] array = {5, 3, 2, 4, 1, 10, 6, 8, 7, 9};

        insertionSort.sort(array);

        System.out.println(Arrays.toString(array));

    }

    public void sort(int[] data) {
        for (int i = 1; i < data.length; i++) {
            int j = i;
            while (j > 0 && data[j - 1] > data[j]) {
                swap(data, j - 1, j);
                j--;
            }
        }
    }

    public void swap(int[] data, int source, int target) {
        int temp = data[source];
        data[source] = data[target];
        data[target] = temp;
    }
}
