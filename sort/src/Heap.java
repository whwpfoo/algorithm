// 최대 힙
// --> 부모 노드가 자식 노드보다 큰 자료구조
//      10
//     /  \
//    9    8
//   / \  / \
//  4   53   2
//
// 최소 힙
// --> 부모 노드가 자식 노드보다 작은 자료구조
//      0
//     / \
//    1   2
//   / \ / \
//  3  4 5  6
//

import java.util.*;

public class Heap {
    public static void main(String[] args) {
        int[] arr = {7, 6, 5, 8, 3, 5, 9, 1};

        System.out.println(Arrays.toString(arr));
        heapSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void heapSort(int[] arr) {
        for (int i = arr.length; i > 0; i--) {
            heapify(arr, i);
            swap(arr, 0, i - 1);
        }
    }

    public static void heapify(int[] arr, int length) {
        int lastIndex = length - 1;
        int parentIndex = getParent(lastIndex);

        while (parentIndex >= 0) {
            heapify(arr, parentIndex--, lastIndex);
        }
    }

    private static void heapify(int[] arr, int parentIndex, int lastIndex) {
        int left, right, max;

        if ((parentIndex * 2) + 1 <= lastIndex) { // 왼쪽 노드 기준으로 돌리기 위함
            left = parentIndex * 2 + 1;
            right = parentIndex * 2 + 2;

            if (arr[left] > arr[parentIndex]) { // 왼쪽 노드랑 먼저 비교
                swap(arr, left, parentIndex);
            }

            if (right <= lastIndex && arr[right] > arr[parentIndex]) { // 오른쪽 노드가 존재한다면 오른쪽 노드와도 비교
                swap(arr, right, parentIndex);
            }
        }
    }

    private static int getParent(int lastIndex) {
        return lastIndex / 2;
    }


    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
