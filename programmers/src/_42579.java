import java.util.*;
import java.util.stream.Collectors;

// https://programmers.co.kr/learn/courses/30/lessons/42579
public class _42579 {
    public static void main(String[] args) {
        String[] genres = {"classic", "pop", "classic", "classic", "pop"};
        int[] plays = {500, 600, 150, 800, 2500};
        _42579 obj = new _42579();
        System.out.println(Arrays.toString(obj.solution(genres, plays)));
    }
    // 각 장르에서 노래를 2개 씩 추출 한다
    //  장르에서 재생횟수가 가장 많이 된 노래를 정렬
    //  재생횟수가 같다면 고유번호가 낮은 순으로 정렬 (오름차순)
    public int[] solution(String[] genres, int[] plays) {
        Map<String, Album> map = new HashMap<>();

        int playLength = plays.length;
        for (int i = 0; i < playLength; i++) {
            Album album = map.getOrDefault(genres[i], new Album(genres[i]));
            album.addMusic(i, plays[i]);
            map.put(genres[i], album);
        }

        List<Album> list = map.values().stream()
                .sorted((o1, o2) -> o2.total - o1.total)
                .collect(Collectors.toList());

        List<Integer> result = new ArrayList<>();
        for (Album album : list) {
            album.sortMusic();
            int size = album.musics.size();
            result.add(album.musics.get(size - 1)[0]);
            if (size >= 2) {
                result.add(album.musics.get(size - 2)[0]);
            }
        }

        return result.stream().mapToInt(Integer::intValue).toArray();
    }

    class Album {
        String genre;
        int total;
        List<int[]> musics = new ArrayList<>();

        public Album(String genre) {
            this.genre = genre;
        }

        public void addMusic(int id, int play) {
            int[] info = new int[2];
            info[0] = id;
            info[1] = play;

            this.musics.add(info);
            this.total += play;
        }

        public void sortMusic() {
            Collections.sort(musics, (o1, o2) -> {
                if (o1[1] == o2[1]) {
                    return o2[0] - o1[0];
                } else {
                    return o1[1] - o2[1];
                }
            });
        }
    }
}
