import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * 키패드 누르기: https://programmers.co.kr/learn/courses/30/lessons/67256
 * 맨 처음 왼손 엄지손가락은 * 키패드에 오른손 엄지손가락은 # 키패드 위치에서 시작하며, 엄지손가락을 사용하는 규칙은 다음과 같습니다.
 * 엄지손가락은 상하좌우 4가지 방향으로만 이동할 수 있으며 키패드 이동 한 칸은 거리로 1에 해당합니다.
 * 왼쪽 열의 3개의 숫자 1, 4, 7을 입력할 때는 왼손 엄지손가락을 사용합니다.
 * 오른쪽 열의 3개의 숫자 3, 6, 9를 입력할 때는 오른손 엄지손가락을 사용합니다.
 * 가운데 열의 4개의 숫자 2, 5, 8, 0을 입력할 때는 두 엄지손가락의 현재 키패드의 위치에서 더 가까운 엄지손가락을 사용합니다.
 * 4-1. 만약 두 엄지손가락의 거리가 같다면, 오른손잡이는 오른손 엄지손가락, 왼손잡이는 왼손 엄지손가락을 사용합니다.
 */
public class _67256 {

    public static void main(String[] args) {
        int[] arr = {1, 3, 4, 5, 8, 2, 1, 4, 5, 9, 5};
        String type = "right";

        _67256 obj = new _67256();
        String result = obj.solution(arr, type);

        System.out.println(result);
    }

    public String solution(int[] numbers, String hand) {
        StringBuilder sb = new StringBuilder();
        Map<Integer, Integer> distanceMap = new HashMap<>();

        IntStream.range(0, 10)
                .forEach(idx -> {
                    if (idx == 0) distanceMap.put(idx, 0);
                    else if (idx == 1 || idx == 2 || idx == 3) distanceMap.put(idx, 3);
                    else if (idx == 4 || idx == 5 || idx == 6) distanceMap.put(idx, 2);
                    else if (idx == 7 || idx == 8 || idx == 9) distanceMap.put(idx, 1);
                });
        distanceMap.put(-1, 0);

        int leftPos = -1;
        int rightPos = -1;

        for (int i = 0; i < numbers.length; i++) {
            int move = numbers[i];

            if (move == 1 || move == 4 || move == 7) {
                leftPos = move;
                sb.append("L");
            } else if (move == 9 || move == 6 || move == 3) {
                rightPos = move;
                sb.append("R");
            } else {
                int leftDistance = 0;

                if (leftPos != 1 && leftPos != 4 && leftPos != 7 && leftPos != -1) {
                    leftDistance = Math.abs(distanceMap.get(move) - distanceMap.get(leftPos));
                } else {
                    leftDistance = Math.abs(distanceMap.get(move) - distanceMap.get(leftPos)) + 1;
                }

                int rightDistance = 0;

                if (rightPos != 3 && rightPos != 6 && rightPos != 9 && rightPos != -1) {
                    rightDistance = Math.abs(distanceMap.get(move) - distanceMap.get(rightPos));
                } else {
                    rightDistance = Math.abs(distanceMap.get(move) - distanceMap.get(rightPos)) + 1;
                }

                if (leftDistance < rightDistance) {
                    sb.append("L");
                    leftPos = move;
                } else if (leftDistance > rightDistance) {
                    sb.append("R");
                    rightPos = move;
                } else {
                    if ("left".equalsIgnoreCase(hand) == false) {
                        sb.append("R");
                        rightPos = move;
                    } else {
                        sb.append("L");
                        leftPos = move;
                    }
                }
            }
        }

        return sb.toString();
    }
}
