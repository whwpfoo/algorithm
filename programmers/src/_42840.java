import java.util.*;

/**
 * 모의고사: https://programmers.co.kr/learn/courses/30/lessons/42840
 */
public class _42840 {
    public static void main(String[] args) {
        int[] answer = {1,2,3,4,5};
        int[] result = solution(answer);
        System.out.println(Arrays.toString(result));
    }

    public static int[] solution(int[] answers) {
        int[] firstStudent = {1, 2, 3, 4, 5};
        int[] secondStudent = {2, 1, 2, 3, 2, 4, 2, 5};
        int[] thirdStudent = {3, 3, 1, 1, 2, 2, 4, 4, 5, 5};
        int[] scoreArr = new int[3];
        int fStudentScore = getScore(firstStudent, answers);
        int sStudentScore = getScore(secondStudent, answers);
        int tStudentScore = getScore(thirdStudent, answers);

        scoreArr[0] = fStudentScore;
        scoreArr[1] = sStudentScore;
        scoreArr[2] = tStudentScore;

        int maxScore = 0;

        for (int i = 0; i < scoreArr.length; i++) {
            maxScore = Math.max(maxScore, scoreArr[i]);
        }

        List<Integer> answerList = new ArrayList<>();

        for (int i = 0; i < scoreArr.length; i++) {
            if (maxScore == scoreArr[i]) answerList.add(i + 1);
        }

        return answerList.stream().mapToInt(Integer::valueOf).toArray();
    }

    public static int getScore(int[] studentAnswer, int[] answers) {
        Queue<Integer> answerQueue = new LinkedList<>();
        int totalScore = 0;

        for (Integer num : studentAnswer) {
            answerQueue.add(num);
        }

        for (int answer : answers) {
            int tempAnswer = answerQueue.poll();

            if (answer == tempAnswer) {
                totalScore++;
            }

            answerQueue.add(tempAnswer);
        }

        return totalScore;
    }
}
