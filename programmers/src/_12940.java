/**
 * 최대공약수와 최소공배수: https://programmers.co.kr/learn/courses/30/lessons/12940
 */
public class _12940 {

    public static void main(String[] args) {

    }

    public int[] solution(int n, int m) {
        int[] answer = new int[2];
        int loopLength = n > m ? n : m;
        int maxDivisor = 0;

        for (int i = 1; i < loopLength; i++) {
            if (n % i == 0 && m % i == 0) maxDivisor = i;
        }

        answer[0] = maxDivisor;
        answer[1] = maxDivisor * (n / maxDivisor) * (m / maxDivisor);

        return answer;
    }
}
