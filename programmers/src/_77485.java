// https://programmers.co.kr/learn/courses/30/lessons/77485 - 행렬 테두리 회전하기

import java.util.Arrays;

public class _77485 {
    public static void main(String[] args) {
        _77485 obj = new _77485();
        int rows = 6;
        int cols = 6;
        int[][] q = {{2,2,5,4},{3,3,6,6},{5,1,6,3}};
        int[] solution = obj.solution(rows, cols, q);
        System.out.println(Arrays.toString(solution));
    }

    // 2 <= rows <= 100, 2 <= columns <= 100
    public int[] solution(int rows, int columns, int[][] queries) {
        int[] answer = new int[queries.length];
        int[][] map = makeMap(rows, columns);

        for (int i = 0; i < queries.length; i++) {
            answer[i] = rotation(map,queries[i][0], queries[i][1], queries[i][2], queries[i][3]);
        }
        return answer;
    }

    public int[][] makeMap(int row, int col) {
        int[][] map = new int[row][col];
        int count = 1;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                map[i][j] = count;
                count++;
            }
        }
        return map;
    }

    public int rotation(int[][] map, int x1, int y1, int x2, int y2) {
        int rowStart = x1 - 1;
        int rowLimit = x2 - 1;
        int colStart = y1 - 1;
        int colLimit = y2 - 1;
        int[] first = new int[y2 - y1 + 1];

        int[] second = new int[x2 - x1 + 1];
        int[] third = new int[y2 - y1 + 1];
        int[] fourth = new int[x2 - x1 + 1];


        first[first.length - 1] = 999999;
        second[second.length - 1] = 999999;
        third[third.length - 1] = 999999;
        fourth[fourth.length - 1] = 999999;
        int index = 0;

        for (int i = colStart; i <= colLimit - 1; i++) {
            first[index++] = map[rowStart][i];
        }
        index = 0;

        for (int i = rowStart; i <= rowLimit - 1; i++) {
            second[index++] = map[i][colLimit];
        }
        index = 0;

        for (int i = colStart + 1; i <= colLimit; i++) {
            third[index++] = map[rowLimit][i];
        }
        index = 0;

        for (int i = rowStart + 1; i <= rowLimit; i++) {
            fourth[index++] = map[i][colStart];
        }
        index = 0;

        int minValue = Integer.MAX_VALUE;
        int a = 0;
        int b = 0;
        // 열 채워넣기
        for (int i = colStart; i <= colLimit; i++) {
            minValue = Math.min(minValue, Math.min(first[index], third[index]));
            if (i != colStart) {
                map[rowStart][i] = first[a++];
            }
            if (i != colLimit) {
                map[rowLimit][i] = third[b++];
            }
            index++;
        }
        index = 0;
        a = 0;
        b = 0;
        // 행 채워 넣기
        for (int i = rowStart; i <= rowLimit; i++) {
            minValue = Math.min(minValue, Math.min(second[index], fourth[index]));
            if (i != rowStart) {
                map[i][colLimit] = second[a++];
            }
            if (i != rowLimit) {
                map[i][colStart] = fourth[b++];
            }
            index++;
        }

        return minValue;
    }
}
