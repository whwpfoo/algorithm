/**
 * 점프와 순간 이동: https://programmers.co.kr/learn/courses/30/lessons/12980
 */
public class _12980 {
    public int solution(int n) {
        int ans = 0;

        while (n != 0) {
            if (n % 2 == 0) {
                n = n / 2;
            } else {
                n = n - 1;
                ans++;
            }
        }

        return ans;
    }

}
