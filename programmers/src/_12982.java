import java.util.Arrays;

/**
 * 예산: https://programmers.co.kr/learn/courses/30/lessons/12982
 */
public class _12982 {

    public static void main(String[] args) {
        /**
         *  1. 오름차순 정렬
         *  2. 인덱스 값과 예산비교
         *    - 인덱스 값이 예산보다 작거나 같으면 => 예산 - 인덱스 값
         */
//        [1,3,2,5,4]	9	3
        _12982 obj = new _12982();
        int[] d = {1, 3, 2, 5, 4};
        int budget = 9;

        int result = obj.solution(d, budget);

        System.out.println("result => " + result);

    }

    public int solution(int[] d, int budget) {
        int answer = 0;
        d = Arrays.stream(d).sorted().toArray();

        for (int requestMoney : d) {
            if (requestMoney <= budget) {
                budget = budget - requestMoney;
                answer++;
            }
        }

        return answer;
    }
}
