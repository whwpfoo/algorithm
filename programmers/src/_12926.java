/**
 * 시저 암호: https://programmers.co.kr/learn/courses/30/lessons/12926
 */
public class _12926 {

    public static void main(String[] args) {
        String foo = "     ";
        foo.chars().forEach(text -> System.out.println(text));
        StringBuilder sb = new StringBuilder();
        System.out.println(sb.toString());
    }

    public String solution(String s, int n) {
        StringBuilder sb = new StringBuilder();

        s.chars().forEach(text -> {
            if (isWhiteSpace(text)) {
                sb.append(" ");
            } else {
                int move = text + n;

                if (isUpperText(text)) { // 대문자인 경우
                    if (move > 90) move = move - 90 + 64;
                } else {
                    if (move > 122) move = move - 122 + 96;
                }

                sb.append((char)move);
            }
        });

        return sb.toString();
    }

    public static boolean isUpperText(int c) {
        return c >= 65 && c <= 90 ? true : false;
    }

    public static boolean isWhiteSpace(int c) {
        return c == ' ';
    }
}
