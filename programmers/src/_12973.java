import java.util.Stack;

/**
 * 짝지어 제거하기: https://programmers.co.kr/learn/courses/30/lessons/12973
 */
public class _12973 {
    static Stack<Integer> stack = new Stack<>();

    public static void main(String[] args) {
        String foo = "baabaa";
        solution(foo);
    }
    public static int solution(String s) {
        StringBuilder sb = new StringBuilder();
        sb.append(s);

        int length = sb.length() - 1;

        for (int i = 0; i < length; ) {
            String word = sb.substring(i, i + 2);

            if (word.charAt(0) == word.charAt(1)) {
                sb.delete(i, i + 2);
                i = 0;
                length = sb.length() - 1;
            } else {
                i++;
            }
        }

        return sb.length() == 0 ? 1 : 0;
    }

    public static int solution2(String s) {
        s.chars().forEach(c -> {
            if (!stack.isEmpty()) {
                if (c == stack.peek()) stack.pop();
                else stack.push(c);
            } else {
                stack.push(c);
            }
        });

        return stack.size() == 0 ? 1 : 0;
    }
}
