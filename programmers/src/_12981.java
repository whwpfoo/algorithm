import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * 영어 끝말잇기: https://programmers.co.kr/learn/courses/30/lessons/12981
 */
public class _12981 {
    public static void main(String[] args) {
        int n = 3;
        String[] words = new String[] {"tank", "kick", "know", "wheel", "land", "dream", "mother", "robot", "tank"};
        solution(n, words);
    }
    public static int[] solution(int n, String[] words) {
        Map<String, Integer> saveWordMap = new HashMap<>();
        Queue<String> queue = new LinkedList<>();
        int turn = 1;

        for (int i = 0; i < words.length; i++) {
            boolean isDuplicate = saveWordMap.get(words[i]) == null ? false : true;

            if (isDuplicate) {
                break;
            }

            if (queue.isEmpty() == false) {
                String word = queue.poll();
                char end = word.charAt(word.length() - 1);
                char start = words[i].charAt(0);

                if (end != start) {
                    break;
                }
            }

            saveWordMap.put(words[i], turn);
            queue.offer(words[i]);
            turn++;
        }

        if (turn - 1 == words.length) {
            return new int[] {0, 0};
        }

        return new int[] {(turn - 1) % n + 1, (turn - 1) / n + 1};
    }
}
