import java.util.HashMap;
import java.util.Map;

/**
 * 위장: https://programmers.co.kr/learn/courses/30/lessons/42578
 */
public class _42578 {
    public int solution(String[][] clothes) {
        Map<String, Integer> map = new HashMap<>();

        for (int i = 0; i < clothes.length; i++) {
            map.put(clothes[i][1], map.getOrDefault(clothes[i][1], 0) + 1);
        }

        return  map.values().stream().mapToInt(v -> v + 1).reduce((v1, v2) -> v1 * v2).getAsInt() - 1;
    }
}
