import java.util.*;

/**
 * 후보키: https://programmers.co.kr/learn/courses/30/lessons/42890
 * [        0      1      2     3
     * 0 ["100","ryan","music","2"],
     * 1 ["200","apeach","math","2"],
     * 2 ["300","tube","computer","3"],
     * 3 ["400","con","computer","4"],
     * 4 ["500","muzi","music","3"],
     * 5 ["600","apeach","music","2"]
 * ]
 */
public class _42890 {

    Comparator<Integer> comp = new Comparator<Integer>() {
        public int countBits(int n) {
            int ret = 0;

            while (n != 0) {
                if ((n & 1) != 0) ret++;
                n = n >> 1;
            }

            return ret;
        }

        @Override
        public int compare(Integer o1, Integer o2) {
            int x = countBits(o1), y = countBits(o2);
            if (x > y)
                return 1;
            else if (x < y)
                return -1;
            else
                return 0;
        }
    };


    public static boolean check(String[][] relation, int row, int col, int subset) {
        for (int a = 0; a < row - 1; a++) {
            for (int b = a + 1; b < row; b++) {
                boolean isSame = true;

                for (int k = 0; k < col; k++) {
                    if ((subset & 1 << k) == 0) continue;
                    if (relation[a][k].equals(relation[b][k]) == false) {
                        isSame = false;
                        break;
                    }
                }

                if (isSame) {
                    return false;
                }
            }
        }

        return true;
    }

    public int solution(String[][] relation) {
        int answer = 0;
        int rowCount = relation.length; // 행 길이
        int columnCount = relation[0].length; // 열 길이

        List<Integer> candidates = new LinkedList<>();

        for (int i = 1; i < 1 << columnCount; i++) {
            if (check(relation, rowCount, columnCount, i) == true) {
                candidates.add(i);
            }
        }

        Collections.sort(candidates, comp);

        while (candidates.size() != 0) {
            int n = candidates.remove(0);
            answer++;

            for (Iterator<Integer> it = candidates.iterator(); it.hasNext();) {
                int c = it.next();

                if ((n & c) == n) {
                    it.remove();
                }
            }
        }

        return answer;
    }
}
