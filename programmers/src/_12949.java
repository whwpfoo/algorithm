/**
 * 행렬의 곱셈: https://programmers.co.kr/learn/courses/30/lessons/12949
 */
public class _12949 {
    public static void main(String[] args) {

    }
    public int[][] solution(int[][] arr1, int[][] arr2) {
        int rowLeng = arr1.length;
        int colLeng = arr1[0].length;

        int[][] answer = new int[rowLeng][arr2[0].length];

        for (int row = 0; row < rowLeng; row++) {
            for (int i = 0; i < arr2[0].length; i++){
                int result = 0;

                for (int col = 0; col < colLeng; col++) {
                    result += arr1[row][col] * arr2[col][i];
                }

                answer[row][i] = result;
            }
        }

        return answer;
    }
}
