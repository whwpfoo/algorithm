import java.util.HashMap;
import java.util.Map;

// https://programmers.co.kr/learn/courses/30/lessons/42839?language=java - 소수찾기
public class _42839 {
    public static void main(String[] args) {
        _42839 o = new _42839();
        System.out.println(o.solution("321"));
    }

    private Map<Integer, Integer> primeMap = new HashMap<>();
    private boolean[] visit;

    public int solution(String numbers) {
        int numberLength = numbers.length();
        int[] nums = new int[numberLength];
        visit = new boolean[numberLength];

        for (int i = 0; i < numbers.length(); i++) {
            nums[i] = numbers.charAt(i) - '0';
        }

        combination(nums, new StringBuilder());

        return primeMap.keySet().size();
    }

    // 조합
    public void combination(int[] nums, StringBuilder sb) {
        System.out.println(sb);

        if (sb.length() > 0 && isPrime(Integer.parseInt(sb.toString()))) {
            primeMap.put(Integer.parseInt(sb.toString()),
                    primeMap.getOrDefault(sb.toString(), 0) + 1);
        }

        for (int i = 0; i < nums.length; i++) {
            if (visit[i] == false) {
                visit[i] = true;
                sb.append(nums[i]);
                combination(nums, sb);
                visit[i] = false;
                sb.deleteCharAt(sb.length() - 1);
            }
        }
    }

    // check prime
    public boolean isPrime(int num) {
        if (num == 0 || num == 1) {
            return false;
        }

        for(int i = 2; i * i <= num; i++){
            if (num % i == 0) return false;
        }

        return true;
    }
}
