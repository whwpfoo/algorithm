import java.util.Arrays;

/**
 * x만큼 간격이 있는 n개의 숫자: https://programmers.co.kr/learn/courses/30/lessons/12954
 */
public class _12954 {

    public static void main(String[] args) {
        _12954 obj = new _12954();

        int x = 2;
        int n = 5;
        long[] result = obj.solution(x, 5);

        System.out.println(Arrays.toString(result));
    }

    public long[] solution(int x, int n) {
        long[] answer = new long[n];

        for (int i = 0; i < n; i++) {
            if (i == 0) answer[i] = x;
            else answer[i] = answer[i - 1] + x;
        }

        return answer;
    }
}
