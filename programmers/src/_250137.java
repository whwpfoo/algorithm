/**
 * 붕대 감기
 * https://school.programmers.co.kr/learn/courses/30/lessons/250137
 */
public class _250137 {
    // bandage:[5, 1, 5] [시전 시간, 초당 회복량, 추가 회복량]
    // health: 30
    // attacks: [[2, 10], [9, 15], [10, 5], [11, 5]]
    public int solution(int[] bandage, int health, int[][] attacks) {
        int HP = health;
        int time = attacks[attacks.length - 1][0];
        int[] damage = new int[1001];
        for (int[] attack : attacks) {
            damage[attack[0]] = attack[1];
        }

        int bonus = 0;
        for (int i = 1; i <= time; i++) {
            if (HP <= 0)
                break;

            if (damage[i] > 0) {
                HP -= damage[i];
                bonus = 0;
            } else {
                HP += bandage[1];
                if (bonus == bandage[0]) {
                    HP += bandage[2];
                    bonus = 0;
                } else {
                    bonus++;
                }
            }

            if (HP > health)
                HP = health;
        }

        return HP <= 0 ? -1 : HP;
    }
}
