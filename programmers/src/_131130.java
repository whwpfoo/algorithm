import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 혼자 놀기의 달인
 * https://school.programmers.co.kr/learn/courses/30/lessons/131130
 */
public class _131130 {
    public static void main(String[] args) {
        _131130 obj = new _131130();
        int result = obj.solution(new int[]{8, 6, 3, 7, 2, 5, 1, 4});
        System.out.println(result);
    }

    public int solution(int[] cards) {
        List<Integer> group = new ArrayList<>();
        boolean[] opened = new boolean[cards.length + 1];
        int idx = cards[0], count = 0;

        while (true) {
            if (opened[idx]) {
                group.add(count);
                count = 0;
                int open = 1;
                while (open <= cards.length && opened[open]) open++;

                if (open > cards.length) {
                    break;
                } else {
                    idx = cards[open - 1];
                    continue;
                }
            }
            opened[idx] = true;
            count++;
            idx = cards[idx - 1];
        }

        if (group.size() == 1)
            return 0;
        Collections.sort(group);

        return group.get(group.size() - 1) * group.get(group.size() - 2);
    }
}
