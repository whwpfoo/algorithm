/**
 * 자릿수 더하기: https://programmers.co.kr/learn/courses/30/lessons/12931
 */
public class _12931 {

    public int solution(int n) {
        return String.valueOf(n).chars().map(Character::getNumericValue).sum();
    }
}
