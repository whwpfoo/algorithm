import java.util.*;

public class _84325 {
    public static void main(String[] args) {

    }

    public String solution(String[] table, String[] languages, int[] preference) {
        List<JobPosition> positionList = new ArrayList<>();

        for (String s : table) {
            String name = s.substring(0, s.indexOf(" "));
            String[] langs = s.substring(s.indexOf(" ") + 1).split(" ");
            positionList.add(new JobPosition(name, langs));
        }

        for (JobPosition jobPosition : positionList) {
            jobPosition.calcPreferenceScore(languages, preference);
        }

        Collections.sort(positionList);

        return positionList.get(0).name;
    }

    class JobPosition implements Comparable<JobPosition> {
        String name;
        int preferenceScore;
        Map<String, Integer> preferenceLanguageMap = new HashMap<>();

        public JobPosition(String name, String[] languages) {
            this.name = name;

            for (int i = 0; i < languages.length; i++) {
                String language = languages[i];
                int score = 5 - i;
                this.preferenceLanguageMap.put(language, score);
            }
        }

        public void calcPreferenceScore(String[] languages, int[] preference) {
            int sum = 0;
            int size = languages.length;

            for (int i = 0; i < size; i++) {
                if (preferenceLanguageMap.containsKey(languages[i])) {
                    int personalScore = preferenceLanguageMap.get(languages[i]);
                    int publicScore = preference[i];
                    sum += personalScore * publicScore;
                }
            }
            this.preferenceScore = sum;
        }

        @Override
        public int compareTo(JobPosition o) {
            if (this.preferenceScore == o.preferenceScore) {
                return this.name.compareTo(o.name);
            }
            return Integer.compare(o.preferenceScore, this.preferenceScore);
        }
    }
}
