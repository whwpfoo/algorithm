import java.util.*;

// https://programmers.co.kr/learn/courses/30/lessons/86971
public class _86971 {
    public static void main(String[] args) {
        _86971 obj = new _86971();
        int n = 9;
        int[][] wires = {{1,3},{2,3},{3,4},{4,5},{4,6},{4,7},{7,8},{7,9}};

        obj.solution(n, wires);
    }

    public int solution(int n, int[][] wires) {
        ArrayList<Integer>[] arr = new ArrayList[n + 1];

        for (int i = 1; i <= n; i++) {
            arr[i] = new ArrayList<>();
        }

        for (int i = 0; i < wires.length; i++) {
            int from = wires[i][0], to = wires[i][1];
            arr[from].add(to);
            arr[to].add(from);
        }

        int result = n;

        for (int[] wire : wires) {
            int from = wire[0], to = wire[1];
            int fromCnt = findTower(arr, arr[from], from, to);
            int toCnt = findTower(arr, arr[to], to, from);
            result = Math.min(result, Math.abs(fromCnt - toCnt));
        }

        return result;
    }

    public int findTower(ArrayList[] towers, List<Integer> t, int s, int r) {
        Set<Integer> visit = new HashSet<>();
        Queue<Integer> q = new LinkedList<>();

        visit.add(s);

        for (int tt : t) {
            if (tt == r) continue;
            q.offer(tt);
            visit.add(tt);
        }
        while (q.isEmpty() == false) {
            int tt = q.poll();
            for (int ttt : (ArrayList<Integer>)towers[tt]) {
                if (visit.contains(ttt)) continue;
                q.offer(ttt);
                visit.add(ttt);
            }
        }
        return visit.size();
    }
}
