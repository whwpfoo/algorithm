import java.util.HashMap;
import java.util.Map;

/**
 * 폰켓몬: https://programmers.co.kr/learn/courses/30/lessons/1845
 */
public class _1845 {
    public int solution(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i],nums[i]);
        }

        int type = map.size(); // 실제 타입의 수
        int choice = nums.length / 2; // 담을수 있는 횟수

        if (choice <= type) return choice;

        return type;
    }
}
