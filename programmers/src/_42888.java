import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 오픈채팅방: https://programmers.co.kr/learn/courses/30/lessons/42888
 * [
 * "Enter uid1234 Muzi",
 * "Enter uid4567 Prodo",
 * "Leave uid1234",
 * "Enter uid1234 Prodo",
 * "Change uid4567 Ryan"
 * ]
 */
public class _42888 {

    public String[] solution(String[] record) {
        Map<String, String> memberIdAndNameMap = new HashMap<>();

        for (int i = 0; i < record.length; i++) {
            String[] member = record[i].split(" ");

            if (isLeave(member[0]) == false) {
                memberIdAndNameMap.put(member[1], member[2]);
            }
        }

        List<String> answerList = new ArrayList<>();

        for (int i = 0; i < record.length; i++) {
            String[] member = record[i].split(" ");

            if (isChange(member[0]) == false) {
                String membername = memberIdAndNameMap.get(member[1]);
                String behavior =  behavior2String(member[0]);
                answerList.add(membername + behavior);
            }
        }

        return answerList.toArray(new String[answerList.size()]);
    }

    public boolean isLeave(String behavior) {
        return "Leave".equalsIgnoreCase(behavior) ? true : false;
    }

    public boolean isChange(String behavior) {
        return "Change".equalsIgnoreCase(behavior) ? true : false;
    }

    public String behavior2String(String behavior) {
        String result = null;

        switch (behavior) {
            case "Enter":
                result =  "님이 들어왔습니다.";
                break;
            case "Leave":
                result =  "님이 나갔습니다.";
                break;
        }

        return result;
    }
}
