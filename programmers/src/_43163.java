// https://programmers.co.kr/learn/courses/30/lessons/43163
public class _43163 {
    public int solution(String begin, String target, String[] words) {
        if (!isExistTarget(words, target)) {
            return 0;
        } else {
            return convert(begin, target, words, 0, new boolean[words.length]);
        }
    }

    public int convert(String begin, String target, String[] words,
                       int result, boolean[] visited) {
        if (begin.equals(target)) {
            return result;
        }
        int count = Integer.MAX_VALUE;
        for (int i = 0; i < words.length; i++) {
            if (visited[i] == false) {
                visited[i] = true;
                if (canConvert(begin, words[i])) {
                    count = Math.min(convert(words[i], target, words, result + 1, visited), count);
                }
                visited[i] = false;
            }
        }
        return count;
    }

    // 문자열을 이루는 문자가 1개 이하로 달라야 함
    public boolean canConvert(String begin, String word) {
        int diffCount = 0;
        int length = begin.length();
        for (int i = 0; i < length; i++) {
            if (begin.charAt(i) != word.charAt(i)) diffCount++;
        }
        return diffCount == 1;
    }

    // target이 words에 존재하지 않는경우
    public boolean isExistTarget(String[] words, String target) {
        for (String word : words) {
            if (word.equals(target)) return true;
        }
        return false;
    }
}
