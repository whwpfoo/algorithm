/**
 * 문자열 다루기 기본: https://programmers.co.kr/learn/courses/30/lessons/12918
 * s길이가 4 또는 6 이고 숫자로만 구성 => true; 아니면 false
 */
public class _12918 {
    public boolean solution(String s) {
        return (s.length() == 4 || s.length() == 6) && s.chars().allMatch(Character::isDigit) ? true : false;
    }
}
