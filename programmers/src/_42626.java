import java.util.PriorityQueue;

/**
 * 더 맵게: https://programmers.co.kr/learn/courses/30/lessons/42626
 */
public class _42626 {
    public int solution(int[] scoville, int K) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();

        for (int sco : scoville) {
            pq.offer(sco);
        }
        int mixCount = 0;

        for ( ; pq.size() > 1;) {
            if (pq.peek() >= K) return mixCount;

            pq.offer(pq.poll() + (pq.poll() * 2));
            mixCount++;
        }

        return pq.poll() >= K ? mixCount : -1;
    }
}
