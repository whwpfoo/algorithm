import java.util.Arrays;

/**
 * 제일 작은 수 제거하기: https://programmers.co.kr/learn/courses/30/lessons/12935
 */
public class _12935 {
    public static void main(String[] args) {
        _12935 obj = new _12935();

        obj.solution(new int[] {10, 1, 2, 3});
    }

    public int[] solution(int[] arr) {
        if (arr.length == 1) return new int[]  {-1};
        int minNumber = Arrays.stream(arr).sorted().limit(1).toArray()[0];
        return Arrays.stream(arr).filter(value -> value != minNumber).toArray();
    }
}
