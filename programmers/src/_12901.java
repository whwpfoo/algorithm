import java.time.LocalDate;
/**
 * 2016년: https://programmers.co.kr/learn/courses/30/lessons/12901
 */
public class _12901 {

    public static void main(String[] args) {
        LocalDate targetDate = LocalDate.of(2016, 3 ,3);
        System.out.println(targetDate.getDayOfWeek().name());
    }
    public String solution(int a, int b) {
        LocalDate date = LocalDate.of(2016, a ,b);
        return date.getDayOfWeek().name().substring(0, 3);
    }
}
