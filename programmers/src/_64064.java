import java.util.*;

/**
 * 불량 사용자
 * https://school.programmers.co.kr/learn/courses/30/lessons/64064
 */
public class _64064 {
    Set<String> dup = new HashSet<>();

    public int solution(String[] user_id, String[] banned_id) {
        var ids = new ArrayList[banned_id.length];

        for (int i = 0; i < ids.length; i++) {
            ids[i] = new ArrayList<String>();
        }

        for (int i = 0; i < banned_id.length; i++) {
            int len = banned_id[i].length();
            sub:
            for (String user : user_id) {
                if (len != user.length()) continue;
                for (int j = 0; j < user.length(); j++) {
                    if (banned_id[i].charAt(j) == '*') continue;
                    if (banned_id[i].charAt(j) != user.charAt(j)) continue sub;
                }
                ids[i].add(user);
            }
        }

        return combination(ids, 0, new HashSet<String>());
    }

    private int combination(ArrayList<String>[] ids, int idx, HashSet<String> bannIds) {
        if (bannIds.size() == ids.length) {
            List<String> l = new ArrayList<>(bannIds);
            l.sort((String::compareTo));
            if (dup.contains(l.toString())) {
                return 0;
            }
            dup.add(l.toString());
            return 1;
        }

        int result = 0;

        if (idx < ids.length) {
            ArrayList<String> user = ids[idx];
            for (String id : user) {
                if (bannIds.contains(id)) continue;
                bannIds.add(id);
                result += combination(ids, idx + 1, bannIds);
                bannIds.remove(id);
            }
        }

        return result;
    }

    // user_id: ["frodo", "fradi", "crodo", "abc123", "frodoc"]
    // banned_id: ["fr*d*", "abc1**"]
    public static void main(String[] args) {
        _64064 obj = new _64064();
        int result = obj.solution(new String[]{"frodo", "fradi", "crodo", "abc123", "frodoc"}, new String[]{"*rodo", "*rodo", "******"});
        System.out.println(result);
    }
}
