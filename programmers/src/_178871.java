import java.util.HashMap;
import java.util.Map;

// https://school.programmers.co.kr/learn/courses/30/lessons/178871
public class _178871 {
    public String[] solution(String[] players, String[] callings) {
        Map<String, Integer> m = new HashMap<>();

        for (int i = 0; i < players.length; i++) {
            m.put(players[i], i);
        }

        for (int i = 0; i < callings.length; i++) {
            int number = m.get(callings[i]);
            String p = swap(players, number);
            m.put(callings[i], number - 1);
            m.put(p, number);
        }

        return players;
    }

    public String swap(String[] p, int number) {
        String temp = p[number - 1];
        p[number - 1] = p[number];
        p[number] = temp;
        return temp;
    }
}
