import java.util.Arrays;

/**
 * 나누어 떨어지는 숫자 배열: https://programmers.co.kr/learn/courses/30/lessons/12910
 */
public class _12910 {
    public int[] solution(int[] arr, int divisor) {
        int[] answer = {};
        answer = Arrays.stream(arr).filter(value -> value % divisor == 0).sorted().toArray();

        return answer.length == 0 ? new int[] {-1} : answer;
    }
}
