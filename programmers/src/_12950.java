/**
 * 행렬의 덧셈: https://programmers.co.kr/learn/courses/30/lessons/12950
 */
public class _12950 {

    public static void main(String[] args) {

    }

    public int[][] solution(int[][] arr1, int[][] arr2) {
        int line = arr1.length;
        int cell = arr1[0].length;
        int[][] answer = new int[line][cell];

        for (int i = 0; i < line; i++) {
            for (int j = 0; j < cell; j++) {
                answer[i][j] = arr1[i][j] + arr2[i][j];
            }
        }

        return answer;
    }
}
