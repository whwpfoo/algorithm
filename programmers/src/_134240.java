// https://school.programmers.co.kr/learn/courses/30/lessons/134240
public class _134240 {

    public static void main(String[] args) {

        _134240 obj = new _134240();
        String result = obj.solution(new int[]{1, 3, 4, 6});
        System.out.println(result);
    }

    public String solution(int[] food) {

        StringBuilder sb = new StringBuilder();
        sb.append(0);
        int waterPos = 0;

        for (int i = 1; i < food.length; i++) {
            int foodCount = food[i] / 2;
            sb.insert(waterPos, String.valueOf(i).repeat(foodCount));
            waterPos += foodCount;
            sb.insert(waterPos + 1, String.valueOf(i).repeat(foodCount));
        }

        return sb.toString();
    }
}
