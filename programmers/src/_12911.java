/**
 * 다음 큰 숫자: https://programmers.co.kr/learn/courses/30/lessons/12911
 */
public class _12911 {
    public int solution(int n) {
        int oneCount = Integer.bitCount(n);
        int plus = 1;

        while (true) {
            int num = Math.addExact(n, plus);

            if (oneCount == Integer.bitCount(num)) {
                return num;
            }
            plus += 1;
        }
    }
}
