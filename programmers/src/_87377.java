
import java.util.*;

// https://programmers.co.kr/learn/courses/30/lessons/87377
public class _87377 {
    // 두 직선이 존재하는 경우 교점의 공식
    // x = BF - ED / AD - BC
    // y = EC - AF / AD - BC
    // 단 AD - BC = 0 을 만족한다면 교점은 없거나 완전 일치
    public static void main(String[] args) {
        _87377 obj = new _87377();
        int[][] line = {{2, -1, 4}, {-2, -1, 4}, {0, -1, 1}, {5, -8, -12}, {5, 8, 12}};
        System.out.println(Arrays.toString(obj.solution(line)));
    }

    public String[] solution(int[][] line) {
        Set<long[]> positions = new HashSet<>();

        long minX = Long.MAX_VALUE, minY = Long.MAX_VALUE;

        for (int i = 0; i < line.length; i++) {
            for (int j = i + 1; j < line.length; j++) {
                if (isZero((long)line[i][0] * line[j][1], (long)line[i][1] * line[j][0])) continue;

                double x = getPosition((long)line[i][1] * line[j][2] - (long)line[i][2] * line[j][1],
                        (long)line[i][0] * line[j][1] - (long)line[i][1] * line[j][0]);
                double y = getPosition((long)line[i][2] * line[j][0] - (long)line[i][0] * line[j][2],
                        (long)line[i][0] * line[j][1] - (long)line[i][1] * line[j][0]);

                if (x - (long)x == 0 && y - (long)y == 0) {
                    positions.add(new long[] {(long)x, (long)y});
                    minX = Math.min(minX, (long)x);
                    minY = Math.min(minY, (long)y);
                }
            }
        }


        Map<Long, List<Long>> map = new HashMap<>();
        long height = 0, width = 0;

        for (long[] p : positions) {
            long px = p[0], py = p[1];

            if (minX < 0) {
                px = px + Math.abs(minX);
            } else if (minX > 0) {
                px = px - Math.abs(minX);
            }
            if (minY < 0) {
                py = py + Math.abs(minY);
            } else if (minY > 0) {
                py = py - Math.abs(minY);
            }

            List<Long> xs = map.getOrDefault(py, new ArrayList<>());
            xs.add(px);
            map.put(py, xs);
            height = Math.max(height, py);
            width = Math.max(width, px);
        }

        String defaultString = getDefault(width);
        String[] answer = new String[(int)height + 1];

        for (long i = 0; i < answer.length; i++) {
            long y = answer.length - 1 - i;

            if (map.get(y) == null) {
                answer[(int)i] = defaultString;
                continue;
            }

            StringBuilder sb = new StringBuilder(defaultString);
            List<Long> xs = map.get(y);
            for (long idx : xs) {
                sb.setCharAt((int)idx, '*');
            }
            answer[(int)i] = sb.toString();
        }

        return answer;
    }

    private String getDefault(long boardWidth) {
        StringBuilder sb = new StringBuilder();
        for (long i = 0; i <= boardWidth; i++) {
            sb.append(".");
        }
        return sb.toString();
    }

    public boolean isZero(long a, long b) {
        return a == b ? true : false;
    }

    public double getPosition(long a, long b) {
        return (double)a / b;
    }
}
