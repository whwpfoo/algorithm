import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// https://programmers.co.kr/learn/courses/30/lessons/87946
public class _87946 {
    public static void main(String[] args) {
        _87946 obj = new _87946();
        System.out.println(obj.solution(80, new int[][]{{80, 20}, {50, 40}, {30, 10}}));
    }

    boolean[] visited;
    int result;

    public int solution(int k, int[][] dungeons) {
        visited = new boolean[dungeons.length];
        getDungeonCount(dungeons, visited, k, 0);
        return result;
    }
    // d[i][0]: 입장 스태미나
    // d[i][1]: 소모 스태미나
    public void getDungeonCount(int[][] d, boolean[] visited, int stamina, int count) {
        if (stamina < 0) {
            return;
        }

        for (int i = 0; i < d.length; i++) {
            if (visited[i] == false) {
                visited[i] = true;
                int requireStamina = d[i][0], consumeStamina = d[i][1];

                if (stamina >= requireStamina) {
                    stamina -= consumeStamina;
                    getDungeonCount(d, visited, stamina, count + 1);
                    stamina += consumeStamina;
                }
                visited[i] = false;
            }
        }

        result = Math.max(result, count);
    }
}
