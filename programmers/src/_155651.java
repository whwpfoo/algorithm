import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * 호텔 대실
 * https://school.programmers.co.kr/learn/courses/30/lessons/155651
 */
class Solution {
    public int solution(String[][] book_time) {
        int[][] times = new int[book_time.length][book_time[0].length];

        for (int i = 0; i < book_time.length; i++) {
            String[] v = book_time[i];
            String[] st = v[0].split(":");
            String[] et = v[1].split(":");
            int i1 = Integer.parseInt(st[0]) * 60 + Integer.parseInt(st[1]);
            int i2 = Integer.parseInt(et[0]) * 60 + Integer.parseInt(et[1]);
            times[i][0] = i1;
            times[i][1] = i2;
        }
        Arrays.sort(times, Comparator.comparingInt(v -> v[0]));

        // 퇴실시간이 빠른 순으로 정렬
        PriorityQueue<int[]> pq = new PriorityQueue<>(Comparator.comparingInt(v -> v[1]));
        for (int[] time : times) {
            if (!pq.isEmpty()) {
                int[] t = pq.peek();
                if (time[0] - t[1] >= 10)
                    pq.poll();
            }
            pq.offer(time);
        }

        return pq.size();
    }
}