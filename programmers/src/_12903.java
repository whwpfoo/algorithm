/**
 * 가운데 글자 가져오기: https://programmers.co.kr/learn/courses/30/lessons/12903
 */
public class _12903 {
    public String solution(String s) {
        return isLengthEven(s) ? s.substring(s.length() / 2 - 1, s.length() / 2 + 1) : String.valueOf(s.charAt(s.length() / 2));
    }

    public static boolean isLengthEven(String s) {
        return s.length() % 2 == 0 ? true : false;
    }
}
