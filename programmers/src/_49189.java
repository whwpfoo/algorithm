import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

// https://programmers.co.kr/learn/courses/30/lessons/49189 - 가장 먼 노드
public class _49189 {
    public int solution(int n, int[][] edge) {
        List<List<Integer>> graph = new ArrayList<>();
        //int[][] graph = new int[n + 1][n + 1];
        int[] edgeCount = new int[50000];
        boolean[] visited = new boolean[n + 1]; // 1부터 시작하기 위해 +1 로 설정

        for (int i = 1; i <= n; i++) {
            graph.add(new ArrayList<Integer>());
        }

        for (int i = 0; i < edge.length; i++) {
            int start = edge[i][0];
            int end = edge[i][1];
            graph.get(start - 1).add(end);
            graph.get(end - 1).add(start);
            //graph[start][end] = graph[end][start] = 1;
        }


        Queue<Node> q = new LinkedList<>();
        q.offer(new Node(1, 0));
        visited[1] = true;


        while (!q.isEmpty()) {
            Node node = q.poll();
            edgeCount[node.distance]++;

            List<Integer> adjList = graph.get(node.num - 1);
            for (int num : adjList) {
                if (visited[num] == false) {
                    q.offer(new Node(num, node.distance + 1));
                    visited[num] = true;
                }
            }
            // for (int i = 1; i <= n; i++) {
            //     if (visited[i] == false
            //         && (graph[i][node.num] == 1 || graph[node.num][i] == 1)) {
            //         q.offer(new Node(i, node.distance + 1));
            //         visited[i] = true;
            //     }
            // }
        }


        int result = 0;
        for (int i = edgeCount.length - 1; i >= 0; i--) {
            if (edgeCount[i] != 0) {
                result = edgeCount[i];
                break;
            }
        }
        return result;
    }

    class Node {
        int num;
        int distance;

        public Node(int num, int distance) {
            this.num = num;
            this.distance = distance;
        }
    }
}
