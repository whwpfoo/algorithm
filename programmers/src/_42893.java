import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 매칭 점수: https://programmers.co.kr/learn/courses/30/lessons/42893
 */
public class _42893 {
    public static void main(String[] args) {

        String[] pages = {"<html lang=\"ko\" xml:lang=\"ko\" xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n  <meta charset=\"utf-8\">\n  <meta property=\"og:url\" content=\"https://careers.kakao.com/interview/list\"/>\n</head>  \n<body>\n<a href=\"https://programmers.co.kr/learn/courses/4673\"></a>#!MuziMuzi!)jayg07con&&\n\n</body>\n</html>", "<html lang=\"ko\" xml:lang=\"ko\" xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n  <meta charset=\"utf-8\">\n  <meta property=\"og:url\" content=\"https://www.kakaocorp.com\"/>\n</head>  \n<body>\ncon%\tmuzI92apeach&2<a href=\"https://hashcode.co.kr/tos\"></a>\n\n\t^\n</body>\n</html>"};
        _42893 obj = new _42893();

        System.out.println(obj.solution("Muzi", pages));

    }

    public int solution(String word, String[] pages) {
        String keyword = word.toLowerCase();

        Map<String, PageInfo> pageMap = new HashMap<>();
        int index = 0;

        for (String page : pages) {
            page = page.toLowerCase();


            int defaultPoint = 0;
            int pos = page.indexOf(keyword);

            while (pos > -1) {
                char before = page.charAt(pos - 1);
                char next = page.charAt(pos + keyword.length());

                if (!Character.isLowerCase(before) && !Character.isLowerCase(next)) {
                    defaultPoint++;
                }

                pos = page.indexOf(keyword, pos + 1);

            }


            int linkCount = 0;
            pos = page.indexOf("<a href=");

            while (pos > -1) {
                linkCount++;
                pos = page.indexOf("<a href=", pos + 1);
            }


            String domain = "";
            Pattern domainPattern = Pattern.compile("<meta property=\"og:url\" content=\"https://(.+?)\"/>");
            Matcher domainMatcher = domainPattern.matcher(page);

            if (domainMatcher.find()) {
                domain = domainMatcher.group(1);
            }


            PageInfo pageInfo = new PageInfo(page, index, defaultPoint, linkCount);
            pageMap.put(domain, pageInfo);

            index++;
        }

        for (PageInfo pageInfo : pageMap.values()) {
            Pattern pattern = Pattern.compile("<a href=\"https://(.+?)\"");
            Matcher matcher = pattern.matcher(pageInfo.page);
            while (matcher.find()) {
                String url = matcher.group(1);
                if (pageMap.containsKey(url)) {
                    pageMap.get(url).setLinkPoint(pageInfo.defaultPoint, pageInfo.linkCount);
                }
            }
        }

        ArrayList<PageInfo> list = new ArrayList<>(pageMap.values());
        Collections.sort(list);

        return list.get(0).index;
    }

    public class PageInfo implements Comparable<PageInfo>{
        String page;
        int index;
        int defaultPoint;
        int linkCount;
        double linkPoint;

        public PageInfo(String page, int index, int defaultPoint, int linkCount) {
            this.page = page;
            this.index = index;
            this.defaultPoint = defaultPoint;
            this.linkCount = linkCount;
        }

        public void setLinkPoint(int DefaultValue, int linkCount) {
            linkPoint += (double) DefaultValue / (double) linkCount;
        }

        @Override
        public int compareTo(PageInfo o) {
            double a = (double) this.defaultPoint + this.linkPoint;
            double b = (double) o.defaultPoint + o.linkPoint;

            return Double.compare(b, a);
        }
    }
}
