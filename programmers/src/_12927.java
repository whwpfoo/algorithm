import java.util.Arrays;

// https://programmers.co.kr/learn/courses/30/lessons/12927
public class _12927 {
    public static void main(String[] args) {
        _12927 obj = new _12927();
        int n = 4;
        int[] works = {4, 3, 3};
        System.out.println(obj.solution(n, works));
    }
    
    public long solution(int n, int[] works) {
        Arrays.sort(works);

        int lastWorkIndex = works.length - 1;
        int start = lastWorkIndex, max = works[lastWorkIndex];

        while (n > 0 && max > 0) {
            int pointer = start;

            while (pointer > 0 && works[pointer] >= max) pointer--; // 가장 큰 값의 이전 값 '인덱스' 를 구한다

            if (pointer == 0 && works[pointer] == max) { // works 배열의 모든 수가 동일한 상태
                while (n > 0 && pointer <= lastWorkIndex) { // 처음 부터 마지막 인덱스 까지 작업 시간을 낮춰준다
                    works[pointer]--; n--; pointer++;
                }
                continue;
            }

            int standard = works[pointer];          // 기준 값 (두번 째로 큰 수)
            int next = pointer + 1;                 // 기준 값 보다 큰 수의 시작 인덱스
            int count = lastWorkIndex - pointer;    // 기준 값 보다 큰 수의 개수

            // 가장 큰 수를 '기준 값' 으로 맞추기 위해 남은 작업 시간을 소비해 감소시킨다
            while (n > 0 && count > 0) {
                if (next == lastWorkIndex + 1) next = pointer + 1;
                if (works[next] > standard) {
                    works[next]--; n--;
                    if (works[next] == standard) count--;
                }
                next++;
            }

            max = standard; start = pointer;
        }

        return answer(works);
    }

    public long answer(int[] works) {
        long result = 0;
        for (int i = 0; i < works.length; i++) {
            result += works[i] * works[i];
        }
        return result;
    }
}
