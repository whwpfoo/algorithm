/**
 * 서울에서 김서방 찾기: https://programmers.co.kr/learn/courses/30/lessons/12919
 */
public class _12919 {
    public String solution(String[] seoul) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < seoul.length; i++) {
            if (seoul[i].contains("Kim")) {
                sb.append("김서방은 ").append(i + "에 있다");
                break;
            }
        }

        return sb.toString();

    }
}
