import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 무인도 여행
 * https://school.programmers.co.kr/learn/courses/30/lessons/154540?language=java
 */
public class _154540 {
    boolean[][] visited;
    int[][] map;
    int[] dx = {0, 0, -1, 1};
    int[] dy = {-1, 1, 0, 0};

    public int[] solution(String[] maps) {
        int row = maps.length;
        int col = maps[0].length();
        visited = new boolean[row][col];
        map = new int[row][col];

        for (int i = 0; i < maps.length; i++) {
            String r = maps[i];
            for (int j = 0; j < r.length(); j++) {
                if ("X".equalsIgnoreCase(Character.toString(r.charAt(j))) == false) {
                    map[i][j] = r.charAt(j) - '0';
                }
            }
        }

        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (visited[i][j])
                    continue;
                int value = search(map, i, j);
                if (value > 0) {
                    result.add(value);
                }
            }
        }

        if (result.size() == 0) {
            return new int[] {-1};
        }

        return result.stream().sorted().mapToInt(Integer::new).toArray();
    }

    public int search(int[][] map, int r, int c) {
        visited[r][c] = true;
        int food = map[r][c];

        if (food == 0)
            return food;

        for (int i = 0; i < 4; i++) {
            int dr = r + dy[i];
            int dc = c + dx[i];

            if (dr < 0 || dr >= map.length || dc < 0 || dc >= map[0].length)
                continue;
            if (visited[dr][dc])
                continue;

            food += search(map, dr, dc);
        }

        return food;
    }

    public static void main(String[] args) {
        _154540 obj = new _154540();
        int[] solution = obj.solution(new String[]{"X591X", "X1X5X", "X231X", "1XXX1"});
        System.out.println(Arrays.toString(solution));
    }
}
