/**
 * https://programmers.co.kr/learn/courses/30/lessons/12971
 */
public class _12971 {
    public static void main(String[] args) {

    }

    int[] dp1;
    int[] dp2;

    public int solution(int sticker[]) {
        int size = sticker.length;
        if (size == 1) return sticker[0];
        if (size == 2) return Math.max(sticker[0], sticker[1]);
        dp1 = new int[size];
        dp2 = new int[size];
        int a = attach(size - 2, sticker, true);
        int b = attach(size - 1, sticker, false);
        return Math.max(a, b);
    }

    public int attach(int index, int[] sticker, boolean isAttachFirst) {
        if ((index == 0 || index == 1) && isAttachFirst) {
            return sticker[0];
        } else if (index == 0 && isAttachFirst == false) {
            return 0;
        } else if (index == 1 && isAttachFirst == false) {
            return sticker[index];
        }

        if (isAttachFirst) {
            if (dp1[index] != 0) {
                return dp1[index];
            }
            return dp1[index] = Math.max(attach(index-2, sticker, true) + sticker[index], attach(index-1, sticker, isAttachFirst));
        } else {
            if (dp2[index] != 0) {
                return dp2[index];
            }
            return dp2[index] = Math.max(attach(index-2, sticker, false) + sticker[index], attach(index-1, sticker, isAttachFirst));
        }
    }
}
