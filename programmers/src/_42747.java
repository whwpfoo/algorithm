import java.util.Arrays;
import java.util.stream.Stream;

/**
 * H-Index: https://programmers.co.kr/learn/courses/30/lessons/42747
 */
public class _42747 {
    public static void main(String[] args) {
    }

    public int solution(int[] citations) {
        Arrays.sort(citations);
        int paperSize = citations.length;
        int answer = 0;
        // 인용횟수
        for (int i = 0; i < 10000; i++) {
            int 이상 = 0;
            int 이하 = 0;
            // 논문 탐색
            for (int j = 0; j < paperSize; j++) {
                if (citations[j] >= i) {
                    이상++;
                } else {
                    이하++;
                }
            }

            if (이상 == 이하) {
                answer = i;
                break;
            }
        }

        return answer;
    }
}
