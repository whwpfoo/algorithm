import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * JadenCase 문자열 만들기: https://programmers.co.kr/learn/courses/30/lessons/12951
 */
public class _12951 {
    public static void main(String[] args) {
    }

    public static String solution(String s) {
        StringBuilder sb = new StringBuilder();
        int wordLastIndex = s.length() - 1;

        for (int i = wordLastIndex; i >= 0; i--) {
            if (i != 0 && s.charAt(i - 1) != 32) {
                sb.insert(0, setLowerCase(s.charAt(i)));
            } else {
                sb.insert(0, Character.toUpperCase(s.charAt(i)));
            }
        }

        return sb.toString();
    }

    public static char setLowerCase(char c) {
        if (c >= 65 && c <= 90) {
            return (char)((int)c + 32);
        }
        return c;
    }

    public static String solution2(String s) {
        List<String> targetList = new ArrayList<>();

        Pattern p = Pattern.compile("\\s?([a-zA-Z0-9]*)\\s?");
        Matcher matcher = p.matcher(s);

        while(matcher.find()) {
            targetList.add(matcher.group());
        }

        for (String value : targetList) {
            String v = value.trim();
            if (v.length() > 1) {
                s = s.replaceAll("^"+v+"\\s", v.substring(0,1).toUpperCase() + v.substring(1).toLowerCase()+" ");
                s = s.replaceAll("^"+v+"$", v.substring(0,1).toUpperCase() + v.substring(1).toLowerCase());
                s = s.replaceAll("\\s"+v+"$", " "+v.substring(0,1).toUpperCase() + v.substring(1).toLowerCase());
                s = s.replaceAll("\\s"+v+"\\s", " "+v.substring(0,1).toUpperCase() + v.substring(1).toLowerCase()+" ");
            } else {
                s = s.replaceAll("^"+v+"\\s", v.toUpperCase()+" ");
                s = s.replaceAll("^"+v+"$", v.toUpperCase());
                s = s.replaceAll("\\s"+v+"$", " "+v.toUpperCase());
                s = s.replaceAll("\\s"+v+"\\s", " "+v.toUpperCase()+" ");
            }
        }

        return s;
    }
}
