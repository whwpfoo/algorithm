import java.util.Arrays;
import java.util.LinkedList;

/**
 * 미로 탈출
 * https://school.programmers.co.kr/learn/courses/30/lessons/159993
 */
public class _159993 {
    int[][] map;
    boolean[][] visited;
    int[] s = new int[2];
    int[] l = new int[2];
    int[] e = new int[2];
    int[] dx = {1, 0, -1, 0};
    int[] dy = {0, 1, 0, -1};

    // map: S 시작지점, E 출구, L 레버, O 통로, X 벽
    // case1: S -> L 최단거리
    // case2: L -> E 최단거리
    // * X 못지나감
    public int solution(String[] maps) {
        int rowL = maps.length; int colL = maps[0].length();
        map = new int[rowL][colL];
        visited = new boolean[rowL][colL];

        // init
        // 0: 통로, 1: 벽
        for (int i = 0; i < rowL; i++) {
            if (maps[i].indexOf("S") > -1) {
                s[0] = i; s[1] = maps[i].indexOf("S");
            }
            if (maps[i].indexOf("L") > -1) {
                l[0] = i; l[1] = maps[i].indexOf("L");
            }
            if (maps[i].indexOf("E") > -1) {
                e[0] = i; e[1] = maps[i].indexOf("E");
            }

            String[] row = maps[i].split("");
            for (int k = 0; k < row.length; k++) {
                if ("X".equals(row[k])) map[i][k] = 1;
                if ("L".equals(row[k])) map[i][k] = 2;
                if ("E".equals(row[k])) map[i][k] = 3;
            }
        }

        // case1
        LinkedList<int[]> q = new LinkedList<>();
        q.offer(new int[]{s[0], s[1], 0});
        visited[s[0]][s[1]] = true;
        int answer = 0;

        while (!q.isEmpty()) {
            int[] pos = q.poll();
            if (map[pos[0]][pos[1]] == 2) {
                answer = pos[2];
                break;
            }

            for (int dir = 0; dir < 4; dir++) {
                int r = pos[0] + dy[dir];
                int c = pos[1] + dx[dir];
                if (r < 0 || r >= rowL || c < 0 || c >= colL) continue;
                if (visited[r][c] || map[r][c] == 1) continue;

                visited[r][c] = true;
                q.offer(new int[] {r, c, pos[2] + 1});
            }
        }

        if (answer == 0)
            return -1;

        // case2
        q = new LinkedList<>();
        visited = new boolean[rowL][colL];
        q.offer(new int[]{l[0], l[1], 0});
        visited[l[0]][l[1]] = true;

        while (!q.isEmpty()) {
            int[] pos = q.poll();
            if (map[pos[0]][pos[1]] == 3) {
                return answer + pos[2];
            }

            for (int dir = 0; dir < 4; dir++) {
                int r = pos[0] + dy[dir];
                int c = pos[1] + dx[dir];
                if (r < 0 || r >= rowL || c < 0 || c >= colL) continue;
                if (visited[r][c] || map[r][c] == 1) continue;

                visited[r][c] = true;
                q.offer(new int[] {r, c, pos[2] + 1});
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        _159993 obj = new _159993();
        int solution = obj.solution(new String[]{"SOOOL", "XXXXO", "OOOOO", "OXXXX", "OOOOE"});
        System.out.println(solution);
    }
}
