// https://programmers.co.kr/learn/courses/30/lessons/77885
public class _77885 {
    public long[] solution(long[] numbers) {
        long[] answer = new long[numbers.length];
        int index = 0;
        for (long number : numbers) {
            if (number % 2 == 0) {
                answer[index++] = number + 1;
                continue;
            }
            String binaryNumber = Long.toBinaryString(number);
            if (binaryNumber.charAt(0) == '1') {
                binaryNumber = "0" + binaryNumber;
            }
            int pos = binaryNumber.lastIndexOf("0");
            String[] split = binaryNumber.split("");
            split[pos] = "1";
            split[pos + 1] = "0";
            String join = String.join("", split);
            long l = Long.parseLong(join, 2);
            answer[index++] = l;
        }
        return answer;
    }
}
