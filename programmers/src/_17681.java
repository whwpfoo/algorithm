import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * [1차] 비밀지도 https://programmers.co.kr/learn/courses/30/lessons/17681
 */
public class _17681 {

    private static final String BLOCK = "#";
    private static final String EMPTY = " ";

    public static void main(String[] args) {
        int n = 6;
        int[] arr1 = {46, 33, 33, 22, 31, 50};
        int[] arr2 = {27, 56, 19, 14, 14, 10};

        _17681 obj = new _17681();
        obj.solution(n, arr1, arr2);
    }

    public String[] solution(int n, int[] arr1, int[] arr2) {
        String[] answer = new String[n];

        for (int i = 0; i < n; i++) {
            int orCalculate = arr1[i] | arr2[i];
            String binary = Integer.toBinaryString(orCalculate);
            binary = zeroFill(n - binary.length(), binary);
            answer[i] = binary.replaceAll("1", BLOCK).replaceAll("0", EMPTY);
        }

        return answer;
    }

    public String zeroFill(int fillLength, String binary) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < fillLength; i++) {
            sb.append("0");
        }
        sb.append(binary);
        return sb.toString();
    }
}
