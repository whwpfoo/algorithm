/**
 * 예상 대진표: https://programmers.co.kr/learn/courses/30/lessons/12985
 */
public class _12985 {
    public int solution(int n, int a, int b) {
        int result = 0;
        if (a > b) result = tournament(b ,a, 1);
        else result = tournament(a, b, 1);
        return result;
    }

    public static int tournament(int a, int b, int count) {
        if ((b - a) == 1 && ((b + 1) % 2) != 0) {
            return count;
        }

        if (a % 2 == 0) {
            a = (a / 2);
        } else {
            a = (a / 2) + 1;
        }

        if (b % 2 == 0) {
            b = (b / 2);
        } else {
            b = (b / 2) + 1;
        }

        return tournament(a, b, count + 1);
    }
}
