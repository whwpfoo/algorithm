import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

// https://school.programmers.co.kr/learn/courses/30/lessons/92341
public class _92341 {

    public static void main(String[] args) {
        int[] fees = {180, 5000, 10, 600};
        String[] records = {"05:34 5961 IN", "06:00 0000 IN", "06:34 0000 OUT", "07:59 5961 OUT", "07:59 0148 IN", "18:59 0000 IN", "19:09 0148 OUT", "22:59 5961 IN", "23:00 5961 OUT"};
        _92341 obj = new _92341();
        int[] result = obj.solution(fees, records);
        System.out.println(Arrays.toString(result));

    }

    public int[] solution(int[] fees, String[] records) {

        Map<String, History> timeMap = getTimeMap(records);
        List<Integer> answer = new ArrayList<>();

        for (History h : timeMap.values()) {
            answer.add(h.calcTime().calcMoney(fees).getTotalMoney());
        }

        return answer.stream().mapToInt(Integer::intValue).toArray();
    }

    private Map<String, History> getTimeMap(String[] records) {

        Map<String, History> map = new TreeMap<>();

        for (String r : records) {
            String[] s = r.split(" ");
            if (map.containsKey(s[1])) {
                map.get(s[1]).addTime(s[0], s[2]);
            } else {
                map.put(s[1], new History());
                map.get(s[1]).addTime(s[0], s[2]);
            }
        }

        return map;
    }

    static class History {

        private long totalTime = 0;
        private int totalMoney = 0;
        private List<String> inBound = new ArrayList<>();
        private List<String> outBound = new ArrayList<>();

        public void addTime(String min, String type) {

            if ("IN".equals(type)) {
                inBound.add(min);
            } else if ("OUT".equals(type)) {
                outBound.add(min);
            } else {
                throw new IllegalArgumentException();
            }
        }

        public History calcTime() {

            for (int i = 0; i < outBound.size(); i++) {
                String[] in = inBound.get(i).split(":");
                String[] out = outBound.get(i).split(":");
                totalTime += LocalTime.of(Integer.parseInt(in[0]), Integer.parseInt(in[1]))
                        .until(LocalTime.of(Integer.parseInt(out[0]), Integer.parseInt(out[1])), ChronoUnit.MINUTES);
            }

            if (inBound.size() != outBound.size()) {
                String[] in = inBound.get(inBound.size() - 1).split(":");
                totalTime += LocalTime.of(Integer.parseInt(in[0]), Integer.parseInt(in[1]))
                        .until(LocalTime.of(23, 59), ChronoUnit.MINUTES);
            }

            return this;
        }

        public History calcMoney(int[] fees) {
            totalMoney = fees[1];

            if (totalTime > fees[0]) {
                totalMoney += Math.ceil(
                        (double) (totalTime - fees[0]) / fees[2]) * fees[3];
            }

            return this;
        }

        public int getTotalMoney() {
            return totalMoney;
        }
    }
}
