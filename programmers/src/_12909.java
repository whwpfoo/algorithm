import java.util.Stack;

/**
 * 올바른 괄호: https://programmers.co.kr/learn/courses/30/lessons/12909
 */
public class _12909 {
    boolean solution(String s) {
        Stack<Character> stack = new Stack<>();
        int length = s.length();

        for (int i = 0; i < length; i++) {
            if (!stack.isEmpty()) {
                if (s.charAt(i) != stack.peek()) {
                    stack.pop();
                } else {
                    stack.push(s.charAt(i));
                }
            } else {
                if (s.charAt(i) == ')') return false;
                stack.push(s.charAt(i));
            }
        }

        return stack.size() == 0 ? true : false;
    }
}