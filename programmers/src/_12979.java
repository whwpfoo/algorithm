/**
 * https://programmers.co.kr/learn/courses/30/lessons/12979
 */
public class _12979 {
    public static void main(String[] args) {
        _12979 obj = new _12979();
        obj.solution(11, new int[] {4, 11}, 1);
    }

    public int solution(int n, int[] stations, int w) {
        boolean[] apartmentArray = new boolean[n];

        for (int position : stations) {
            apartmentArray[position - 1] = true;
            int index = position - 1;
            for (int i = 1; i <= w; i++) {
                if (index + i <= n - 1) apartmentArray[index + i] = true;
                if (index - i >= 0) apartmentArray[index - i] = true;
            }
        }


        int lastIndex = n - 1;
        int index = 0;
        int count = 0;

        while (index <= lastIndex) {
            if (apartmentArray[index]) {
                index++;
            } else {
                int pos = index + w;
                if (pos <= lastIndex) {
                    if (apartmentArray[pos]) {
                        for (int i = pos - 1; i >= index; i--) {
                            if (i >= 0 && apartmentArray[i] == false) {
                                apartmentArray[i] = true;
                                for (int j = 1; j <= w; j++) {
                                    if (i - j >= 0) {
                                        apartmentArray[i - j] = true;
                                    } else {
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    } else {
                        apartmentArray[pos] = true;
                        for (int i = 1; i <= w; i++) {
                            if (pos + i <= lastIndex)
                                apartmentArray[pos + i] = true;
                            if (pos - i >= 0)
                                apartmentArray[pos - i] = true;
                        }
                    }
                    count++;
                }
            }
        }

        return count;
    }
}
