import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * [1차] 다트 게임: https://programmers.co.kr/learn/courses/30/lessons/17682
 */
public class _17682 {

    private static final String SINGLE = "1";
    private static final String DOUBLE = "2";
    private static final String TRRIPLE = "3";

    public int solution(String dartResult) {
        int answer = 0;

        Pattern pattern = Pattern.compile("(\\d+[S|D|T]\\D?)(\\d+[S|D|T]\\D?)(\\d+[S|D|T]\\D?)");
        Matcher matcher = pattern.matcher(dartResult);


        if (matcher.find()) {
            int count = matcher.groupCount();
            boolean isMultiply = false;
            int minus = -1;

            for (int i = count; i > 0; i--) {
                String group = matcher.group(count);
                int point = Integer.parseInt(group.replaceAll("[^0-9]", ""));
                String[] optionArr = group.replace(String.valueOf(point), "").split("");

                optionArr[0] = optionArr[0].equalsIgnoreCase("S")
                        ? SINGLE : optionArr[0].equalsIgnoreCase("D")
                        ? DOUBLE : TRRIPLE;

                int result = (int)Math.pow(point, Integer.parseInt(optionArr[0]));

                if (isMultiply) {
                    result *= 2;
                    isMultiply = false;
                }

                if (optionArr.length > 1) {
                    if (optionArr[1].equalsIgnoreCase("*")) {
                        result *= 2;
                        isMultiply = true;
                    } else {
                        result *= minus;
                    }
                }

                answer += result;
                count--;
            }
        }

        return answer;
    }
}
