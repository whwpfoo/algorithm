import java.util.Arrays;
import java.util.LinkedList;

// https://school.programmers.co.kr/learn/courses/30/lessons/118667
public class _118667 {

    public int solution(int[] queue1, int[] queue2) {

        long q1Total = getTotal(queue1);
        long q2Total = getTotal(queue2);

        if (q1Total == q2Total) {
            return 0;
        } else if ((q1Total + q2Total) % 2 != 0) {
            return -1;
        }

        LinkedList<Long> q1 = getLongTypeQ(queue1);
        LinkedList<Long> q2 = getLongTypeQ(queue2);
        int answer = 0;
        int loop = 0;

        while (loop < queue1.length * 3) {
            if (q1Total > q2Total) {
                q1Total -= q1.peekFirst();
                q2Total += q1.peekFirst();
                q2.offer(q1.poll());
            } else if (q1Total < q2Total) {
                q2Total -= q2.peekFirst();
                q1Total += q2.peekFirst();
                q1.offer(q2.poll());
            } else {
                return answer;
            }
            answer++; loop++;
        }

        return -1;
    }

    private long getTotal(int[] q) {
        return Arrays.stream(q).mapToLong(Long::new).sum();
    }

    private LinkedList<Long> getLongTypeQ(int[] q) {

        LinkedList<Long> ll = new LinkedList<>();
        Arrays.stream(q).mapToLong(Long::new).forEach(v -> ll.offer(v));

        return ll;
    }
}
