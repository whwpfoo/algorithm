package main

// https://programmers.co.kr/learn/courses/30/lessons/42860
func solution(name string) int {
	result := upDown(name)
	nameLength := len(name)
	cursor := nameLength - 1

	for i := 0; i < nameLength; i++ {
		next := i + 1
		for next < nameLength && name[next] == 'A' {
			next += 1
		}

		countA := next - i - 1 				//
		defaultMove := nameLength - countA - 1	// A제외한 움직임 횟수
		minValue := min(i, nameLength - next)
		cursor = min(cursor, defaultMove + minValue)

	}
	return result + cursor
}

func upDown(name string) int {
	count := 0
	for cha := range name {
		a := cha - 'A'
		b := 'Z' - cha + 1
		if a < b {
			count += a
		} else {
			count += b
		}
	}
	return count
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}