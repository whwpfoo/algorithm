import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * [3차] 방금그곡: https://programmers.co.kr/learn/courses/30/lessons/17683
 * 기억하는 멜로디: ABC
 * 곡 리스트
 * [
 *  12:00,12:14,HELLO,C#DEFGAB,
 *  13:00,13:05,WORLD,ABCDEF
 *  ]
 */
public class _17683 {
    public static void main(String[] args) {
        //"(.*)code(.*)")
        String m = "ABC#";
        String[] musicinfos = {"12:00,12:14,HELLO,C#DEFGAB", "13:00,13:05,WORLD,ABCDEF"};
        boolean status = false;
        String musicSheet = "C#DEFGABC#";

        int shopLength = musicSheet.replaceAll("[^#]", "").length();
        System.out.println(shopLength);

    }

    public static String solution(String m, String[] musicinfos) {
        List<MusicInfo> musicInfoList = new ArrayList<>();

        for (int i = 0; i < musicinfos.length; i++) {
            String[] musicInfoArray = musicinfos[i].split(",");

            LocalTime startTime = LocalTime.of
                    (Integer.parseInt(musicInfoArray[0].split(":")[0]), Integer.parseInt(musicInfoArray[0].split(":")[1]));
            LocalTime endTime = LocalTime.of
                    (Integer.parseInt(musicInfoArray[1].split(":")[0]), Integer.parseInt(musicInfoArray[1].split(":")[1]));

            int playTime = (int)Duration.between(startTime, endTime).getSeconds() / 60;
            String subject = musicInfoArray[2];
            String sheet = musicInfoArray[3];
            StringBuilder sb = new StringBuilder();

            int shopLength = sheet.replaceAll("[^#]", "").length();
            int repeat = playTime / (sheet.length() - shopLength);
            int tail = playTime % sheet.length();

            while (repeat > 0) {
                sb.append(sheet);
                repeat--;
            }

            for (int j = 0; j < tail; j++) {
                char scale = sheet.charAt(j);
                sb.append(scale);
                if (scale == '#') {
                    tail++;
                }
            }

            String musicSheet = sb.toString();
            char mSclae = m.charAt(m.length() - 1);
            boolean status = false;

            if (mSclae == '#') {
                status = musicSheet.matches("(.*)" + m + "(.*)");
            } else {
                status = musicSheet.matches("(.*)" + m + "([^#].*)");
            }

            musicInfoList.add(new MusicInfo(playTime, subject, status));
        }

        musicInfoList = musicInfoList.stream().filter(obj -> obj.status == true).sorted().collect(Collectors.toList());
        String answer = "(None)";

        if (musicInfoList.size() != 0) {
            answer = musicInfoList.get(0).subject;
        }

        return answer;
    }

    public static class MusicInfo implements Comparable<MusicInfo>{
        public int playTime;
        public String subject;
        public boolean status;

        public MusicInfo(int playTime, String subject, boolean status) {
            this.playTime = playTime;
            this.subject = subject;
            this.status = status;
        }

        @Override
        public int compareTo(MusicInfo o) {
            MusicInfo target = (MusicInfo)o;
            return target.playTime - this.playTime;
        }
    }
}
