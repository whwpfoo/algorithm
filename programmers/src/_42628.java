import java.util.PriorityQueue;
import java.util.Queue;

// https://programmers.co.kr/learn/courses/30/lessons/42628
public class _42628 {
    public static void main(String[] args) {
    }

    static final String ADD = "I";
    static final int REMOVE_MIN = -1;

    public int[] solution(String[] operations) {
        Queue<Integer> q = new PriorityQueue<>();

        for (String s : operations) {
            String[] op = s.split(" ");
            String command = op[0];
            int number = Integer.parseInt(op[1]);
            q = execute(command, number, q);
        }

        if (q.size() == 0) {
            return new int[] {0, 0};
        }

        return result(q);
    }

    public Queue<Integer> execute(String op, int num, Queue<Integer> q) {
        if (op.equals(ADD)) {
            q.add(num);
            return q;
        }
        if (q.size() == 0) {
            return q;
        }
        if (num == REMOVE_MIN) {
            q.poll();
            return q;
        }

        Queue<Integer> nq = new PriorityQueue<>();

        while (q.size() > 1) {
            nq.add(q.poll());
        }

        return nq;
    }

    public int[] result(Queue<Integer> q) {
        int[] answer = new int[2];

        answer[1] = q.poll();
        while (q.size() > 1) {
            q.poll();
        }
        answer[0] = q.poll();

        return answer;
    }
}
