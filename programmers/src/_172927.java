import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 광물 캐기
 * https://school.programmers.co.kr/learn/courses/30/lessons/172927
 */
public class _172927 {
    // [1, 3, 2]	["diamond", "diamond", "diamond", "iron", "iron", "diamond", "iron", "stone"]
    public int solution(int[] picks, String[] minerals) {
        List<int[]> l = new ArrayList<>();

        for (int i = 0; i < minerals.length; i++) {
            int idx = i % 5;
            if (idx == 0)
                l.add(new int[] {0, 0, 0});

            int[] pick = l.get(i / 5);
            String mineral = minerals[i];
            if ("diamond".equals(mineral)) pick[0]++;
            if ("iron".equals(mineral)) pick[1]++;
            if ("stone".equals(mineral)) pick[2]++;
        }

        int total = Arrays.stream(picks).reduce((v1, v2) -> v1 + v2).getAsInt() * 5;
        if (total < minerals.length) {
            int val = (minerals.length - total) / 5;
            if (minerals.length % 5 != 0) val++;
            for (int i = 0; i < val; i++) {
                l.remove(l.size() - 1);
            }
        }

        // 내림차순: dia -> iron
        Collections.sort(l, (v1, v2) -> {
            if (v2[0] == v1[0])
                return v2[1] - v1[1];
            return v2[0] - v1[0];
        });

        int answer = 0;

        for (int[] pick : l) {
            if (picks[0] > 0) {
                answer += pick[0];
                answer += pick[1];
                answer += pick[2];
                picks[0]--;
            } else if (picks[1] > 0) {
                answer += pick[0] * 5;
                answer += pick[1];
                answer += pick[2];
                picks[1]--;
            } else if (picks[2] > 0) {
                answer += pick[0] * 25;
                answer += pick[1] * 5;
                answer += pick[2];
                picks[2]--;
            }
        }

        return answer;
    }

    public static void main(String[] args) {
        _172927 obj = new _172927();
        int result = obj.solution(new int[]{0, 1, 1}, new String[]{"diamond", "diamond", "diamond", "diamond", "diamond", "iron", "iron", "iron", "iron", "iron", "diamond"});
        System.out.println(result);
    }
}
