import java.util.Collections;
import java.util.LinkedList;

/**
 * https://programmers.co.kr/learn/courses/30/lessons/17678
 */
public class _17678 {
    public static void main(String[] args) {
        int n = 1;
        int t = 1;
        int m = 5;
        String[] times = {"00:01", "00:01", "00:01", "00:01", "00:01"};

        _17678 obj = new _17678();
        System.out.println(obj.solution(n, t, m, times));
    }

    public String solution(int n, int t, int m, String[] timetable) {
        int limit = n;
        int nextTime = t;


        LinkedList<Time> timeList = new LinkedList<>();
        for (String time : timetable) {
            int hh = Integer.valueOf(time.substring(0, 2));
            int mm = Integer.valueOf(time.substring(3));
            timeList.add(new Time(hh, mm));
        }
        Collections.sort(timeList);


        Time start = new Time(9, 0);
        int index = 0;
        while (limit > 0) {
            int count = m;
            for (int i = index; i < timeList.size(); i++) {
                if (timeList.get(i).hour < start.hour ||
                   (timeList.get(i).hour <= start.hour && timeList.get(i).min <= start.min)) {
                    index++;
                    count--;
                    if (count == 0) break;
                }
            }
            if (limit == 1) {
                if (count == 0) {
                    Time time = timeList.get(index);
                    time.minusMin(1);
                    return convertTime(time.hour, time.min);
                } else {
                    return convertTime(start.hour, start.min);
                }
            }
            start.addMin(nextTime);
            limit--;
        }


        return null;
    }

    public String convertTime(int hour, int min) {
        if (hour < 10 && min < 10) {
            return "0" + hour + ":" + "0" + min;
        } else if (hour < 10) {
            return "0" + hour + ":" + min;
        } else if (min < 10) {
            return hour + ":" + "0" + min;
        } else {
            return hour + ":" + min;
        }
    }

    class Time implements Comparable<Time> {
        int hour;
        int min;

        public Time(int hour, int min) {
            this.hour = hour;
            this.min = min;
        }

        public void addMin(int min) {
            if (this.min + min >= 60) {
                hour++;
                this.min = (this.min + min) - 60;
            } else {
                this.min = this.min + min;
            }
        }

        public void minusMin(int min) {
            if (this.min - min < 0) {
                hour--;
                this.min = 60 - Math.abs(this.min - min);
            } else {
                this.min = this.min - min;
            }
        }

        @Override
        public int compareTo(Time other) {
            if (this.hour == other.hour) {
                return this.min - other.min;
            }
            return this.hour - other.hour;
        }
    }
}
