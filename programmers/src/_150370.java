import java.util.*;

/**
 * 개인정보 수집 유효기간
 * https://school.programmers.co.kr/learn/courses/30/lessons/150370
 */
public class _150370 {
    public int[] solution(String today, String[] terms, String[] privacies) {
        int[] current = getMonthDay(today);

        Map<String, Integer> termMap = new HashMap<>();
        for (String term : terms) {
            String[] t = term.split(" ");
            termMap.put(t[0], Integer.parseInt(t[1]));
        }

        List<Integer> answer = new ArrayList<>();
        for (int i = 0; i < privacies.length; i++) {
            String[] data = privacies[i].split(" ");
            int[] target = getMonthDay(data[0]);

            target[0] += termMap.get(data[1]);
            if (target[1] == 1) {
                target[1] = 28; target[0]--;
            } else {
                target[1]--;
            }
            if (target[0] > current[0]) {
                continue;
            }
            if (target[0] < current[0]) {
                answer.add(i + 1);
                continue;
            }
            if (target[0] == current[0] && target[1] < current[1]) {
                answer.add(i + 1);
            }
        }

        return answer.stream().mapToInt(Integer::new).toArray();
    }

    public int[] getMonthDay(String date) {
        String[] d = date.split("\\.");
        int month = Integer.parseInt(d[1]) + (Integer.parseInt(d[0]) * 12);
        int day = Integer.parseInt(d[2]);
        return new int[] {month, day};
    }

    public static void main(String[] args) {
        _150370 obj = new _150370();
        int[] result = obj.solution("2022.05.19", new String[]{"A 6", "B 12", "C 3"}, new String[]{"2021.05.02 A", "2021.07.01 B", "2022.02.19 C", "2022.02.20 C"});
        System.out.println(Arrays.toString(result));
    }
}
