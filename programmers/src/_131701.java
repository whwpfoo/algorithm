import java.util.HashSet;
import java.util.Set;

// https://school.programmers.co.kr/learn/courses/30/lessons/131701
public class _131701 {

    public static void main(String[] args) {

        _131701 obj = new _131701();
        int result = obj.solution(new int[]{7, 9, 1, 1, 4});
        System.out.println(result);
    }

    public int solution(int[] elements) {

        int[] arr = new int[elements.length * 2];
        int idx = 0;

        // arr에 elements 값을 복사 (원순열 이기때문에 2배로)
        for (int i = 0; i < arr.length; i++) {
            arr[i] = elements[idx++];

            if (idx == elements.length) {
                idx = 0;
            }
        }
        
        Set<Integer> result = new HashSet<>();
        int cut = 1;

        while (cut <= elements.length) {
            int curr = 0;
            for (int i = 0; i < cut; i++) {
                curr = curr + arr[i];
            }

            result.add(curr);

            for (int i = 1; i < elements.length; i++) {
                curr = curr - arr[i - 1] + arr[i + cut - 1]; // sliding window
                result.add(curr);
            }
            cut++;
        }

        return result.size();
    }
}
