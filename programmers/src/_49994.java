import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// https://programmers.co.kr/learn/courses/30/lessons/49994
public class _49994 {
    public static void main(String[] args) {
        _49994 obj = new _49994();
        int ulurrdllu = obj.solution("ULURRDLLU");
        System.out.println(ulurrdllu);
    }

    public int solution(String dirs) {
        if (dirs.length() == 0) { return 0; }

        Set<String> moveCountList = new HashSet<>();
        String empty = "";
        int x = 0;
        int y = 0;

        String[] commands = dirs.split("");

        for (String command : commands) {
            String origin = empty + x + y;
            String change;

            if (command.equals("U")) {
                if (y + 1 > 5) continue;
                y++;
            } else if (command.equals("D")) {
                if (y - 1 < -5) continue;
                y--;
            } else if (command.equals("L")) {
                if (x - 1 < -5) continue;
                x--;
            } else {
                if (x + 1 > 5) continue;
                x++;
            }

            change = empty + x + y;
            moveCountList.add(origin + change);
            moveCountList.add(change + origin);
        }

        return moveCountList.size() / 2;
    }
}
