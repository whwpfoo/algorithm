import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * https://programmers.co.kr/learn/courses/30/lessons/12978
 */
public class _12978 {
    public static void main(String[] args) {
        _12978 obj = new _12978();
        int N = 6;
        int[][] roads = {{1,2,1},{1,3,2},{2,3,2},{3,4,3},{3,5,2},{3,5,3},{5,6,1}};
        int K = 4;
        System.out.println(obj.solution(N, roads, K));
    }

    public int solution(int N, int[][] road, int K) {
        // node edge 연결 테이블
        int[][] maps = new int[N + 1][N + 1];
        for (int i = 0; i < road.length; i++) {
            int target = road[i][0];
            int source = road[i][1];
            int weight = road[i][2];

            maps[target][source] = maps[source][target] = maps[target][source] != 0 ? Math.min(maps[target][source], weight) : weight;
        }

        // node 간 거리 테이블 초기화
        int[] distanceTable = new int[N + 1];
        Arrays.fill(distanceTable, Integer.MAX_VALUE);
        distanceTable[1] = 0;

        // node 방문 테이블 초기화
        boolean[] visited = new boolean[N + 1];
        Arrays.fill(visited, false);

        // node distance에 따른 우선순위 queue 생성
        PriorityQueue<Node> q = new PriorityQueue<>();
        q.offer(new Node(1, 0));

        while (q.isEmpty() == false) {
            Node node = q.poll();
            int index = node.index;
            int distance = node.distance;

            // node의 인접한 노드들을 priority queue 에 집어 넣어야 함
            // 단, 최소거리를 비교하여 거리 테이블을 갱신시켜줘야 함
            for (int i = 1; i < maps.length; i++) {
                if (maps[index][i] != 0 && visited[i] == false) {
                    if (distanceTable[i] <= maps[index][i] + distance) {
                        continue;
                    }
                    Node target = new Node(i, maps[index][i] + distance);
                    distanceTable[i] = target.distance;
                    q.offer(target);
                }
            }

            visited[index] = true;
        }

        int result = 0;
        for (int i = 1; i < distanceTable.length; i++) {
            if (distanceTable[i] <= K) result++;
        }
        return result;
    }

    class Node implements Comparable<Node> {
        int index;
        int distance;

        public Node(int index, int distance) {
            this.index = index;
            this.distance = distance;
        }

        @Override
        public int compareTo(Node o) {
            return this.distance - o.distance;
        }
    }
}
