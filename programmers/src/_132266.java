import java.util.*;
import java.util.stream.Collectors;

/**
 * 부대복귀
 * https://school.programmers.co.kr/learn/courses/30/lessons/132266
 */
public class _132266 {
    Map<Integer, List<Integer>> map;
    Set<Integer> set;
    int[] distance;
    boolean[] visited;

    public int[] solution(int n, int[][] roads, int[] sources, int destination) {
        map = init(roads);
        set = Arrays.stream(sources).boxed().collect(Collectors.toSet());
        distance = new int[n + 1];
        visited = new boolean[n + 1];

        Arrays.fill(distance, Integer.MAX_VALUE);

        LinkedList<int[]> q = new LinkedList<>();
        q.offer(new int[] {destination, 0});
        visited[destination] = true;

        while (!q.isEmpty()) {
            int[] v = q.poll();
            if (set.contains(v[0])) {
                distance[v[0]] = Math.min(v[1], distance[v[0]]);
            }

            List<Integer> road = map.get(v[0]);
            if (road != null) {
                for (int r : road) {
                    if (visited[r] == false) {
                        visited[r] = true;
                        q.offer(new int[] {r, v[1] + 1});
                    }
                }
            }
        }

        List<Integer> answer = new ArrayList<>();
        for (int v : sources) {
            answer.add(distance[v] == Integer.MAX_VALUE ? -1 : distance[v]);
        }

        return answer.stream().mapToInt(v -> v).toArray();
    }

    public Map<Integer, List<Integer>> init(int[][] roads) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int[] r : roads) {
            if (!map.containsKey(r[0])) {
                map.put(r[0], new ArrayList<>());
            }
            if (!map.containsKey(r[1])) {
                map.put(r[1], new ArrayList<>());
            }
            map.get(r[0]).add(r[1]);
            map.get(r[1]).add(r[0]);
        }
        return map;
    }

    public static void main(String[] args) {
        _132266 obj = new _132266();
        int[] result = obj.solution(3, new int[][]{{1, 2}, {2, 3}}, new int[]{2, 3}, 1);
        System.out.println(Arrays.toString(result));
    }
}
