/**
 * https://programmers.co.kr/learn/courses/30/lessons/67259
 */
public class _67259 {
    public static void main(String[] args) {
        int[][] board = {{0, 0, 1, 0}, {0, 0, 0, 0}, {0, 1, 0, 1}, {1, 0, 0, 0}};
        _67259 obj = new _67259();
        System.out.println(obj.solution(board));

    }

    int result = Integer.MAX_VALUE;
    boolean[][] visited;
    int[] posX = {0, 1, 0, -1};
    int[] posY = {1, 0, -1, 0};
    int[][] dist;

    public int solution(int[][] board) {
        visited = new boolean[board.length][board[0].length];
        dist = new int[board.length][board[0].length];

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                dist[i][j] = Integer.MAX_VALUE;
            }
        }

        Car car = new Car();
        find(car, board, visited, 0, 0, car.sumCost());
        return result;
    }

    public void find(Car car, int[][] board, boolean[][] visited, int row, int col, int cost) {
        if (dist[row][col] != Integer.MAX_VALUE && dist[row][col] < cost) {
            return;
        }

        dist[row][col] = Math.min(dist[row][col], cost);
        if (cost >= result) return;


        if (row == visited.length - 1 && col == visited[0].length - 1) {
            result = Math.min(result, car.sumCost());
            return;
        }


        visited[row][col] = true;


        for (int i = 0; i < 4; i++) {
            if (car.count == 0) {
                car.direction = null;
            }
            car.count++;
            if (row + posX[i] >= 0 && row + posX[i] <= board.length - 1 &&
                col + posY[i] >= 0 && col + posY[i] <= board[0].length - 1 &&
                board[row + posX[i]][col + posY[i]] != 1 && visited[row + posX[i]][col + posY[i]] == false) {


                String direction = car.direction;
                car.plusStraight();

                if (direction == null) {
                    if (i == 0 || i == 2) {
                        car.direction = "lr";
                    } else {
                        car.direction = "ud";
                    }
                } else {
                    if ("lr".equals(direction) && (i == 1 || i == 3)) {
                        car.direction = "ud";
                        car.plusCorner();
                    } else if ("ud".equals(direction) && (i == 0 || i == 2)) {
                        car.direction = "lr";
                        car.plusCorner();
                    }
                }

                find(car, board, visited, row + posX[i], col + posY[i], car.sumCost());
                visited[row + posX[i]][col + posY[i]] = false;
                car.minusStraight();


                if (direction != null && !direction.equals(car.direction)) {
                    car.direction = direction;
                    car.minusCorner();
                }
            }
            car.count--;
        }
    }

    class Car {
        int straight;
        int corner;
        int count;
        String direction;

        public Car() {
            this.straight = 0;
            this.corner = 0;
            this.count = 0;
            this.direction = null;
        }

        public int sumCost() {
            return (this.straight * 100) + (this.corner * 500);
        }

        public void plusCorner() {
            this.corner++;
        }

        public void minusCorner() {
            this.corner--;
        }

        public void plusStraight() {
            this.straight++;
        }

        public void minusStraight() {
            this.straight--;
        }
    }
}
