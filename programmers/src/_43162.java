import java.util.LinkedList;
import java.util.Queue;

// https://programmers.co.kr/learn/courses/30/lessons/43162
public class _43162 {
    boolean[] visit;
    int answer = 0;
    public int solution(int n, int[][] computers) {
        visit = new boolean[n];

        for (int i = 0; i < n; i++) {
            if (visit[i]) {
                continue;
            }

            Queue<Integer> q = new LinkedList<>();
            q.offer((i));
            visit[i] = true;
            answer++;

            while (q.isEmpty() == false) {
                int computer = q.poll();

                for (int j = 0; j < n; j++) {
                    if (j == computer || visit[j]) {
                        continue;
                    }
                    if (computers[computer][j] == 1) {
                        q.offer(j);
                        visit[j] = true;
                    }
                }
            }
        }

        return answer;
    }
}
