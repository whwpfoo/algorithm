import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

/**
 * 두 개 뽑아서 더하기: https://programmers.co.kr/learn/courses/30/lessons/68644
 */
public class _68644 {
    public static void main(String[] args) {
        int[] numbers = new int[] {2, 1, 3, 4,1};
        System.out.println(Arrays.toString(solution(numbers)));
    }

    public static int[] solution(int[] numbers) {
        Set<Integer> sumOfNumberSet = new TreeSet<>();

        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = i; j < numbers.length - 1; j++) {
                sumOfNumberSet.add(numbers[i] + numbers[j + 1]);
            }
        }

        return sumOfNumberSet.stream().mapToInt(Integer::intValue).toArray();
    }
}
