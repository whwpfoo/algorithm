
/**
 * 이상한 문자 만들기: https://programmers.co.kr/learn/courses/30/lessons/12930
 * 문자열 s는 한 개 이상의 단어로 구성되어 있습니다.
 * 각 단어는 하나 이상의 공백문자로 구분되어 있습니다.
 *
 * 각 단어의 짝수번째 알파벳은 대문자로,
 * 홀수번째 알파벳은 소문자로 바꾼 문자열을 리턴하는 함수,
 * solution을 완성하세요.
 */
public class _12930 {
    public String solution(String s) {
        StringBuilder sb = new StringBuilder();
        String[] wordArr = s.split(" ", -1);

        for (int i = 0; i < wordArr.length; i++) {
            String word = wordArr[i];
            String[] textArr = word.split("", -1);

            for (int j = 0; j < textArr.length; j++) {
                if (j % 2 == 0) {
                    textArr[j] = textArr[j].toUpperCase();
                } else {
                    textArr[j] = textArr[j].toLowerCase();
                }

                sb.append(textArr[j]);
            }

            if (i != wordArr.length -1) sb.append(" ");
        }

        return sb.toString();
    }
}
