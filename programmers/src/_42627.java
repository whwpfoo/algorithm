import java.util.Comparator;
import java.util.PriorityQueue;

// https://programmers.co.kr/learn/courses/30/lessons/42627?language=java
public class _42627 {
    class Job {
        int requestTime;
        int expendTime;

        Job(int requestTime, int expendTime) {
            this.requestTime = requestTime;
            this.expendTime = expendTime;
        }
    }

    public int solution(int[][] jobs) {
        int answer = 0;
        int count = 0;
        int time = 0;

        PriorityQueue<Job> requestPriority = new PriorityQueue<>(Comparator.comparingInt(o -> o.requestTime));
        for (int[] job : jobs) {
            int request = job[0];
            int expend = job[1];
            requestPriority.offer(new Job(request, expend));
        }

        PriorityQueue<Job> expendPriority = new PriorityQueue<>(Comparator.comparingInt(o -> o.expendTime));

        while (count < jobs.length) {
            while (!requestPriority.isEmpty() && time >= requestPriority.peek().requestTime) {
                expendPriority.offer(requestPriority.poll());
            }

            if (!expendPriority.isEmpty()) {
                Job job = expendPriority.poll();
                answer += job.expendTime + (time - job.requestTime);
                time += job.expendTime;
                count++;
            } else {
                time++;
            }
        }

        return answer / count;
    }
}
