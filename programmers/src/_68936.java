// https://programmers.co.kr/learn/courses/30/lessons/68936
public class _68936 {
    public static void main(String[] args) {
        _68936 obj = new _68936();
        int[][] param = {
                {1, 1, 0, 0},
                {1, 0, 0, 0},
                {1, 0, 0, 1},
                {1, 1, 1, 1}
        };
        obj.solution(param);

        System.out.println(obj.zeroCount);
        System.out.println(obj.oneCount);
    }

    int zeroCount = 0;
    int oneCount = 0;

    public int[] solution(int[][] arr) {
        int length = arr.length;
        find(arr, 0, 0, length);
        return new int[] {zeroCount, oneCount};
    }

    public void find(int[][] arr, int x, int y, int length) {
        int value = arr[x][y];

        if (length == 1) {
            if (value == 0) zeroCount++;
            if (value == 1) oneCount++;
            return;
        }

        if (isSame(arr, x, y, length)) {
            if (value == 0) zeroCount++;
            if (value == 1) oneCount++;
            return;
        }

        int limit = length / 2;
        find(arr, x, y, limit);
        find(arr, x, y + limit, limit);
        find(arr, x + limit, y, limit);
        find(arr, x + limit, y + limit, limit);
    }

    public boolean isSame(int[][] arr, int row, int col, int limit) {
        int standard = arr[row][col];
        for (int i = row; i < row + limit; i++) {
            for (int j = col; j < col + limit; j++) {
                if (standard != arr[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }
}
