// https://programmers.co.kr/learn/courses/30/lessons/72410 - 신규 아이디 추천
public class _72410 {
    public String solution(String new_id) {
        return new IdFactory()
                .toLowerCase(new_id)
                .removeSpecialCharacter()
                .removeDuplicateDot()
                .removeFirstDot()
                .removeLastDot()
                .addWordIfEmpty()
                .subStringIfOverLength()
                .resizeIfLessThanMinLength()
                .result();
    }

    class IdFactory {
        StringBuilder id;

        public IdFactory() {
            this.id = new StringBuilder();
        }

        public IdFactory toLowerCase(String new_id) {
            id.append(new_id.toLowerCase());
            return this;
        }

        public IdFactory removeSpecialCharacter() {
            for (int i = 0; i < id.length(); i++) {
                char c = id.charAt(i);
                if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || c == '-' || c == '_' || c =='.') {
                    continue;
                }
                id.deleteCharAt(i);
                i--;
            }
            return this;
        }

        public IdFactory removeDuplicateDot() {
            for (int i = 0; i < id.length() - 1; i++) {
                char c = id.charAt(i);
                if (c != '.') {
                    continue;
                }
                char c2 = id.charAt(i + 1);
                if (c == c2) {
                    id.deleteCharAt(i + 1);
                    i--;
                }
            }
            return this;
        }

        public IdFactory removeFirstDot() {
            if (id.length() > 0) {
                if (id.charAt(0) == '.') {
                    id.deleteCharAt(0);
                }
            }
            return this;
        }

        public IdFactory removeLastDot() {
            if (id.length() > 0) {
                if (id.charAt(id.length() - 1) == '.') {
                    id.deleteCharAt(id.length() - 1);
                }
            }
            return this;
        }

        public IdFactory addWordIfEmpty() {
            if (id.length() == 0) {
                id.append("a");
            }
            return this;
        }

        public IdFactory subStringIfOverLength() {
            if (id.length() >= 16) {
                id.delete(15, id.length());
                if (id.charAt(id.length() - 1) == '.') {
                    id.deleteCharAt(id.length() - 1);
                }
            }
            return this;
        }

        public IdFactory resizeIfLessThanMinLength() {
            if (id.length() <= 2) {
                char c = id.charAt(id.length() - 1);
                while (id.length() < 3) {
                    id.append(c);
                }
            }
            return this;
        }

        public String result() {
            return id.toString();
        }
    }
}
