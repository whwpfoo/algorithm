import java.util.*;

public class _43105 {
    public static void main(String[] args) {
        _43105 o = new _43105();
//        int[][] t = {{7}, {3, 8}, {8, 1, 0}, {2, 7, 4, 4}, {4, 5, 2, 6, 5}};
        int[][] t = {{1},{1,2},{1,1,3},{9,1,8,1}};
        System.out.println(o.solution(t));
    }

    public int solution(int[][] triangle) {
        Map<Integer, List<Integer>> m = new HashMap<>();
        List<Integer> start = new ArrayList<>();

        start.add(triangle[0][0]);
        m.put(0, start);

        for (int i = 1; i < triangle.length; i++) {
            int[] arr = triangle[i];
            List<Integer> sumList = new ArrayList<>();
            List<Integer> beforeSumList = m.get(i - 1);

            for (int j = 0; j < arr.length; j++) {
                if (j == 0) {
                    sumList.add(arr[j] + beforeSumList.get(0));
                } else if (j == arr.length - 1) {
                    sumList.add(arr[j] + beforeSumList.get(beforeSumList.size() - 1));
                } else {
                    sumList.add(Math.max(arr[j] + beforeSumList.get(j), arr[j] + beforeSumList.get(j - 1)));
                }
            }

            m.put(i, sumList);
        }

        List<Integer> last = m.get(triangle.length - 1);
        Collections.sort(last);

        return last.get(last.size() - 1);
    }
}
