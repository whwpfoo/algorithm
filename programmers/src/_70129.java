import java.util.Arrays;

// https://programmers.co.kr/learn/courses/30/lessons/70129
public class _70129 {
    public static void main(String[] args) {
        String arg = "1111111";
        _70129 obj = new _70129();
        int[] solution = obj.solution(arg);
        System.out.println(Arrays.toString(solution));
    }
    public int[] solution(String s) {
        StringBuilder sb = new StringBuilder(s);
        int convert = 0;
        int remove = 0;

        while (sb.length() != 1) {
            String removeZero = sb.toString().replaceAll("0", "");

            int originLength = sb.length();
            int changeLength = removeZero.length();

            remove += originLength - changeLength;
            sb.setLength(0);
            sb.append(Integer.toBinaryString(changeLength));
            convert++;
        }

        return new int[] {convert, remove};
    }
}
