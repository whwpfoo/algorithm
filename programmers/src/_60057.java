import java.util.LinkedList;
import java.util.Queue;

/**
 * 문자열 압축: https://programmers.co.kr/learn/courses/30/lessons/60057
 */
public class _60057 {
    public static void main(String[] args) {
        String s = "aabbaccc";
        int result = solution(s);
        System.out.println(result);
    }

    public static int solution(String s) {
        int sSize = s.length();

        int answer = Integer.MAX_VALUE;
        int start = 1;
        int limit = (sSize / 2) + 1;

        while (start <= limit) {
            Queue<String> list = new LinkedList<>();

            for (int i = 0; i <= sSize - 1; i += start) {
                int end = i + start > sSize ? sSize : i + start;
                list.offer(s.substring(i, end));
            }

            StringBuilder sb = new StringBuilder();
            int count = 1;

            for ( ; !list.isEmpty();) {
                String word = list.poll();

                for (String w : list) {
                    if (!word.equals(w)) break;
                    count++;
                }

                if (count > 1) {
                    sb.append(count + word);
                    for (; count > 1; count--) {
                        list.remove();
                    }
                    continue;
                }

                sb.append(word);
            }

            answer = Math.min(answer, sb.length());
            start++;
        }

        return answer;
    }
}
