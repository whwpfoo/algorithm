import java.util.*;

/**
 * https://programmers.co.kr/learn/courses/30/lessons/67258
 */
public class _67258 {
    public static void main(String[] args) {

    }

    public int[] solution(String[] gems) {
        Set<String> set = new HashSet<>(Arrays.asList(gems));
        Map<String, Integer> map = new HashMap<>();
        List<Position> list = new ArrayList<>();
        int back = 0;
        int forward = 0;
        int gemsSize = set.size();
        while(true) {
            if (forward == gems.length)
                break;
            if (map.size() == gemsSize) {
                list.add(new Position(forward, back));
                map.put(gems[forward], map.get(gems[forward]) - 1);
                int count = map.get(gems[forward]);
                if (count == 0) {
                    map.remove(gems[forward]);
                }
                forward++;
            } else {
                if (back == gems.length)
                    break;
                map.put(gems[back], map.getOrDefault(gems[back], 0) + 1);
                back++;
            }
        }
        Collections.sort(list);
        Position p = list.get(0);
        return new int[] {p.x+1, p.y};
    }
    class Position implements Comparable<Position> {
        int x;
        int y;
        public Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int compareTo(Position o) {
            int len1 = this.y - this.x;
            int len2 = o.y - o.x;

            if (len1 == len2)
                return this.x - o.x;
            return len1 - len2;
        }
    }
}
