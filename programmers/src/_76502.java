import java.util.*;

// https://programmers.co.kr/learn/courses/30/lessons/76502
public class _76502 {
    public static void main(String[] args) {
        _76502 o = new _76502();
        String let = "[)(]";
        int solution = o.solution(let);
        System.out.println(solution);
    }

    Map<String, Object> bracket= new HashMap<>();

    public int solution(String s) {
        int length = s.length();

        if (length % 2 != 0)
            return 0;

        bracket.put("()", new Object());
        bracket.put("[]", new Object());
        bracket.put("{}", new Object());

        int answer = 0; int start = 0;
        do {
            if (isCorrect(s)) {
                answer++;
            }
            s = move(s);
            start++;
        } while (start < s.length());
        return answer;
    }

    public String move(String s) {
        return s.substring(1) + s.substring(0, 1);
    }

    public boolean isCorrect(String s) {
        char head = s.charAt(0);
        char tail = s.charAt(s.length() - 1);

        if (head == ')' || head == ']' || head == '}') return false;
        if (tail == '(' || tail == '[' || tail == '{') return false;

        Stack<Character> open = new Stack<>();
        int start = 0;
        while (start < s.length()) {
            int pointer = start;
            while (pointer < s.length()
                    && (s.charAt(pointer) == '(' || s.charAt(pointer) == '{' || s.charAt(pointer) == '[')) {
                open.push(s.charAt(pointer));
                pointer++;
            }

            if (open.isEmpty()) {
                return false;
            }

            while (true) {
                String o = open.peek().toString();
                String c = Character.toString(s.charAt(pointer));
                if (bracket.get(o+c) != null) {
                    open.pop();
                    pointer++;
                }
                break;
            }

            if (start == pointer) {
                return false;
            }

            start = pointer;
        }

        return true;
    }
}
