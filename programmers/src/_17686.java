import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * [3차] 파일명 정렬: https://programmers.co.kr/learn/courses/30/lessons/17686
 * 파일명	                      HEAD	    NUMBER	    TAIL
 * foo9.txt	                      foo	      9	        .txt
 * foo010bar020.zip	              foo	     010	  bar020.zip
 */
public class _17686 {

    public static void main(String[] args) {
        _17686 obj = new _17686();
        String[] files = {"img12.png", "img10.png", "img02.png", "img1.png", "IMG01.GIF", "img2.JPG"};
        String[] files2 = {"F-5", "B-50", "A-10", "F-14"};
        String[] file3 = {"MUZI01.zip", "muzi1.png"};
        String[] file4 = {"a2", "A1", "AA12"};
        String[] result = obj.solution(file4);

        System.out.println(Arrays.toString(result));
    }

    public  String[] solution(String[] files) {
        FileInfo[] fileArr = new FileInfo[files.length];

        Pattern pattern = Pattern.compile("([a-zA-Z\\-\\s]+)([0-9]{1,5})(.*)");
        Matcher matcher = null;
        int index = 0;

        for (String file : files) {
            matcher = pattern.matcher(file);

            while (matcher.find()) {
                FileInfo f = new FileInfo(matcher.group(1), Integer.parseInt(matcher.group(2)), matcher.group(2), matcher.group(3));
                fileArr[index++] = f;
            }
        }

        Arrays.sort(fileArr);

        for (int i = 0; i < fileArr.length; i++) {
            files[i] = fileArr[i].toString();
        }

        return files;
    }

    public class FileInfo implements Comparable<FileInfo> {
        public String head;
        public Integer number;
        public String number2String;
        public String tail;

        public FileInfo(String head, Integer number, String number2String, String tail) {
            this.head = head;
            this.number = number;
            this.number2String = number2String;
            this.tail = tail == null ? "" : tail;
        }

        @Override
        public int compareTo(FileInfo o) {
            return this.head.toUpperCase().equalsIgnoreCase(o.head.toUpperCase())
                    ? this.number - o.number : this.head.toUpperCase().compareTo(o.head.toUpperCase());
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.head).append(this.number2String).append(this.tail);
            return sb.toString();
        }
    }
}
