/**
 * 수박수박수박수박수박수?: https://programmers.co.kr/learn/courses/30/lessons/12922
 */
public class _12922 {
    //짝수인덱스 : 수
    //홀수인덱스 : 박
    public String solution(int n) {
        StringBuilder sb = new StringBuilder();

        int index = 0;

        while (index < n) {
            if (index % 2 == 0) {
                sb.append("수");
            } else {
                sb.append("박");
            }

            index++;
        }

        return sb.toString();
    }
}
