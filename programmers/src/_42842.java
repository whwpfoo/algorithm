/**
 * 카펫: https://programmers.co.kr/learn/courses/30/lessons/42842
 */
public class _42842 {
    public static void main(String[] args) {
        solution(10, 2);

        int[] answer = {
                (int)(1+Math.sqrt(1*1-4*1))/2,
                (int)(1-Math.sqrt(1*1-4*1))/2
        };

    }
    public static int[] solution(int brown, int yellow) {
        // 가로 >= 세로
        int[] answer = new int[2];

        for (int i = yellow; i >= 1; i--) {
            if (yellow % i == 0) {
                int value = yellow / i;

                if (i > value) {
                    int horizontal = 2 * i;
                    int vertical = 2 * value;

                    if (horizontal + vertical + 4 == brown) {
                        answer[0] = i + 2;
                        answer[1] = value + 2;
                        break;
                    }
                }
            }
        }

        return answer;
    }
}
