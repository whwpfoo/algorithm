import java.util.Arrays;

/**
 * 구명보트: https://programmers.co.kr/learn/courses/30/lessons/42885
 */
public class _42885 {
    public static void main(String[] args) {
        int[] people = {10, 20, 30, 40, 50, 60, 70, 80, 90};
        int limit = 100;
        int result = solution(people, limit);

        System.out.println(result);
    }

    public static int solution(int[] people, int limit) {
        Arrays.sort(people);

        int answer = 0;
        int start = 0;
        int end = people.length - 1;

        while (start < end) {
            if (people[end--] + people[start] <= limit) {
                start++;
                answer++;
                continue;
            }
            answer++;
        }

        return answer + end - start + 1;
    }
}
