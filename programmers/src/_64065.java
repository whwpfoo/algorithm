import java.util.HashMap;
import java.util.Map;

/**
 * 튜플: https://programmers.co.kr/learn/courses/30/lessons/64065
 */
public class _64065 {
    public static void main(String[] args) {

    }

    public int[] solution(String s) {
        Map<String, Integer> sMap = new HashMap<String, Integer>();
        String[] sArray = s.split("},");

        for (String numbers : sArray) {
            String[] temp = numbers.replace("}", "").replace("{", "").split(",");

            for (String number : temp) {
                Integer numberCount = sMap.get(number);

                if (numberCount == null) {
                    sMap.put(number, 1);
                } else {
                    sMap.put(number, numberCount + 1);
                }
            }
        }

        return sMap.entrySet().stream()
                .sorted((o1, o2) -> o2.getValue() - o1.getValue())
                .mapToInt(value -> Integer.parseInt(value.getKey()))
                .toArray();
    }
}
