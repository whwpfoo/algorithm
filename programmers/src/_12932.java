import java.util.Arrays;

/**
 * 자연수 뒤집어 배열로 만들기: https://programmers.co.kr/learn/courses/30/lessons/12932
 */
public class _12932 {
    public static void main(String[] args) {
        long n = 12345;
        int[] result = new StringBuilder().append(n).reverse().chars().map(Character::getNumericValue).toArray();

        System.out.println(Arrays.toString(result));
    }

    public int[] solution(long n) {
        return new StringBuilder().append(n).reverse().chars().map(Character::getNumericValue).toArray();
    }
}
