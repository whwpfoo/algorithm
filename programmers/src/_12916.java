/**
 * 문자열 내 p와 y의 개수: https://programmers.co.kr/learn/courses/30/lessons/12916
 */
public class _12916 {
    boolean solution(String s) {
        int deleteP = s.replaceAll("p", "").replaceAll("P", "").length();
        int deleteY = s.replaceAll("y", "").replaceAll("Y", "").length();

        return deleteP == deleteY ? true : false;
    }
}
