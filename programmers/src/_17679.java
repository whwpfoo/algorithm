import java.util.LinkedList;
import java.util.Queue;

/**
 * [1차] 프렌즈4블록: https://programmers.co.kr/learn/courses/30/lessons/17679
 */
public class _17679 {
    public static void main(String[] args) {
        int m = 6;
        int n = 6;
        String[] board = new String[] {
                "AABBEE","AAAEEE","VAAEEV","AABBEE","AACCEE","VVCCEE"
        };
        int result = solution(m, n, board);
        System.out.println(result);
    }

    public static Queue<Integer> q = new LinkedList<>();
    public static String[][] gameBoard;
    public static int count;

    public static int solution(int m, int n, String[] board) {
        gameBoard = new String[m][n];
        int boardLength = board.length;

        for (int i = 0; i < boardLength; i++) {
            String[] array = board[i].split("");
            int arrayLength = array.length;

            for (int j = 0; j < arrayLength; j++) {
                gameBoard[i][j] = array[j];
            }
        }

        main:
        while (true) {
            for (int i = 0; i < m - 1; i++) {
                for (int j = 0; j < n - 1; j++) {
                    checkBlock(gameBoard, i, j);
                }
            }

            if (q.isEmpty()) break main;
            fillSpace(gameBoard);
            fillMember(gameBoard);
            q.clear();
        }

        return count;
    }

    public static void checkBlock(String[][] board, int row, int column) {
        String friend = board[row][column];

        if (friend == null || friend.equals("")) { return; }

        int rowLength = row + 2;
        int colLength = column + 2;

        for (int i = row; i < rowLength; i++) {
            for (int j = column; j < colLength; j++) {
                if (friend.equals(board[i][j]) == false) {
                    return;
                }
            }
        }
        q.offer(row);
        q.offer(column);
    }

    public static void fillSpace(String[][] board) {
        while (q.isEmpty() == false) {
            int row = q.poll();
            int column = q.poll();

            int rowLength = row + 2;
            int colLength = column + 2;

            for (int i = row; i < rowLength; i++) {
                for (int j = column; j < colLength; j++) {
                    if (board[i][j] != null &&
                        board[i][j].equals("") == false) {
                        board[i][j] = "";
                        count++;
                    }
                }
            }
        }
    }

    public static void fillMember(String[][] board) {
        StringBuilder sb = new StringBuilder();
        int rowLength = board.length;
        int colLength = board[0].length;

        for (int i = 0; i < rowLength; i++) {
            for (int j = 0; j < colLength; j++) {
                if (board[i][j] != null &&
                    board[i][j].equals("") == false) {
                    sb.append(board[i][j]);
                } else {
                    sb.append(".");
                }
            }
            sb.append("|");
        }

        String[] temp = sb.toString().split("\\|");
        String[][] newBoard = new String[temp.length][temp[0].length()];

        int tempColLength = temp[0].length();
        int tempRowLength = temp.length;

        for (int i = 0; i < tempColLength; i++) { // 열
            int tempIdx = tempRowLength - 1;

            for (int j = tempRowLength - 1; j >= 0; j--) { // 행
                String word = String.valueOf(temp[j].charAt(i));

                if (word.equals(".")) {
                    continue;
                } else {
                    newBoard[tempIdx][i] = word;
                    tempIdx--;
                }
            }
        }

        gameBoard = newBoard;
    }
}
