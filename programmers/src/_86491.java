// https://programmers.co.kr/learn/courses/30/lessons/86491
public class _86491 {
    public int solution(int[][] sizes) {
        int max = 0, min = 1001;

        for (int i = 0; i < sizes.length; i++) {
            int w = sizes[i][0], h = sizes[i][1];
            max = Math.max(max, Math.max(w, h));

            if (min == 1001) {
                min = Math.min(w, h);
            } else {
                if (min < Math.min(w, h)) {
                    min = Math.min(w, h);
                }
            }
        }

        return max * min;
    }
}
