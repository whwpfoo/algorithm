import java.util.*;

public class _72412 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        String foo = sb.toString();
        System.out.println(foo);

        if (foo.equals("")) {
            System.out.println("foo is empty");
        }
    }


    public int[] solution(String[] info, String[] query) {
        int[] answer = new int[query.length];


        Map<String, List<Integer>> m = new HashMap<>();
        List<Integer> scoreList = new ArrayList<>();


        for (String let : info) {
            String[] s = let.split(" ");
            String l = s[0];
            String p = s[1];
            String c = s[2];
            String f = s[3];
            int score = Integer.parseInt(s[4]);

            scoreList.add(score);

            for (int i = 0; i < 4; i++) {
                List<Integer> oneCombi = m.getOrDefault(s[i], new ArrayList<>());
                oneCombi.add(score);
                m.put(s[i], oneCombi);

                for (int j = i + 1; j < 4; j++) {
                    List<Integer> twoCombi = m.getOrDefault(s[i] + s[j], new ArrayList<>());
                    twoCombi.add(score);
                    m.put(s[i] + s[j], twoCombi);

                    for (int k = j + 1; k < 4; k++) {
                        List<Integer> threeCombi = m.getOrDefault(s[i] + s[j] + s[k], new ArrayList<>());
                        threeCombi.add(score);
                        m.put(s[i] + s[j] + s[k], threeCombi);
                    }
                }
            }

            List<Integer> fourCombi = m.getOrDefault(l + p + c + f, new ArrayList<>());
            fourCombi.add(score);
            m.put(l + p + c + f, fourCombi);
        }

        for (Map.Entry<String, List<Integer>> e : m.entrySet()) {
            Collections.sort(e.getValue());
        }

        Collections.sort(scoreList);


        int index = 0;
        for (String let : query) {
            StringBuilder sb = new StringBuilder();
            String[] s = let.split(" ");

            for (int i = 0; i < 7; i += 2) {
                if (s[i].equals("-")) {
                    continue;
                }
                sb.append(s[i]);
            }

            String q = sb.toString();
            List<Integer> resultList = sb.length() == 0 ? scoreList : m.getOrDefault(q, new ArrayList<>());

            int score = Integer.parseInt(s[7]);
            int start = 0;
            int end = resultList.size();

            while (start < end) {
                int mid = (start + end) / 2;
                if (resultList.get(mid) < score) {
                    start = mid + 1;
                } else {
                    end = mid;
                }
            }

            int result = resultList.size() - start;
            answer[index++] = result;
        }

        return answer;
    }
}
