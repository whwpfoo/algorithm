
import java.util.Arrays;
import java.util.Stack;

// https://programmers.co.kr/learn/courses/30/lessons/77886
public class _77886 {
    public static void main(String[] args) {
        _77886 obj = new _77886();
        String[] s = {"0111111010"};
        System.out.println(Arrays.toString(obj.solution(s)));
    }
    public String[] solution(String[] s) {
        String[] result = new String[s.length];
        for (int i = 0; i < s.length; i++) {
            result[i] = order(s[i]);
        }
        return result;
    }

    private String order(String word) {
        Stack<Character> stack = new Stack<>();

        int targetCount = 0, size = word.length();

        for (int i = 0; i < size; i++) {
            stack.push(word.charAt(i));
            if (stack.size() > 2) {
                char right = stack.pop();
                char middle = stack.pop();
                char left = stack.pop();
                if (isTarget(right, middle, left)) {
                    targetCount++;
                } else {
                    stack.push(left);
                    stack.push(middle);
                    stack.push(right);
                }
            }
        }

        StringBuilder letter = makeRemainLetter(stack);

        int zeroIdx = -1;
        for (int i = letter.length() - 1; i >= 0; i--) {
            if (letter.charAt(i) != '0') {
                continue;
            }
            zeroIdx = i;
            break;
        }

        while (targetCount > 0) {
            if (zeroIdx != -1) {
                letter.insert(zeroIdx + 1, "110");
            } else {
                letter.insert(0, "110");
            }
            targetCount--;
        }

        return letter.toString();
    }

    public boolean isTarget(char... c) {
        return c[0] == '0' && c[1] == '1' && c[2] == '1';
    }

    public StringBuilder makeRemainLetter(Stack<Character> stack) {
        StringBuilder result = new StringBuilder();
        while (stack.isEmpty() == false) {
            result.insert(0, stack.pop());
        }
        return result;
    }
}
