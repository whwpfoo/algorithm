import java.util.*;

// https://school.programmers.co.kr/learn/courses/30/lessons/138476
public class _138476 {

    public static void main(String[] args) {

        int k = 6;
        int[] arr = new int[] {1, 3, 2, 5, 4, 5, 2, 3};

        _138476 obj = new _138476();
        obj.solution(k, arr);
    }

    public int solution(int k, int[] tangerine) {

        PriorityQueue<Integer> q = new PriorityQueue<>(Collections.reverseOrder());
        getTangerineCountMap(tangerine).values().forEach(v -> q.offer(v));
        int result = 0;

        while (k > 0) {
            k -= q.poll();
            result++;
        }

        return result;
    }

    public Map<Integer, Integer> getTangerineCountMap(int[] tangerines) {

        Map<Integer, Integer> map = new HashMap<>();

        for (int t : tangerines) {
            map.put(t, map.getOrDefault(t, 0) + 1);
        }

        return map;
    }
}
