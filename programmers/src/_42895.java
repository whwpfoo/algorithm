import java.util.*;

// https://programmers.co.kr/learn/courses/30/lessons/42895 - N으로 표현
public class _42895 {
    public static void main(String[] args) {
        _42895 obj = new _42895();
        int n = 2;
        int number = 11;
        int result = obj.solution(n, number);
        System.out.println(result);
    }

    public int solution(int N, int number) {
        if (N == number) {
            return 1;
        }

        // init
        List<Set<Integer>> list = new ArrayList<>();

        for (int i = 0; i <= 7; i++) {
            Set<Integer> set = new HashSet<>();
            list.add(set);
        }

        list.get(0).add(N);
        list.get(1).add(N + N);
        list.get(1).add(N - N);
        list.get(1).add(N * N);
        list.get(1).add(N / N);
        list.get(1).add(Integer.parseInt("" + N + N));


        // i: 1 ~ 7
        for (int i = 2; i < list.size(); i++) {
            Set<Integer> target = list.get(i);
            // 2번째라면 0 - 1 번째까지
            for (int j = 0; j < i; j++) {
                Set<Integer> s1 = list.get(j);
                Set<Integer> s2 = list.get(i - 1 - j);
                for (int num1 : s1) {
                    for (int num2 : s2) {
                        target.add(num1 + num2);
                        target.add(num1 * num2);
                        target.add(num1 - num2);
                        target.add(num2 - num1);
                        if (num2 != 0) target.add(num1 / num2);
                        if (num1 != 0) target.add(num2 / num1);
                    }

                }
            }

            int count = i;
            // 5 50 500
            int value = 0;
            while (count >= 0) {
                value += Math.pow(10, count) * N;
                count--;
            }
            target.add(value);
        }

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).contains(number)) return i + 1;
        }

        return -1;
    }
}
