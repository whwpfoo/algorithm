import java.util.LinkedList;

// https://school.programmers.co.kr/learn/courses/30/lessons/131704
public class _131704 {

    public static void main(String[] args) {

        _131704 obj = new _131704();
        int result = obj.solution(new int[]{4, 3, 1, 2, 5});
        System.out.println(result);
    }

    public int solution(int[] order) {

        LinkedList<Integer> mainQueue = new LinkedList<>();
        LinkedList<Integer> waitStack = new LinkedList<>();
        LinkedList<Integer> orderQueue = new LinkedList<>();

        for (int i = 0; i < order.length; i++) {
            mainQueue.offer(i + 1);
            orderQueue.offer(order[i]);
        }

        while (mainQueue.isEmpty() == false) {
            int box = mainQueue.poll();
            int num = orderQueue.peekFirst();
            if (box != num) waitStack.push(box);
            if (box == num) orderQueue.poll();
            while (waitStack.isEmpty() == false) {
                if ((int)waitStack.peekFirst() == (int)orderQueue.peekFirst()) {
                    waitStack.pop(); orderQueue.poll();
                } else {
                    break;
                }
            }
        }

        return order.length - orderQueue.size();
    }
}
