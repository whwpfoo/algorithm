/**
 * 소수 찾기: https://programmers.co.kr/learn/courses/30/lessons/12921
 */
public class _12921 {
    public int solution(int n) {
        int answer = 0;

        for (int i = 2; i <= n; i++) {
            // i...n 부터 각 숫자의 검사를 진행
            int index = 2;
            boolean isDemical = true;

            while (index < i) {
                if (i % index == 0) {
                    isDemical = false;
                    break;
                }
                index++;
            }

            if (isDemical) answer += 1;
        }

        return answer;
    }

    // solution 의 경우 시간초과 -> 해당 함수는 '에라토스테네스의 체'를 이용한 풀이
    public int solution2(int n) {
        int answer = 0;
        boolean[] arr = new boolean[n+1];

        arr[0] = arr[1] = false;

        for(int i = 2; i <= n; i += 1) {
            arr[i] = true;
        }

        for(int i = 2; i * i <= n; i += 1) {
            for(int j = i * i; j <= n; j += i) {
                arr[j] = false;
            }
        }

        for(int i = 0; i <= n; i += 1) {
            if(true == arr[i]) {
                answer += 1;
            }
        }

        return answer;
    }
}
