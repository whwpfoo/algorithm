import java.util.*;

/**
 * 여행경로
 * https://school.programmers.co.kr/learn/courses/30/lessons/43164
 */
public class _43164 {
    Map<String, List<String[]>> map = new HashMap<>();
    int size;

    public String[] solution(String[][] tickets) {
        Arrays.sort(tickets, Comparator.comparing(v -> v[1]));

        size = tickets.length + 1;
        for (String[] t : tickets) {
            if (!map.containsKey(t[0])) {
                map.put(t[0], new ArrayList<>());
            }
            map.get(t[0]).add(new String[] {t[1], "0"});
        }

        return find("ICN", new ArrayList<>(Arrays.asList("ICN"))).toArray(new String[tickets.length + 1]);
    }

    public List<String> find(String name, List<String> answer) {
        if (map.get(name) == null) {
            return answer;
        }

        List<String[]> list = map.get(name);

        for (int i = 0; i < list.size(); i++) {
            String[] t = list.get(i);
            if ("0".equals(t[1])) {
                t[1] = "1";
                answer.add(t[0]);
                List<String> result = find(t[0], answer);
                if (result.size() == size) {
                    return result;
                }
                answer.remove(answer.size() - 1);
                t[1] = "0";
            }
        }

        return answer;
    }

    public static void main(String[] args) {
        _43164 obj = new _43164();
        String[] result = obj.solution(new String[][]{{"ICN", "JFK"}, {"HND", "IAD"}, {"JFK", "HND"}});
        System.out.println(Arrays.toString(result));
    }
}
