import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Function;

/**
 * https://programmers.co.kr/learn/courses/30/lessons/250121
 */
public class _250121 {
    public int[][] solution(int[][] data, String ext, int val_ext, String sort_by) {
        Function<String, Integer> idx = (v) -> "code".equals(v) ? 0 : "date".equals(v) ? 1 : "maximum".equals(v) ? 2 : 3;
        return Arrays.stream(data)
                .filter(v -> v[idx.apply(ext)] < val_ext)
                .sorted(Comparator.comparingInt(v -> v[idx.apply(sort_by)]))
                .toArray(int[][]::new);
    }

    public static void main(String[] args) {
        _250121 obj = new _250121();
        obj.solution(new int[][] {{1, 20300104, 100, 80}, {2, 20300804, 847, 37}, {3, 20300401, 10, 8}}, "date", 20300501, "remain");
    }
}
