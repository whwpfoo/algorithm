import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

// https://programmers.co.kr/learn/courses/30/lessons/1829 - 카카오프렌즈 컬러링북
public class _1829 {
    // m: 가로 n: 세로
    // picture: m x n 크기
    // 색칠 안된 영역: 0
    // 색칠 된 영역: 0이 아닌 정수
    // 같은 영역 판별여부는: 상하좌우가 이어져 있으면 같은 영역
    // 결과는: [영역의수, 가장큰영역의 수]

    boolean[][] visit;

    int[] dx = {-1, 0, 1, 0};
    int[] dy = {0, 1, 0, -1};

    public int[] solution(int m, int n, int[][] picture) {
        visit = new boolean[m][n];
        int areaCount = 0;
        int maxAreaCount = -1;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (picture[i][j] == 0 || visit[i][j]) {
                    continue;
                }
                areaCount++;

                int areaValue = picture[i][j];
                int tempMaxAreaCount = 1;

                Queue<int[][]> q = new LinkedList<>();
                q.offer(new int[][] {{i, j}});
                visit[i][j] = true;

                while(q.isEmpty() == false) {
                    int[][] pos = q.poll();

                    for (int k = 0; k < 4; k++) {
                        int row = pos[0][0] + dx[k];
                        int col = pos[0][1] + dy[k];

                        if (row < 0 || col < 0 || row > m - 1 || col > n - 1) {
                            continue;
                        }
                        if (visit[row][col] || picture[row][col] == 0 || areaValue != picture[row][col]) {
                            continue;
                        }
                        
                        q.offer(new int[][] {{row, col}});
                        visit[row][col] = true;
                        tempMaxAreaCount++;
                    }
                }
                maxAreaCount = Math.max(maxAreaCount, tempMaxAreaCount); // area find End
            } // col End
        } // row End

        return new int[] {areaCount, maxAreaCount};
    }

    public static void main(String[] args) {
        _1829 obj = new _1829();
        int[][] arr = {{1, 1, 1, 0}, {1, 2, 2, 0}, {1, 0, 0, 1}, {0, 0, 0, 1}, {0, 0, 0, 3}, {0, 0, 0, 3}};
        int[] result = obj.solution(6, 4, arr);
        System.out.println(Arrays.toString(result));
    }
}
