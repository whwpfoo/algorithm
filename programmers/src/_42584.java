/**
 * 주식가격 : https://programmers.co.kr/learn/courses/30/lessons/42584
 */
public class _42584 {
    class Solution {
        public int[] solution(int[] prices) {
            int[] answer = new int[prices.length];
            int second = prices.length - 1;

            for (int i = 0; i < prices.length - 1; i++) {
                int price = prices[i];

                for (int j = i + 1; j < prices.length; j++) {
                    if (price > prices[j]) {
                        answer[i] = j - i;
                        break;
                    }
                    if (j == prices.length - 1) answer[i] = j - i;
                }
            }

            return answer;
        }
    }
}
