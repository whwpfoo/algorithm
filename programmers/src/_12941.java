import java.util.Arrays;

/**
 * 최솟값 만들기: https://programmers.co.kr/learn/courses/30/lessons/12941
 */
public class _12941 {
    public int solution(int []A, int []B) {
        int answer = 0;

        Arrays.sort(A);
        Arrays.sort(B);

        for (int i = 0, j = A.length - 1; i < A.length; i++, j--) {
            answer += A[i] * B[j];
        }

        return answer;
    }
}
