import java.util.Arrays;

// https://school.programmers.co.kr/learn/courses/30/lessons/147354
public class _147354 {

    public int solution(int[][] data, int col, int row_begin, int row_end) {

        int[][] table = sortBy(data, col - 1);
        int result = 0;

        for (int i = row_begin; i <= row_end; i++) {
            final int ROW_IDX = i - 1;
            final int ROW = i;
            result ^= Arrays.stream(table[ROW_IDX]).reduce(0, (s, n) -> s + (n % ROW));
        }

        return result;
    }

    public int[][] sortBy(int[][] list, int col) {

        int[][] copy = deepCopy(list);

        Arrays.sort(copy, (o1, o2) -> {
            if (o1[col] == o2[col]) {
                return o2[0] - o1[0];
            }
            return o1[col] - o2[col];
        });

        return copy;
    }

    private int[][] deepCopy(int[][] arr) {

        int[][] copy = new int[arr.length][arr[0].length];

        for (int i = 0; i < arr.length; i++) {
            copy[i] = arr[i].clone();
        }

        return copy;
    }
}
