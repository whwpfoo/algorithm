import java.util.LinkedList;

// https://programmers.co.kr/learn/courses/30/lessons/42583 - 다리를 지나는 트럭
public class _42583 {
    public static void main(String[] args) {
        _42583 obj = new _42583();
        int length = 2;
        int weigth = 10;
        int[] trucks = {7, 4, 5, 6};

        int result = obj.solution(length, weigth, trucks);
        System.out.println(result);
    }

    public int solution(int bridge_length, int weight, int[] truck_weights) {
        int count = 0;

        int time = bridge_length;
        int limit = weight;
        int idx = 0;

        LinkedList<Truck> q = new LinkedList<>(); // 다리
        q.offer(new Truck(truck_weights[idx++]));
        //count++;

        while (!q.isEmpty()) {
            count++;

            // 다리에 현재 있는 트럭들의 시간을 증가 시켜줘야 함
            for (Truck t : q) {
                t.time++;
            }
            // pop 하는 조건
            Truck out = q.peek();
            if (out.time > time) {
                q.poll();
            }

            // push 하는 조건
            if (idx < truck_weights.length) {
                Truck next = new Truck(truck_weights[idx]);
                int bridgeSize = q.size();

                if (bridgeSize < time) {
                    int total = 0;

                    for (Truck t : q) {
                        total += t.weight;
                    }

                    if (total + next.weight <= limit) {
                        next.time++;
                        q.offer(next);
                        idx++;
                    }
                }
            }
        }

        return count;
    }

    class Truck {
        int time;
        int weight;

        public Truck(int weight) {
            this.time = 0;
            this.weight = weight;
        }
    }
}
