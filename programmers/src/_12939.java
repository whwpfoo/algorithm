import java.util.TreeSet;
import java.util.stream.Stream;

/**
 * 최댓값과 최솟값: https://programmers.co.kr/learn/courses/30/lessons/12939
 */
public class _12939 {
    public String solution(String s) {
        TreeSet<Integer> set = new TreeSet<Integer>();
        Stream.of(s.split(" ")).map(Integer::parseInt).forEach(num -> set.add(num));
        return set.first() + " " + set.last();
    }
}
