/**
 * 스킬트리: https://programmers.co.kr/learn/courses/30/lessons/49993
 */
public class _49993 {
    public static void main(String[] args) {
        String skill = "CBD";
        String[] skills = {"BACDE", "CBADF", "AECB", "BDA"};
        int result = solution(skill, skills);
        System.out.println(result);
    }

    public static int solution(String skill, String[] skill_trees) {
        int answer = 0;
        int skillSize = skill.length();
        int skillTreeSize = skill_trees.length;

        main:
        for (int i = 0; i < skillTreeSize; i++) {
            String skillTree = skill_trees[i];

            for (int j = 0; j < skillSize; j++) {
                skillTree = skillTree.replaceAll(String.valueOf(skill.charAt(j)), j + 1 + ",");
            }

            skillTree = skillTree.replaceAll("[^0-9,]", "");

            if (skillTree.length() == 0) {
                answer += 1;
                continue main;
            }

            String[] skillTreeArray = skillTree.split(",");
            int skillTreeArraySize = skillTreeArray.length;

            if (!skillTreeArray[0].equals("1")) continue main;

            for (int k = 1; k < skillTreeArraySize; k++) {
                if (!skillTreeArray[k].equals(k + 1 + "")) {
                    continue main;
                }
            }

            answer += 1;
        }

        return answer;
    }
}
