import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 실패율: https://programmers.co.kr/learn/courses/30/lessons/42889
 */
public class _42889 {

    public static void main(String[] args) {
        int N = 5;
        int[] stages = {2, 1, 2, 6, 2, 4, 3, 3};
//        기댓값 〉	[3, 4, 2, 1, 5]

        _42889 obj = new _42889();
        int[] result = obj.solution(N, stages);

        System.out.println(Arrays.toString(result));
    }

    public int[] solution(int N, int[] stages) {
        int[] answer = new int[N];

        Map<String, Double> failRateMap = new HashMap<>();
        int currentStage = 1;
        int player = stages.length;

        while (currentStage <= N) {
            int tempStage = currentStage;

            int failPlayer = (int)Arrays.stream(stages)
                    .filter(val -> val == tempStage)
                    .count();

            double failRate = failPlayer == 0 ? 0 : (double)failPlayer / (double)player;

            failRateMap.put(String.valueOf(currentStage), failRate);
            currentStage++;
            player = player - failPlayer;
        }

        List<Map.Entry<String, Double>> resultList = failRateMap.entrySet().stream()
                .sorted((o1, o2) -> {
                    if (o1.getValue() > o2.getValue()) {
                        return -1;
                    } else if (o1.getValue() < o2.getValue()) {
                        return 1;
                    } else {
                        return Integer.compare(Integer.parseInt(o1.getKey()), Integer.parseInt(o2.getKey()));
                    }
                })
                .collect(Collectors.toList());

        for (int i = 0; i < resultList.size(); i++) {
            answer[i] = Integer.parseInt(resultList.get(i).getKey());
        }

        return answer;
    }
}
