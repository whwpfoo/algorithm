import java.math.BigInteger;

/**
 * 멀쩡한 사각형: https://programmers.co.kr/learn/courses/30/lessons/62048
 */
public class _62048 {
    public static void main(String[] args) {

    }

    public long solution(int w, int h) {
        int gcd = 0;
        if (w > h) gcd = gcd(w, h);
        else gcd = gcd(h, w);
        return ((long)w * (long)h) - (w + h - gcd);
    }

    public int gcd(int big, int small) {
        if (small == 0) return big;
        return gcd(small, big % small);
    }
}
