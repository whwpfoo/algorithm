import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * 핸드폰 번호 가리기: https://programmers.co.kr/learn/courses/30/lessons/12948
 */
public class _12948 {

    public String solution(String phone_number) {
        StringBuilder sb = new StringBuilder();
        String[] phoneNumberArr = phone_number.split("");

        IntStream.range(0, phone_number.length() - 4).forEach(idx -> phoneNumberArr[idx] = "*");
        Arrays.stream(phoneNumberArr).forEach(value -> sb.append(value));

        return sb.toString();
    }
}
