import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 가장 많이 받은 선물
 * https://school.programmers.co.kr/learn/courses/30/lessons/258712
 *
 * 두 사람이 선물을 주고받은 기록이 있다면, 이번 달까지 두 사람 사이에 더 많은 선물을 준 사람이 다음 달에 선물을 하나 받습니다.
 * 예를 들어 A가 B에게 선물을 5번 줬고, B가 A에게 선물을 3번 줬다면 다음 달엔 A가 B에게 선물을 하나 받습니다.
 * 두 사람이 선물을 주고받은 기록이 하나도 없거나 주고받은 수가 같다면, 선물 지수가 더 큰 사람이 선물 지수가 더 작은 사람에게 선물을 하나 받습니다.
 * 선물 지수는 이번 달까지 자신이 친구들에게 준 선물의 수에서 받은 선물의 수를 뺀 값입니다.
 * 예를 들어 A가 친구들에게 준 선물이 3개고 받은 선물이 10개라면 A의 선물 지수는 -7입니다. B가 친구들에게 준 선물이 3개고 받은 선물이 2개라면 B의 선물 지수는 1입니다. 만약 A와 B가 선물을 주고받은 적이 없거나 정확히 같은 수로 선물을 주고받았다면, 다음 달엔 B가 A에게 선물을 하나 받습니다.
 * 만약 두 사람의 선물 지수도 같다면 다음 달에 선물을 주고받지 않습니다.
 */
public class _258712 {
    public int solution(String[] friends, String[] gifts) {
        Map<String, Friend> table = new HashMap<>();
        for (String friend : friends) {
            table.put(friend, new Friend());
        }

        for (String gift : gifts) {
            String[] names = gift.split(" ");
            table.get(names[0]).sendTo(names[1]);
            table.get(names[1]).receiveFrom(names[0]);
        }

        for (String name : table.keySet()) {
            Friend source = table.get(name);

            for (String friend : friends) {
                if (name.equals(friend) || source.isTrade(friend))
                    continue;
                Friend target = table.get(friend);
                source.trade(friend);
                target.trade(name);

                int[] log = source.getLogBy(friend);
                if (log == null || log[0] == log[1]) {
                    int sRate = source.getGiftRate();
                    int tRate = target.getGiftRate();
                    if (sRate == tRate)
                        continue;
                    if (sRate > tRate) source.addGiftCount();
                    if (sRate < tRate) target.addGiftCount();
                } else {
                    if (log[0] > log[1]) source.addGiftCount();
                    if (log[0] < log[1]) target.addGiftCount();
                }
            }
        }

        return table.values().stream().sorted((t1, t2) -> t2.getGiftCount() - t1.getGiftCount()).findFirst().get().getGiftCount();
    }

    class Friend {
        int giftRate;
        int giftCount;
        Set<String> giftTrade = new HashSet<>();
        Map<String, int[]> giftLog = new HashMap<>();

        public void trade(String name) {
            giftTrade.add(name);
        }

        public void addGiftCount() { giftCount++; }

        public int getGiftCount() { return giftCount; }

        public int getGiftRate() { return giftRate; }

        public boolean isTrade(String name) {
            return giftTrade.contains(name);
        }

        public void sendTo(String name) {
            init(name);
            giftLog.get(name)[0]++;
            plusGiftRate();
        }

        public void receiveFrom(String name) {
            init(name);
            giftLog.get(name)[1]++;
            minusGiftRate();
        }

        public int[] getLogBy(String name) {
            return giftLog.get(name);
        }

        public void init(String name) {
            if (!giftLog.containsKey(name)) {
                giftLog.put(name, new int[2]);
            }
        }

        public void plusGiftRate() { giftRate++; }

        public void minusGiftRate() { giftRate--; }
    }

    public static void main(String[] args) {
        _258712 obj = new _258712();
        int result = obj.solution(new String[]{"muzi", "ryan", "frodo", "neo"}, new String[]{"muzi frodo", "muzi frodo", "ryan muzi", "ryan muzi", "ryan muzi", "frodo muzi", "frodo ryan", "neo muzi"});
        System.out.println(result);
    }
}
