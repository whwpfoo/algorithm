/**
 * 124 나라의 숫자: https://programmers.co.kr/learn/courses/30/lessons/12899
 */
public class _12899 {
    public static void main(String[] args) {
        int num = 10;
        System.out.println(solution(num));
    }

    public static String solution(int n) {
        String[] array = {"4", "1", "2"};

        StringBuilder sb = new StringBuilder();
        while (n > 0) {
            int remainder = n % 3;
            sb.insert(0, array[remainder]);
            n = n / 3;

            if (remainder == 0) n--;
        }
        return sb.toString();
    }
}
