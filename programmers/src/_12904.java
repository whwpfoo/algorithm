// https://programmers.co.kr/learn/courses/30/lessons/12904
public class _12904 {
    public int solution(String s) {
        int wordLength = s.length();

        if (wordLength == 1) return 1;
        if (isPalindrome(s, 0, s.length())) return wordLength;

        int answer = 1;

        for (int i = 0; i < wordLength; i++) {
            if (wordLength - i < answer) { // 검사하려는 문자의 길이가 현재 저장된 길이 보다 작으면 루프 종료
                break;
            }

            int pointer = wordLength - 1;

            while (pointer - i + 1 > answer) {
                // 시작 문자와 끝문자가 같으면 Palindrome 문자 인지 검사
                if (s.charAt(i) == s.charAt(pointer) && isPalindrome(s, i, pointer - i + 1)) {
                    answer = Math.max(answer, pointer - i + 1);
                    break; // 문자의 뒤에서 부터 탐색하므로 Palindrome 문자가 맞다면 바로 루프 종료
                }
                pointer--;
            }
        }

        return answer;
    }

    public boolean isPalindrome(String word, int start, int length) {
        int s = start, e = start + length - 1;
        while (s < e) {
            if (word.charAt(s) != word.charAt(e)) return false;
            s++; e--;
        }
        return true;
    }
}
