import java.util.*;

/**
 * 프린터: https://programmers.co.kr/learn/courses/30/lessons/42587
 */
public class _42587 {
    public int solution(int[] priorities, int location) {
        Queue<Document> q = new LinkedList<>();

        for (int i = 0; i < priorities.length; i++) {
            q.offer(new Document(priorities[i], i));
        }

        int count = 0;

        for ( ; !q.isEmpty();) {
            Document doc = q.poll();

            boolean isOffer = false;
            for (Document target : q) {
                if (doc.priority < target.priority) {
                    isOffer = true;
                    break;
                }
            }

            if (isOffer) {
                q.offer(doc);
                continue;
            }

            count++;

            if (doc.number == location) {
                break;
            }
        }

        return count;
    }

    static class Document {
        public int priority;
        public int number;

        public Document(int priority, int number) {
            this.priority = priority;
            this.number = number;
        }
    }
}
