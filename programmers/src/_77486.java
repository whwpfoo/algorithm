import java.util.*;
import java.util.stream.IntStream;

// https://programmers.co.kr/learn/courses/30/lessons/77486
public class _77486 {
    public static void main(String[] args) {
        String[] enroll = {"john", "mary", "edward", "sam", "emily", "jaimie", "tod", "young"};
        String[] referral = {"-", "-", "mary", "edward", "mary", "mary", "jaimie", "edward"};
        String[] seller = {"young", "john", "tod", "emily", "mary"};
        int[] amount = {12, 4, 2, 5, 10};
        System.out.println(Arrays.toString(solution(enroll, referral, seller, amount)));
    }

    public static int[] solution(String[] enroll, String[] referral, String[] seller, int[] amount) {
        Map<String, String> referralMap = new HashMap<>();
        Map<String, Node> enrollMap = new HashMap<>();

        for (int i = 0; i < enroll.length; i++) {
            Node person = new Node(enroll[i]);
            enrollMap.put(enroll[i], person);
            if ("-".equals(referral[i])) {
                referralMap.put(enroll[i], "master");
            } else {
                referralMap.put(enroll[i], referral[i]);
            }
        }
        for (int i = 0; i < seller.length; i++) {
            Node person = enrollMap.get(seller[i]);
            int sellPrice = amount[i] * 100;
            while (person != null) {
                Node referralPerson = enrollMap.get(referralMap.get(person.name));
                int divided = (int)(sellPrice * 0.1);
                if (divided > 0) {
                    person.setMoney(sellPrice - divided);
                    sellPrice = divided;
                } else {
                    person.setMoney(sellPrice);
                    break;
                }
                person = referralPerson;
            }
        }
        return IntStream.range(0, enroll.length)
                .map(idx -> enrollMap.get(enroll[idx]).money)
                .toArray();
    }

    static class Node {
        String name;
        int money;

        public Node(String name) {
            this.name = name;
        }

        public void setMoney(int money) {
            this.money += money;
        }
    }
}
