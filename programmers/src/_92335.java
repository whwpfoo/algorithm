import java.util.Arrays;

// https://school.programmers.co.kr/learn/courses/30/lessons/92335
public class _92335 {

    public static void main(String[] args) {

        String s = toRadix(437674, 3);
        System.out.println(s);
    }

    public int solution(int n, int k) {

        String let = toRadix(n, k);
        int answer = 0;

        for (String s : let.split("[0]+")) {
            if (isPrime(Long.parseLong(s))) {
                answer++;
            }
        }

        return answer;
    }

    public static String toRadix(int target, int radix) {

        StringBuilder sb = new StringBuilder();
        long number = target;

        while (number != 0) {
            sb.append(number % radix);
            number = number / radix;
        }

        return sb.reverse().toString();
    }

    private boolean isPrime(long num) {

        if (num == 1) {
            return false;
        }

        for (long i = 2; i * i <= num; i++) {
            if (num % i == 0) {
                return false;
            }
        }

        return true;
    }
}
