import java.util.Arrays;

public class _131128 {
    public static void main(String[] args) {
        _131128 obj = new _131128();
        String result = obj.solution("5525", "552");
        System.out.println(result);
    }
    /**
     * 3 ≤ X, Y의 길이(자릿수) ≤ 3,000,000입니다.
     * X, Y는 0으로 시작하지 않습니다.
     * X, Y의 짝꿍은 상당히 큰 정수일 수 있으므로, 문자열로 반환합니다.
     */
    public String solution(String X, String Y) {
        int[] _x = new int[10], _y = new int[10];
        Arrays.stream(X.split("")).forEach(v -> _x[Integer.parseInt(v)]++);
        Arrays.stream(Y.split("")).forEach(v -> _y[Integer.parseInt(v)]++);

        String result = "";
        for (int i = 9; i >= 0; i--) {
            int xc = _x[i], yc = _y[i];
            if (xc == 0 || yc == 0) continue;
            int repeat = xc > yc ? yc : xc;
            result += String.valueOf(i).repeat(repeat);
        }
        if ("".equals(result)) return "-1";
        if (result.startsWith("0")) return "0";
        return result;
    }
}
