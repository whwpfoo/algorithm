/**
 * 괄호 변환: https://programmers.co.kr/learn/courses/30/lessons/60058
 */
public class _60058 {
    public static void main(String[] args) {

    }

    public String solution(String p) {
        return convert(p);
    }

    String convert(String w) {
        if (w.isEmpty()) return w;

        int open = 0;
        int close = 0;
        int idx;
        int length = w.length();

        for(idx = 0; idx < length; idx++) {
            if(w.charAt(idx) == '(')
                open++;
            else
                close++;

            if(open == close) break;
        }

        String u = w.substring(0, idx + 1);
        String v = w.substring(idx + 1);

        if(isCorrect(u)) {
            u += convert(v);
        } else {
            String temp = "(" + convert(v) + ")";
            u = u.substring(1, u.length()-1);
            u = u.replaceAll("\\(","\\/").replaceAll("\\)","\\(").replaceAll("\\/","\\)");
            temp += u;
            return temp;
        }
        return u;
    }

    boolean isCorrect(String u) {
        while(u.equals("") == false) {
            if(u.charAt(0) == ')') {
                return false;
            }

            u = u.replaceAll("\\(\\)","");
        }
        return true;
    }
}
