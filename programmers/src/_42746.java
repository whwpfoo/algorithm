import java.util.*;

/**
 * 가장 큰 수: https://programmers.co.kr/learn/courses/30/lessons/42746
 */
public class _42746 {

    public static void main(String[] args) {
    }

    public String solution(int[] numbers) {
        StringBuilder answer = new StringBuilder();

        String str_numbers[] = new String[numbers.length];
        for(int i=0; i<str_numbers.length; i++) {
            str_numbers[i] = String.valueOf(numbers[i]);
        }

        Arrays.sort(str_numbers, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return (o2+o1).compareTo(o1+o2);
            }
        });


        if(str_numbers[0].startsWith("0")) {
            answer.append("0");
        } else {
            for(int j=0; j<str_numbers.length; j++) {
                answer.append(str_numbers[j]);
            }
        }

        return answer.toString();
    }
}
