// https://programmers.co.kr/learn/courses/30/lessons/83201?language=java

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 학생의 수      ->  0 <= i <= scores.length - 1
 * 0번 학생의 점수 -> (i, 0)
 * 1번 학생의 점수 -> (i, 1)
 * ...
 *
 */
public class _83201 {
    public static void main(String[] args) {
        _83201 obj = new _83201();
        int[][] scores = {{100,90,98,88,65},{50,45,99,85,77},{47,88,95,80,67},{61,57,100,80,65},{24,90,94,75,65}};

        String result = obj.solution(scores);

        System.out.println(result);
    }

    public String solution(int[][] scores) {
        StringBuilder result = new StringBuilder();
        List<Student> students = new ArrayList<>();

        for (int i = 0; i < scores.length; i++) {
            Student student = new Student();

            for (int j = 0; j < scores[0].length; j++) {
                if (i == j) {
                    student.selfScore = scores[j][i];
                }
                student.addScoreList(scores[j][i]); // 점수
            }
            students.add(student);
        }

        for (Student student : students) {
            student.judgeUnique();
            student.calculateAverage();
            result.append(student.evaluate());
        }

        return result.toString();
    }



    class Student {
        boolean unique;
        int selfScore;
        double average;
        List<Integer> scoreList = new ArrayList<>();

        // 점수 세팅
        public void addScoreList(int score) {
            scoreList.add(score);
        }

        // 유일한 최고점 or 최저점 인지 판단
        public void judgeUnique() {
            Collections.sort(scoreList);
            int minScore = scoreList.get(0);
            int maxScore = scoreList.get(scoreList.size() - 1);
            if (selfScore == minScore && selfScore != scoreList.get(1)) {
                unique = true;
            } else if (selfScore == maxScore && selfScore != scoreList.get(scoreList.size() - 2)) {
                unique = true;
            }
        }

        // 평균 구하기
        public void calculateAverage() {
            int scores = scoreList.size();
            int totalScore = 0;

            for (int score : scoreList) {
                totalScore += score;
            }

            if (unique) {
                average = (totalScore - selfScore) / (scores - 1);
            } else {
                average = totalScore / scores;
            }
        }

        // 평균에 대한 점수 구하기
        public String evaluate() {
            if (average >= 90) {
                return "A";
            } else if (average >= 80 && average < 90) {
                return "B";
            } else if (average >= 70 && average < 80) {
                return "C";
            } else if (average >= 50 && average < 70) {
                return "D";
            } else {
                return "F";
            }
        }
    }
}
