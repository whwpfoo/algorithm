/**
 * https://programmers.co.kr/learn/courses/30/lessons/12952
 */
public class _12952 {
    public static void main(String[] args) {
        
    }

    /**
     * queen 은 위아래, 좌우, 대각선 모두 이동이 가능함
     * queen 이 겹치지지 않도록 둘 수 있는 모든 가능성을 리턴 해야 함
     * 시작점은 0,0
     */

    int[][] board;
    int count;

    public int solution(int n) {
        board = new int[n][n];
        countSafeQuuen(board, 0);
        return count;
    }

    public void countSafeQuuen(int[][] board, int col) {
        if (col >= board.length) {
            count++;
            return;
        }

        for (int row = 0; row < board.length; row++) {
            if (isSafeQueen(board, row, col)) {
                board[row][col] = 1;
                countSafeQuuen(board, col + 1);
                board[row][col] = 0;
            }
        }
    }

    public boolean isSafeQueen(int[][] board, int row, int col) {
        for (int i = 0; i < board.length; i++)
            if (board[row][i] == 1)
                return false;
        for (int i = row, j = col; i >= 0 && j >= 0; i--, j--)
            if (board[i][j] == 1)
                return false;
        for (int i = row, j = col; j >= 0 && i < board.length; i++, j--)
            if (board[i][j] == 1)
                return false;
        return true;
    }
}
