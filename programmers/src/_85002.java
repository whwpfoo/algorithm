import java.util.*;

// https://programmers.co.kr/learn/courses/30/lessons/85002
// 1.전체 승률이 높은 복서의 번호가 앞쪽으로 갑니다. 아직 다른 복서랑 붙어본 적이 없는 복서의 승률은 0%로 취급합니다.
// 2.승률이 동일한 복서의 번호들 중에서는 자신보다 몸무게가 무거운 복서를 이긴 횟수가 많은 복서의 번호가 앞쪽으로 갑니다.
// 3.자신보다 무거운 복서를 이긴 횟수까지 동일한 복서의 번호들 중에서는 자기 몸무게가 무거운 복서의 번호가 앞쪽으로 갑니다.
// 4.자기 몸무게까지 동일한 복서의 번호들 중에서는 작은 번호가 앞쪽으로 갑니다.
public class _85002 {
    public static void main(String[] args) {
        int[] w = {50,82,75,120};
        String[] h ={"NLWL","WNLL","LWNW","WWLN"};
        _85002 obj = new _85002();
        System.out.println(Arrays.toString(obj.solution(w, h)));
    }

    public int[] solution(int[] weights, String[] head2head) {
        Map<Integer, Integer> boxerMap = new HashMap<>();
        List<Boxer> boxerList = new ArrayList<>();

        for (int i = 0; i < weights.length; i++) {
            boxerMap.put(i, weights[i]);
            boxerList.add(new Boxer(i, weights[i], head2head[i]));
        }

        for (Boxer boxer : boxerList) {
            boxer.calc(boxerMap);
        }

        return boxerList.stream().sorted().mapToInt(value -> value.id + 1).toArray();
    }

    static class Boxer implements Comparable<Boxer>{
        int id;
        int weight;
        History history;

        public Boxer(int id, int weight, String matchHistory) {
            this.id = id;
            this.weight = weight;
            this.history = new History(matchHistory);
        }

        public void calc(Map<Integer, Integer> boxerMap) {
            this.history.calcWinRate(id);
            this.history.calcWinCountAboutHeavyBoxer(id, weight, boxerMap);
        }

        @Override
        public int compareTo(Boxer o) {
            if (o.history.winRate != this.history.winRate)
                return Double.compare(o.history.winRate, this.history.winRate);

            if (o.history.winCountAboutHeavyBoxer != this.history.winCountAboutHeavyBoxer)
                return Integer.compare(o.history.winCountAboutHeavyBoxer, this.history.winCountAboutHeavyBoxer);

            if (o.weight != this.weight)
                return Integer.compare(o.weight, this.weight);

            return Integer.compare(this.id, o.id);
        }

        class History {
            double winRate;
            int winCountAboutHeavyBoxer;
            String matches;

            private History(String matchHistory) {
                this.matches = matchHistory;
            }

            public void calcWinRate(int id) {
                String match = this.matches;
                int winCount = 0;
                int matchCount = 0;
                for (int i = 0; i < match.length(); i++) {
                    if (i == id) {
                        continue;
                    }
                    if (match.charAt(i) == 'W') {
                        winCount++;
                    }
                    if (match.charAt(i) != 'N') {
                        matchCount++;
                    }
                }
                if (matchCount != 0) {
                    winRate = (double) winCount / matchCount * 100L;
                }
            }

            public void calcWinCountAboutHeavyBoxer(int id, int weight, Map<Integer, Integer> boxerMap) {
                String match = this.matches;
                for (int i = 0; i < match.length(); i++) {
                    if (i == id) {
                        continue;
                    }
                    if (match.charAt(i) == 'W' && boxerMap.get(i) > weight) {
                        winCountAboutHeavyBoxer++;
                    }
                }
            }
        }
    }
}
