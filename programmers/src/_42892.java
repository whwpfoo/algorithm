import java.util.*;

/**
 * 길 찾기 게임: https://programmers.co.kr/learn/courses/30/lessons/42892?language=java
 * nodeinfo의 길이는 1 이상 10,000 이하이다.
 * nodeinfo[i] 는 i + 1번 노드의 좌표이며, [x축 좌표, y축 좌표] 순으로 들어있다.
 * 모든 노드의 좌표 값은 0 이상 100,000 이하인 정수이다.
 * 트리의 깊이가 1,000 이하인 경우만 입력으로 주어진다.
 * 모든 노드의 좌표는 문제에 주어진 규칙을 따르며, 잘못된 노드 위치가 주어지는 경우는 없다.
 */
public class _42892 {
    public static void main(String[] args) {
        _42892 sol = new _42892();
        int[][] data = {{5,5}, {4,4}, {6,4}, {3,3}, {2,2}, {0,0}};
        int[][] result = sol.solution(data);

        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                System.out.print(result[i][j]);
            }
            System.out.println();
        }
    }

    public int index = 0;

    public int[][] solution(int[][] nodeinfo) {
        List<Node> nodeList = new ArrayList<>();


        // Make Node
        int rowLength = nodeinfo.length;

        for (int i = 0; i < rowLength; i++) {
            int data = i + 1;
            int posX = nodeinfo[i][0];
            int posY = nodeinfo[i][1];

            nodeList.add(new Node(data, posX, posY));
        }

        // Node Sort
        Collections.sort(nodeList);

        Node root = nodeList.get(0);
        Graph g = new Graph();

        for (int i = 1; i < nodeList.size(); i++) {
            g.addNode(root, nodeList.get(i));
        }


        int[][] result = new int[2][nodeList.size()];

        g.preorder(root, result);
        index = 0;
        g.postorder(root, result);


        return result;
    }

    public class Graph {
        public void addNode(Node parent, Node child) {
            int pX = parent.getPositionX();
            int cX = child.getPositionX();

            if (pX > cX) {
                if (parent.getLeft() == null) {
                    parent.setLeft(child);
                } else {
                    addNode(parent.getLeft(), child);
                }
            } else {
                if (parent.getRight() == null) {
                    parent.setRight(child);
                } else {
                    addNode(parent.getRight(), child);
                }
            }
        }

        public void preorder(Node node, int[][] result) {
            if (node != null) {
                result[0][index++] = node.getData();
                if (node.left != null) preorder(node.left, result);
                if (node.right != null) preorder(node.right, result);
            }
        }

        public void postorder(Node node, int[][] result) {
            if (node != null) {
                if (node.left != null) postorder(node.left, result);
                if (node.right != null) postorder(node.right, result);
                result[1][index++] = node.getData();
            }
        }
    }

    public class Node implements Comparable<Node> {
        private int data;
        private int positionX;
        private int positionY;
        private Node left;
        private Node right;

        public Node(int data, int x, int y) {
            this.data = data;
            this.positionX = x;
            this.positionY = y;
        }

        public int getData() {
            return data;
        }

        public void setData(int data) {
            this.data = data;
        }

        public int getPositionX() {
            return positionX;
        }

        public void setPositionX(int positionX) {
            this.positionX = positionX;
        }

        public int getPositionY() {
            return positionY;
        }

        public void setPositionY(int positionY) {
            this.positionY = positionY;
        }

        public Node getLeft() {
            return left;
        }

        public void setLeft(Node left) {
            this.left = left;
        }

        public Node getRight() {
            return right;
        }

        public void setRight(Node right) {
            this.right = right;
        }

        @Override
        public int compareTo(Node o) {
            // y 역순정렬 같으면 x 순정렬
            if (o.positionY - this.positionY == 0) {
                return this.positionX - o.positionX;
            }
            return o.positionY - this.positionY;
        }
    }
}
