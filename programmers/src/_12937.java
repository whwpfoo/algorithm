/**
 * 짝수와 홀수: https://programmers.co.kr/learn/courses/30/lessons/12937
 */
public class _12937 {

    public String solution(int num) {
        return num % 2 == 0 ? "Even" : "Odd";
    }
}
