// https://programmers.co.kr/learn/courses/30/lessons/82612
public class _82612 {
    // price: 기본 이용료
    // money: 가지고 있는 금액
    // count: 놀이 기구 이용 횟수
    // count 만큼 놀이 기구를 탄다고 가정했을 때 현재 가지고 있는 금액에서 부족한 금액을 리턴 (부족하지 않다면 0)
    public long solution(int price, int money, int count) {
        long requiredPrice = 0;
        for (int i = 1; i <= count; i++) {
            requiredPrice += price * i;
        }
        return requiredPrice - money > 0 ? requiredPrice - money : 0;
    }
}
