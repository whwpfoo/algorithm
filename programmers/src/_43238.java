import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 * https://programmers.co.kr/learn/courses/30/lessons/43238
 */
public class _43238 {
    public static void main(String[] args) {
        _43238 obj = new _43238();
        long result = obj.solution(6, new int[]{7, 10});
        System.out.println(result);
    }

    /**
     * 이분법
     * 1. times 의 최소값, 최대값을 구한다
     * 2. start, end 변수에 최소값, 최대값 설정
     *  2-1. n * 최대값: 모든 인원 입국심사를 하는데 걸리는 최대 시간을 구한다
     *  2-2. 최소값: 1
     * 3. 입국심사 중간값을 구한다 (최대값 / 2)
     * 4. 중간값으로 입국심사 할 수 있는 인원수를 구한다
     *  4-1. 인원수가 n 보다 큰 경우 start 값을 중간값 + 1로 설정
     *  4-2. 인원수가 n 보다 작은 경우 end 값을 중간값 - 1로 설정
     * 5. 3번 과정을 반복한다
     * 6. 인원수가 n 과 동일한 경우 스탑
     * 7. end 값을 리턴한다
     */
    public long solution(int n, int[] times) {
        long maxTime = Arrays.stream(times).max().getAsInt();
        long left = 0, right = maxTime * n, result = 0;
        while (left <= right) {
            long mid = (left + right) / 2;
            long done = 0;
            for (int time : times) {
                done = done + (mid / time);
            }

            if (done < n) {
                left = mid + 1;
            } else {
                if (mid <= right)
                    result = mid;
                right = mid - 1;
            }
        }
        return result;
    }
}
