import java.util.ArrayList;
import java.util.List;

/**
 * 피보나치 수: https://programmers.co.kr/learn/courses/30/lessons/12945
 */
public class _12945 {
    public static void main(String[] args) {
    }

    public int solution(int n) {
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i <= n; i++) {
            if (i == 0) list.add(0);
            if (i == 1) list.add(1);
            if (i >= 2) {
                int result = list.get(i - 1) % 1234567 + list.get(i - 2) % 1234567;
                list.add(result);
            }
        }
        int answer = list.get(n) % 1234567;
        return answer;
    }
}
