import java.util.HashMap;
import java.util.Map;

/**
 * 완주하지 못한 선수: https://programmers.co.kr/learn/courses/30/lessons/42576
 */
public class _42576 {

    public static void main(String[] args) {
        String root = "fooabcfoofoo";
        String target = "foo";
        root = root.replace(target, "");

        System.out.println(root);
    }

    public String solution(String[] participant, String[] completion) {
        String result = "";
        Map<String, Integer> participantMap = new HashMap<>();

        for (String player : participant) {
            Integer size = participantMap.get(player);
            participantMap.put(player, (size == null ? 1 : size + 1));
        }

        for (String completePlayer : completion) {
            Integer size = participantMap.get(completePlayer);
            participantMap.put(completePlayer, size - 1);
        }

        for (Map.Entry entry : participantMap.entrySet()) {
            if ((Integer)entry.getValue() == 1) {
                result = (String)entry.getKey();
            }
        }

        return result;
    }
}
