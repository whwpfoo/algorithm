import java.util.Arrays;

/**
 * 가장 큰 정사각형 찾기: https://programmers.co.kr/learn/courses/30/lessons/12905
 */
public class _12905 {
    public static void main(String[] args) {

    }

    public int solution(int [][]board) {
        // 1,1 부터 현재 값이 '1' 이라면
        // 현재값 = 좌측, 상단, 좌측상단 체크하여 제일 작은 값 + 1을 할당
        // 배열이 끝날 때까지 반복

        int row = board.length; // 행
        int col = board[0].length; // 열
        int max = 0;

        if (row < 2 || col < 2) {
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    if (board[i][j] == 1) {
                        max = 1;
                        break;
                    }
                }
            }
        } else {
            for (int i = 1; i < row; i++) {
                for (int j = 1; j < col; j++) {
                    if (board[i][j] == 1) {
                        board[i][j] = Math.min(board[i - 1][j - 1],
                                Math.min(board[i][j - 1], board[i - 1][j])) + 1;
                    }
                    max = Math.max(board[i][j], max);
                }
            }
        }

        return (int)Math.pow(max, 2);
    }
}
