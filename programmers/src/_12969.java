import java.util.Scanner;

/**
 * 직사각형 별찍기: https://programmers.co.kr/learn/courses/30/lessons/12969
 */
public class _12969 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();

        _12969 obj = new _12969();
        obj.printRectangle(a, b);
    }

    public static void printRectangle(int horizontal, int vertical) {
        for (int i = 0; i < vertical; i++) {
            for (int j = 0; j < horizontal; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }


}
