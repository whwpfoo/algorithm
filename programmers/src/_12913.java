import java.util.Arrays;

/**
 * 땅따먹기: https://programmers.co.kr/learn/courses/30/lessons/12913
 */
public class _12913 {
    public static void main(String[] args) {

    }

    public int solution(int[][] land) {
        int row = land.length;

        for (int i = 1; i < row; i++) {
            land[i][0] = Math.max(land[i - 1][1],
                    Math.max(land[i - 1][2], land[i - 1][3])) + land[i][0];
            land[i][1] = Math.max(land[i - 1][0],
                    Math.max(land[i - 1][2], land[i - 1][3])) + land[i][1];
            land[i][2] = Math.max(land[i - 1][0],
                    Math.max(land[i - 1][1], land[i - 1][3])) + land[i][2];
            land[i][3] = Math.max(land[i - 1][0],
                    Math.max(land[i - 1][1], land[i - 1][2])) + land[i][3];
        }
        Arrays.sort(land[row - 1]);
        return land[row - 1][3];
    }
}
