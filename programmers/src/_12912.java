/**
 * 두 정수 사이의 합: https://programmers.co.kr/learn/courses/30/lessons/12912
 */
public class _12912 {
    public long solution(int a, int b) {
        long answer = 0;

        if (isEquals(a, b)) {
            return a;
        }

        for (int i = Integer.min(a, b); i <= Integer.max(a, b); i++) {
            answer += i;
        }

        return answer;
    }

    public static boolean isEquals(int a, int b) {
        return a == b;
    }
}
