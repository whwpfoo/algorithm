/**
 * 약수의 합: https://programmers.co.kr/learn/courses/30/lessons/12928
 * 약수는 1 - n 사이의 정수들
 */
public class _12928 {
    public int solution(int n) {
        int answer = 0;

        for (int i = n; i > 0; i--) {
            if (n % i == 0) answer += i;
        }

        return answer;
    }
}
