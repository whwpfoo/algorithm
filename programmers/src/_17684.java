
import java.util.*;
import java.util.stream.IntStream;

/**
 * [3차] 압축: https://programmers.co.kr/learn/courses/30/lessons/17684
 * 현재 입력(w)	다음 글자(c)	출력	사전 추가(w+c)
 *      K	        A	         11	       27: KA
 *      A	        K	         1	       28: AK
 *      KA	        O	        27	       29: KAO
 *      O		       15
 *
 *      색인 번호	1	2	3	...	24	25	26
 *      단어	    A	B	C	...	X	Y	Z
 */
public class _17684 {

    public static void main(String[] args) {
        solution("KAKAO");
    }

    static int[] solution(String msg) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        char c = 'A';
        for (int i = 0; i < 26; i++) {
            map.put(String.valueOf(c), i + 1);
            c++;
        }
        int n = msg.length();
        // A-Z (1-26) 맵 생성

        StringBuilder sb = new StringBuilder();
        StringBuilder ans = new StringBuilder();

        for (int i = 0; i < n; i++) {
            sb.append(msg.charAt(i));
            if (!map.containsKey(sb.toString())) {
                map.put(sb.toString(), map.size() + 1);
                sb.setLength(sb.length() - 1);
                ans.append(map.get(sb.toString()) + ",");
                sb.setLength(0);
                i--;
            }
        }
        if (sb.length() > 0)
            ans.append(map.get(sb.toString()) + ",");

        String[] str = ans.toString().split(",");

        int[] answer = new int[str.length];
        int ii = 0;
        for (String a : str) {
            answer[ii] = Integer.parseInt(a);
            ii++;
        }

        return answer;
    }
}
