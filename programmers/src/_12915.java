import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 문자열 내 마음대로 정렬하기: https://programmers.co.kr/learn/courses/30/lessons/12915
 */
public class _12915 {
    // string의 n 번째 단어를 기준으로 strings를 정렬
    public String[] solution(String[] strings, int n) {
        Map<String, Character> stringMap = new HashMap<>();

        for (int i = 0; i < strings.length; i++) {
            stringMap.put(strings[i], strings[i].charAt(n));
        }

         strings =  stringMap.entrySet().stream()
                .sorted((o1, o2) ->
                        o1.getValue() == o2.getValue()
                        ? o1.getKey().compareTo(o2.getKey())
                        : Character.compare(o1.getValue(), o2.getValue()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList()).toArray(strings);

        return strings;
    }
}
