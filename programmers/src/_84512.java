// https://programmers.co.kr/learn/courses/30/lessons/84512
public class _84512 {
    public static void main(String[] args) {
        String word = "I";
        _84512 obj = new _84512();
        System.out.println(obj.solution(word));
    }

    private static final String[] ALPHABET = {"A", "E", "I", "O", "U"};
    private int order = 0;
    private boolean isFind = false;

    public int solution(String word) {
        permutation(word, new StringBuilder());
        return order;
    }

    public void permutation(String word, StringBuilder letter) {
        if (letter.length() > 5) {
            order--;
            return;
        }
        if (isSame(word, letter)) {
            isFind = true;
            return;
        }

        for (int i = 0; i < ALPHABET.length; i++) {
            if (isFind == false) {
                letter.append(ALPHABET[i]);
                order++;
                permutation(word, letter);
                letter.deleteCharAt(letter.length() - 1);
            }
        }
    }

    public boolean isSame(String word, StringBuilder letter) {
        return word.equals(letter.toString());
    }
}
