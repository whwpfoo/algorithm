import java.util.Arrays;

/**
 * 하샤드 수: https://programmers.co.kr/learn/courses/30/lessons/12947
 *  정수 x를 입력받아 -> x % 각 자릿수의 합 = 0을 만족하면 하샤드 수라고한다
 */
public class _12947 {

    public static void main(String[] args) {

    }

    public boolean solution(int x) {
        Integer sum = Arrays.stream(String.valueOf(x).split("")).mapToInt(Integer::new).sum();

        boolean answer = x % sum == 0 ? true : false;

        return answer;
    }
}
