import java.util.Arrays;
import java.util.Stack;

// https://programmers.co.kr/learn/courses/30/lessons/68646
public class _68646 {
    public static void main(String[] args) {
        _68646 obj = new _68646();
        int[] a = {-16,27,65,-2,58,-92,-71,-68,-61,-33};
        System.out.println(obj.solution(a));
    }

    public int solution(int[] a) {
        int[] minValues = new int[a.length];
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < a.length; i++) {
            int v = Math.min(min, a[i]);
            minValues[i] = v; min = v;
        }
        int answer = 0;
        for (int i = 1; i < a.length - 1; i++) {
            int standard = a[i];
            int left = minValues[i - 1];
            int right = minValues[a.length - 1 - i];

            if (standard <= left || standard <= right) {
                answer++;
            }
        }
        return answer + 2;
    }
}
