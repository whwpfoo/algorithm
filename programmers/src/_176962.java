import java.util.*;

/**
 * 과제 진행하기
 * https://school.programmers.co.kr/learn/courses/30/lessons/176962
 *
 * 1. 과제는 시작하기로 한 시각이 되면 시작합니다.
 * 2. 새로운 과제를 시작할 시각이 되었을 때, 기존에 진행 중이던 과제가 있다면 진행 중이던 과제를 멈추고 새로운 과제를 시작합니다.
 * 3. 진행중이던 과제를 끝냈을 때, 잠시 멈춘 과제가 있다면, 멈춰둔 과제를 이어서 진행합니다.
 *      - 만약, 과제를 끝낸 시각에 새로 시작해야 되는 과제와 잠시 멈춰둔 과제가 모두 있다면, 새로 시작해야 하는 과제부터 진행합니다.
 * 4. 멈춰둔 과제가 여러 개일 경우, 가장 최근에 멈춘 과제부터 시작합니다.
 *
 * [["korean", "11:40", "30"], ["english", "12:10", "20"], ["math", "12:30", "40"]]
 */
public class _176962 {
    public static void main(String[] args) {
        _176962 obj = new _176962();
        String[] result = obj.solution(new String[][]{{"aaa", "12:00", "20"}, {"bbb", "12:10", "30"}, {"ccc", "12:40", "10"}});
        System.out.println(Arrays.toString(result));
    }

    public String[] solution(String[][] plans) {
        List<Subject> subjects = new ArrayList<>();

        for (String[] plan : plans) {
            subjects.add(new Subject(plan));
        }
        Collections.sort(subjects, Comparator.comparingInt(v -> v.startTime));

        List<String> result = new ArrayList<>();
        Stack<Subject> reserved = new Stack<>();

        for (int i = 0; i < subjects.size() - 1; i++) {
            Subject curr = subjects.get(i);
            Subject next = subjects.get(i + 1);

            int endTime = curr.startTime + curr.workTime;
            if (endTime <= next.startTime) {
                result.add(curr.name);

                int remain = next.startTime - endTime;
                if (remain > 0) {
                    while (!reserved.isEmpty()) {
                        if (remain <= 0)
                            break;

                        Subject t = reserved.peek();
                        remain -= t.workTime;
                        if (remain >= 0) {
                            result.add(reserved.pop().name);
                        } else {
                            t.workTime = Math.abs(remain);
                        }
                    }
                }
            } else {
                curr.workTime = Math.abs(next.startTime - endTime);
                reserved.push(curr);
            }
        }

        result.add(subjects.get(subjects.size() - 1).name);

        while (!reserved.isEmpty()) {
            result.add(reserved.pop().name);
        }

        return result.toArray(new String[reserved.size()]);
    }

    class Subject {
        String name;
        int startTime;
        int workTime;

        public Subject(String[] plan) {
            this.name = plan[0];
            this.workTime = Integer.parseInt(plan[2]);
            String[] time = plan[1].split(":");
            this.startTime = Integer.parseInt(time[0]) * 60 + Integer.parseInt(time[1]);
        }
    }
}
