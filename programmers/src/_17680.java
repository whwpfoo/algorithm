import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 캐시: https://programmers.co.kr/learn/courses/30/lessons/17680
 */
public class _17680 {
    public static void main(String[] args) {
        int size = 3;
        String[] cities = new String[] {
            "Jeju", "Pangyo", "Seoul", "Jeju", "Pangyo", "Seoul", "Jeju", "Pangyo", "Seoul"
        };

        int result = solution(size, cities);
        System.out.println(result);

    }

    public static int solution(int cacheSize, String[] cities) {
        if(cacheSize == 0) return 5 * cities.length;

        int answer = 0;
        Queue<String> q = new LinkedList<>();
        int citySize = cities.length;

        for (int i = 0; i < citySize; i++) {
            String city = cities[i];

            Iterator<String> it = q.iterator();
            boolean isDuplicate = false;
            String target = null;

            while (it.hasNext()) {
                String storedCity = it.next();

                if (city.equalsIgnoreCase(storedCity)) {
                    target = storedCity;
                    isDuplicate = true;
                    break;
                }
            }

            if (isDuplicate) {
                q.remove(target);
                answer += 1;
            } else {
                if (q.size() >= cacheSize) q.poll();
                answer += 5;
            }

            q.offer(city);
        }

        return answer;
    }
}