import java.util.HashMap;
import java.util.Map;

/**
 * N개의 최소공배수: https://programmers.co.kr/learn/courses/30/lessons/12953
 */
public class _12953 {
    public static void main(String[] args) {
        int[] arr = new int[]{1,2,3};
        int result = solution(arr);
        System.out.println(result);

    }

    static Map<Integer, Double> map = new HashMap<>();

    public static int solution(int[] arr) {
        int answer = 1;

        for (int i = 0; i < arr.length; i++) {
            calcMinimumMultiple(arr[i]);
        }

        for (double num : map.values()) {
            answer = answer * (int)num;
        }

        return answer;
    }

    public static void calcMinimumMultiple(int num) {
        int index = 2;
        int indices = 1;

        while (num >= index) {
            if (num % index == 0) {
                num = num / index;
                Double current = map.get(index);

                if (current != null) {
                    map.put(index, Math.max(current, Math.pow(index, indices)));
                } else {
                    map.put(index, Math.pow(index, indices));
                }

                indices += 1;
            } else {
                index++;
                indices = 1;
            }
        }
    }
}
