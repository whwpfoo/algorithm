import java.util.LinkedList;

/**
 * [3차] n진수 게임: https://programmers.co.kr/learn/courses/30/lessons/17687
 */
public class _17687 {

    public String solution(int n, int t, int m, int p) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < t * m; i++) {
            sb.append(change(i, n));
        }

        String[] arr = sb.toString().split("");
        StringBuilder result = new StringBuilder();

        for (int i = p - 1; result.toString().length() < t; i += m) {
            result.append(arr[i]);
        }

        return result.toString();
    }

    public static String change(int num, int su){
        LinkedList stack= new LinkedList();
        String result = "";

        while (num > 0) {
            if (num % su > 9) {
                stack.add((char)(num % su + 55));
            } else {
                stack.add(num % su);
            }

            num = num / su;
        }

        while (!stack.isEmpty()) {
            result+=stack.pollLast();
        }

        if (result.equals("")) {
            result="0";
        }

        return result;
    }
}
