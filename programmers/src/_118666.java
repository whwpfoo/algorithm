import java.util.HashMap;
import java.util.Map;

// https://school.programmers.co.kr/learn/courses/30/lessons/118666
public class _118666 {

    public static void main(String[] args) {
        _118666 obj = new _118666();
        String result = obj.solution(
                new String[]{"AN", "CF", "MJ", "RT", "NA"},
                new int[] {5, 3, 2, 7, 5}
        );
        System.out.println(result);
    }

    public String solution(String[] survey, int[] choices) {

        Map<Character, Integer> map = new HashMap<>();
        Map<Integer, Integer> scoreMap = getScoreMap();

        // survey map
        for (int i = 0; i < choices.length; i++) {
            Character c = selectCharacter(choices[i], survey[i]);
            if (c == null) {
                if (map.containsKey(survey[i].charAt(0)) == false) map.put(survey[i].charAt(0), 0);
                if (map.containsKey(survey[i].charAt(1)) == false) map.put(survey[i].charAt(1), 0);
            } else {
                map.put(c, map.getOrDefault(c, 0) + scoreMap.get(choices[i]));
            }
        }

        StringBuilder sb = new StringBuilder();

        for (String sv : new String[] {"RT", "CF", "JM", "AN"}) {
            Integer sc1 = map.get(sv.charAt(0));
            Integer sc2 = map.get(sv.charAt(1));

            if (sc1 != null && sc2 != null) {
                if (sc1.compareTo(sc2) >= 0) {
                    sb.append(sv.charAt(0));
                } else {
                    sb.append(sv.charAt(1));
                }
            } else if (sc1 == null && sc2 != null) {
                sb.append(sv.charAt(1));
            } else if (sc1 != null && sc2 == null) {
                sb.append(sv.charAt(0));
            } else {
                sb.append(sv.charAt(0));
            }
        }

        return sb.toString();
    }

    private Character selectCharacter(int choice, String character) {

        if (choice < 4) return character.charAt(0);
        if (choice > 4) return character.charAt(1);
        return null;
    }

    private Map<Integer, Integer> getScoreMap() {
        return Map.of(
                1, 3,
                2, 2,
                3, 1,
                4, 0,
                5, 1,
                6, 2,
                7, 3);
    }
}
