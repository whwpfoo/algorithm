import java.util.Arrays;

// https://programmers.co.kr/learn/courses/30/lessons/12938
public class _12938 {
    public static void main(String[] args) {
        _12938 obj = new _12938();
        int n = 2;
        int s = 8;
        System.out.println(Arrays.toString(obj.solution(n, s)));
    }

    public int[] solution(int n, int s) {
        if (s / n == 0) {
            return new int[] {-1};
        }

        int[] result = new int[n];
        int start = 0;
        int value = s / n;

        if (s != n * value) {
            for (int i = 0; i < s % n; i++) {
                result[start++] = value + 1;
            }
        }
        for (int i = start; i < n; i++) {
            result[i] = value;
        }
        Arrays.sort(result);

        return result;
    }
}
