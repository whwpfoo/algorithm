import java.util.Arrays;

// https://school.programmers.co.kr/learn/courses/30/lessons/178870
public class _178870 {

    public static void main(String[] args) {
        _178870 obj = new _178870();
        int[] sequence = new int[] {1, 2, 3, 4, 5};
        int[] result = obj.solution(sequence, 7);
        System.out.println(Arrays.toString(result));
    }

    public int[] solution(int[] sequence, int k) {
        int[] prefixSum = new int[sequence.length + 1];

        for (int i = 0; i < sequence.length; i++) {
            prefixSum[i + 1] = prefixSum[i] + sequence[i];
        }

        int length = Integer.MAX_VALUE;
        int l = 0, r = 0;
        int[] result = new int[2];

        while (l <= r && r < prefixSum.length) {
            int sum = prefixSum[r] - prefixSum[l];
            if (sum == k) {
                if (length > r - l - 1) {
                    length = r - l - 1;
                    result[0] = l; result[1] = r - 1;
                }
            }

            if (sum < k) {
                r++;
            } else {
                l++;
            }
        }

        return result;
    }
}
