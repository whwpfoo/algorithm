import java.util.LinkedList;
import java.util.Queue;

// https://programmers.co.kr/learn/courses/30/lessons/1844
public class _1844 {
    public static void main(String[] args) {
        _1844 obj = new _1844();
        int[][] maps = {{1,0,1,1,1},{1,0,1,0,1},{1,0,1,1,1},{1,1,1,0,1},{0,0,0,0,1}};
        int[][] maps2 = {{1,0,1,1,1},{1,0,1,0,1},{1,0,1,1,1},{1,1,1,0,0},{0,0,0,0,1}};
        int result = obj.solution(maps);
        int result2 = obj.solution2(maps);
        System.out.println(result);
        System.out.println(result2);
    }
    // 1 <= maps.length <= 100
    // 1 <= mapps[0].length <= 100
    // 0: 벽, 1: 길
    // 시작위치: 0,0
    // 종료위치: n-1, m-1
    // 시작위치 부터 종료위치 까지의 최단 거리 리턴
    // 단 종료위치에 도달할 수 없는 경우 -1 리턴

    boolean[][] visited;
    int distance = Integer.MAX_VALUE;
    int[][] direct = {
            {0, 1},
            {0, -1},
            {1, 0},
            {-1, 0}
    };

    // dfs 접근해서 풀었으나 '효율성' 테스트에서 시간초과
    public int solution(int[][] maps) {
        int rowSize = maps.length;
        int colSize = maps[0].length;

        visited = new boolean[rowSize][colSize];
        visited[0][0] = true;
        find(maps, 0, 0, 1, rowSize - 1, colSize - 1);
        return distance == Integer.MAX_VALUE ? -1 : distance;
    }

    // bfs 접근해서 다시 풀었음.. '효율성' 까지 통과
    public int solution2(int[][] maps) {
        int rowSize = maps.length;
        int colSize = maps[0].length;

        visited = new boolean[rowSize][colSize];

        Queue<Position> q = new LinkedList<>();
        Position start = new Position(0, 0, 1);

        q.offer(start);
        visited[0][0] = true;

        while (!q.isEmpty()) {
            Position p = q.poll();
            int x = p.x;
            int y = p.y;

            if (x == rowSize - 1 && y == colSize - 1) {
                distance = distance < p.count ? distance : p.count;
                continue;
            }

            for (int i = 0; i < 4; i++) {
                int row = x + direct[i][0];
                int col = y + direct[i][1];

                if (row < 0 || row > rowSize - 1 || col < 0 || col > colSize - 1) {
                    continue;
                }

                if (maps[row][col] == 0 || visited[row][col]) {
                    continue;
                }

                visited[row][col] = true;
                q.offer(new Position(row, col, p.count + 1));
            }
        }

        return distance == Integer.MAX_VALUE ? -1 : distance;
    }

    class Position {
        int x;
        int y;
        int count;

        public Position(int x, int y, int count) {
            this.x = x;
            this.y = y;
            this.count = count;
        }
    }

    public void find(int[][] maps, int row, int col, int count, int rowSize, int colSize) {
        if (row == rowSize && col == colSize) {
            distance = distance < count ? distance : count;
            return;
        }

        for (int i = 0; i < 4; i++) {
            int x = row + direct[i][0];
            int y = col + direct[i][1];
            // 범위를 벗어남
            if (x < 0 || x > rowSize || y < 0 || y > colSize) {
                continue;
            }
            if (maps[x][y] == 0) {
                continue;
            }
            if (visited[x][y] == false) {
                visited[x][y] = true;
                find(maps, x, y, count + 1, rowSize, colSize);
                visited[x][y] = false;
            }
        }
    }
}
