import java.util.Stack;

/**
 * 크레인 인형뽑기 게임: https://programmers.co.kr/learn/courses/30/lessons/64061
 * board	            moves	      result
 * [0,0,0,0,0]     [1,5,3,5,1,2,1,4]    4
 * [0,0,1,0,3]
 * [0,2,5,0,1]
 * [4,2,4,4,2]
 * [3,5,1,3,1]
 */
public class _64061 {
    public int solution(int[][] board, int[] moves) {
        int answer = 0;
        Stack<Integer> container = new Stack<>();

        for (int i = 0; i < moves.length; i++) {
            int target = moves[i];

            for (int j = 0; j < board.length; j++) {
                if (board[j][target - 1] == 0) {
                    continue;
                }

                if (container.empty()) {
                    container.push(board[j][target - 1]);
                } else {
                    int compareValue = container.peek();

                    if (compareValue == board[j][target - 1]) {
                        container.pop();
                        answer += 2;
                    } else {
                        container.push(board[j][target - 1]);
                    }
                }

                board[j][target - 1] = 0;
                break;
            }
        }

        return answer;
    }
}
