
// https://school.programmers.co.kr/learn/courses/30/lessons/131705
public class _131705 {

    private int result;

    public static void main(String[] args) {
        
        _131705 obj = new _131705();
        int answer = obj.solution(new int[]{-2, 3, 0, 2, -5});
        System.out.println(answer);
    }

    public int solution(int[] number) {

        combination(number, new boolean[number.length], 0,  number.length, 3);
        return result;
    }

    public void combination(int[] number, boolean[] visited, int start, int n, int r) {

        if (r == 0) {
            int sum = 0;
            for (int i = 0; i < visited.length; i++) {
                if (visited[i]) sum += number[i];
            }
            if (sum == 0) result++;
            return;
        }

        for (int i = start; i < n; i++) {
            visited[i] = true;
            combination(number, visited, i + 1, n, r - 1);
            visited[i] = false;
        }
    }
}
