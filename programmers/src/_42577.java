/**
 * 전화번호 목록: https://programmers.co.kr/learn/courses/30/lessons/42577
 */
public class _42577 {
    public boolean solution(String[] phone_book) {
        boolean answer = true;

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < phone_book.length; i++) {
            sb.append("|").append(phone_book[i]);
        }

        for (int i = 0; i < phone_book.length; i++) {
            String phone_book_temp = sb.toString();
            phone_book_temp = phone_book_temp.replace("|" + phone_book[i], "x");
            phone_book_temp = phone_book_temp.replaceAll("[^x]", "").trim();

            if (phone_book_temp.length() > 1) {
                answer = false;
                break;
            }
        }

        return answer;
    }
}
