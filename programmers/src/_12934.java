import java.util.Arrays;

/**
 * 정수 제곱근 판별: https://programmers.co.kr/learn/courses/30/lessons/12934
 */
public class _12934 {

    public static void main(String[] args) {
        _12934 obj = new _12934();
//        obj.solution(3);

        String foo = "0000";
        System.out.println(foo.replaceAll("0", "").length());
    }

    public long solution(long n) {
        String[] sqrtValueArr = String.valueOf(Math.sqrt(n)).split("\\.");

        return sqrtValueArr.length > 1
                ? sqrtValueArr[1].replaceAll("0", "").length() == 0
                ? (long)Math.pow(Double.valueOf(sqrtValueArr[0]) + 1, 2) : -1 : -1;


    }
}
