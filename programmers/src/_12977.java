/**
 * 소수 만들기: https://programmers.co.kr/learn/courses/30/lessons/12977
 */
public class _12977 {
    static boolean[] visited;
    static int answer;

    public int solution(int[] nums) {
        int n = nums.length;
        int r = 3;
        visited = new boolean[n];

        combination(nums, visited, 0, n, r);

        return answer;
    }

    static void combination(int[] nums, boolean[] visited, int start, int n, int r) {
        if(r == 0) {
            if(isPrime(nums, visited, n)) answer += 1;
            return;
        }

        for(int i = start; i < n; i++) {
            visited[i] = true;
            combination(nums, visited, i + 1, n, r - 1);
            visited[i] = false;
        }
    }

    static boolean isPrime(int[] nums, boolean[] visited, int n) {
        int num = 0;

        for (int i = 0; i < n; i++) {
            if (visited[i]) {
                num += nums[i];
            }
        }

        return isPrime(num);
    }

    static boolean isPrime(int num) {
        if (num == 1) return false;
        if (num % 2 == 0) return num == 2 ? true : false;

        for (int i = 3; i <= Math.sqrt(num); i += 2) {
            if (num % i == 0) return false;
        }

        return true;
    }
}
