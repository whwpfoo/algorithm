// https://programmers.co.kr/learn/courses/30/lessons/12907
public class _12907 {
    public static void main(String[] args) {
        _12907 obj = new _12907();
        int n = 5;
        int[] money = {1, 2, 5};
        System.out.println(obj.solution(n, money));
    }

    public int solution(int n, int[] money) {
        int[][] dp = new int[money.length][n + 1];

        // 0원을 거슬러 줘야 하는 경우 초기화
        for (int i = 0; i < money.length; i++) {
            dp[i][0] = 1;
        }

        // 맨 처음 동전으로 거슬러 줄 수 있는지 여부
        for (int i = 1; i < dp[0].length; i++) {
            if (i % money[0] == 0) {
                dp[0][i] = 1;
            }
        }

        for (int i = 1; i < dp.length; i++) {
            for (int j = 1; j < dp[0].length; j++) {
                if (j < money[i]) {
                    dp[i][j] = dp[i - 1][j] % 1000000007;
                } else if (j >= money[i]) {
                    dp[i][j] = (dp[i - 1][j] + dp[i][j - money[i]]) % 1000000007;
                }
            }
        }

        return dp[dp.length - 1][dp[0].length - 1];
    }
}
