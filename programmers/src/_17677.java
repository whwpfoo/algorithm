import java.util.ArrayList;
import java.util.List;

/**
 * [1차] 뉴스 클러스터링: https://programmers.co.kr/learn/courses/30/lessons/17677
 */
public class _17677 {
    public static void main(String[] args) {
        String str1 = "FRANCE";
        String str2 = "french";
        int result = solution(str1, str2);
        System.out.println(result);
    }

    public static List<String> A = new ArrayList<>();
    public static List<String> B = new ArrayList<>();

    public static int solution(String str1, String str2) {
        A = set(str1);
        B = set(str2);

        int aListSize = A.size();
        int bListSize = B.size();

        if (aListSize == 0 && bListSize == 0) return 65536;

        double intersection = intersectionCount(aListSize, bListSize);
        double union = aListSize + bListSize - intersection;

        return (int)((intersection / union) * 65536);
    }

    public static int intersectionCount(int aListSize, int bListSize) {
        List<String> copy = new ArrayList<>();
        int result = 0;

        if (aListSize > bListSize) {
            copy.addAll(A);
            result = duplicateWordCounting(B, result, copy);
        } else {
            copy.addAll(B);
            result = duplicateWordCounting(A, result, copy);
        }

        return result;
    }

    private static int duplicateWordCounting(List<String> list, int result, List<String> copy) {
        for (String word : list) {
            if (copy.contains(word)) {
                copy.remove(word);
                result++;
            }
        }
        return result;
    }

    public static List<String> set(String foo) {
        List<String> list = new ArrayList<>();
        int length = foo.length() - 1;

        for (int i = 0; i < length; i++) {
            if (!validateString(foo.charAt(i))) continue;
            if (!validateString(foo.charAt(i + 1))) continue;

            list.add(foo.substring(i, i + 2).toUpperCase());
        }

        return list;
    }

    public static boolean validateString(char character) {
        if (character >= 'A' && character <= 'Z' || character >= 'a' && character <= 'z') {
            return true;
        }

        return false;
    }
}
