import java.util.Arrays;

/**
 * K번째수: https://programmers.co.kr/learn/courses/30/lessons/42748
 */
public class _42748 {
    public static void main(String[] args) {
//        [1, 5, 2, 6, 3, 7, 4]	[[2, 5, 3], [4, 4, 1], [1, 7, 3]]	[5, 6, 3

        int[] array = new int[] {1,5,2,6,3,7,4};
        int[][] commands = new int[][] {{2, 5, 3}, {4, 4, 1}, {1, 7, 3}};

        int[] result = solution(array, commands);

        System.out.println(Arrays.toString(result));
    }
    public static int[] solution(int[] array, int[][] commands) {
        int[] answer = new int[commands.length] ;
        int idx = 0;

        for (int[] command : commands) {
            int start = command[0];
            int end = command[1];
            int target = command[2];
            int[] tempArr = Arrays.copyOfRange(array, start - 1, end);

            Arrays.sort(tempArr);
            answer[idx++] = tempArr[target - 1];
        }

        return answer;
    }
}
