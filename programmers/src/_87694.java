import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// https://programmers.co.kr/learn/courses/30/lessons/87694
public class _87694 {
    public static void main(String[] args) {
//        int[][] rectangle = {
//                {1, 1, 7, 4},
//                {3, 2, 5, 5},
//                {4, 3, 6, 9},
//                {2, 6, 8, 8}
//        };
        int[][] rectangle = {{1, 1, 8, 4}, {2, 2, 4, 9}, {3, 6, 9, 8}, {6, 3, 7, 7}};
        int cx = 9, cy = 7, ix = 6, iy = 1;
//        int[][] rectangle = {{2, 2, 5, 5}, {1, 3, 6, 4}, {3, 1, 4, 6}};
//        int cx = 1, cy = 4, ix = 6, iy = 3;
        _87694 obj = new _87694();
        obj.solution(rectangle, cx, cy, ix, iy);
    }

    int[] dx = {0, 1, 0, -1};
    int[] dy = {-1, 0, 1, 0};
    boolean[][] visited;
    int distance = Integer.MAX_VALUE;

    public int solution(int[][] rectangle, int characterX, int characterY, int itemX, int itemY) {
        int maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;
        List<Rectangle> rectangles = new ArrayList<>();

        for (int i = 0; i < rectangle.length; i++) {
            for (int j = 0; j < rectangle[0].length; j++) {
                rectangle[i][j] = rectangle[i][j] * 2;
            }
        }
        characterX *= 2;
        characterY *= 2;
        itemX *= 2;
        itemY *= 2;

        for (int[] point : rectangle) {
            int lx = point[0];
            int ly = point[1];
            int rx = point[2];
            int ry = point[3];
            rectangles.add(new Rectangle(lx, ly, rx, ry));
            maxX = Math.max(maxX, rx);
            maxY = Math.max(maxY, ry);
        }
        visited = new boolean[maxY + 1][maxX + 1];
        int[][] grid = new int[maxY + 1][maxX + 1];

        for (Rectangle r : rectangles) {
            drawLine(r, grid, maxY);
        }
        printGrid(grid);
        for (Rectangle r : rectangles) {
            removeLine(r, grid, maxX, maxY);
        }
        printGrid(grid);
        int[] me = {characterX, maxY - characterY};
        int[] item = {itemX, maxY - itemY};
        find(grid, visited, me, item, 0);
        return distance / 2;
    }

    class Rectangle {
        List<int[]> points = new ArrayList<>();
        int width;
        int height;

        public Rectangle(int lx, int ly, int rx, int ry) {
            this.width = rx - lx;
            this.height = ry - ly;
            points.add(new int[] {lx, ly}); // x:좌 y:좌
            points.add(new int[] {lx, ry}); // x:좌 y:우
            points.add(new int[] {rx, ry}); // x:우 y:우
            points.add(new int[] {rx, ly}); // x:우 y:좌
        }

        public List<int[]> getPoints() { return this.points; }
        public int getWidth() { return this.width; }
        public int getHeight() { return this.height; }
    }

    public void drawLine(Rectangle rectangle, int[][] grid, int maxY) {
        int width = rectangle.getWidth();
        int height = rectangle.getHeight();
        int[] lxly = rectangle.getPoints().get(0);
        int row = maxY - lxly[1];
        int col = lxly[0];

        // 가로 채우기
        for (int i = col; i <= col + width; i++) {
            grid[row][i] = 1;
            grid[row - height][i] = 1;
        }

        // 세로 채우기
        for (int i = row; i >= row - height; i--) {
            grid[i][col] = 1;
            grid[i][col + width] = 1;
        }
    }

    public void removeLine(Rectangle rectangle, int[][] grid, int maxX, int maxY) {
        int width = rectangle.getWidth();
        int height = rectangle.getHeight();
        int[] lxly = rectangle.getPoints().get(0);
        int row = maxY - lxly[1];
        int col = lxly[0];

        for (int i = row - 1; i >= row - height + 1; i--) {
            for (int j = col + 1; j <= col + width - 1; j++) {
                grid[i][j] = 0;
            }
        }
    }

    public void find(int[][] grid, boolean[][] visited, int[] me, int[] item, int count) {
        if (me[0] == item[0] && me[1] == item[1]) {
            distance = Math.min(distance, count);
            return;
        }
        for (int i = 0; i < 4; i++) {
            int x = me[0] + dx[i];
            int y = me[1] + dy[i];
            if (x < 0 || x > grid[0].length - 1 || y < 0 || y > grid.length - 1) {
                continue;
            }
            if (grid[y][x] == 0 || visited[y][x]) {
                continue;
            }
            visited[y][x] = true;
            find(grid, visited, new int[] {x, y}, item, count + 1);
            visited[y][x] = false;
        }
    }

    public void printGrid(int[][] grid) {
        for (int[] g : grid) {
            System.out.println(Arrays.toString(g));
        }
    }
}
