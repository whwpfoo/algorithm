/**
 * 문자열 내림차순으로 배치하기: https://programmers.co.kr/learn/courses/30/lessons/12917
 */
public class _12917 {
    public String solution(String s) {
        StringBuilder sb = new StringBuilder();

        s.chars().sorted().forEach(word -> sb.append((char)word));

        return sb.reverse().toString();
    }
}
