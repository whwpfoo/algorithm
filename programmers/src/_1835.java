/**
 * 단체사진 찍기: https://programmers.co.kr/learn/courses/30/lessons/1835
 * data의 원소는 다섯 글자로 구성된 문자열이다. 각 원소의 조건은 다음과 같다.
 *
 * 첫 번째 글자와 세 번째 글자는 다음 8개 중 하나이다.
 * {A, C, F, J, M, N, R, T}
 * 각각 어피치, 콘, 프로도, 제이지, 무지, 네오, 라이언, 튜브를 의미한다. 첫 번째 글자는 조건을 제시한 프렌즈, 세 번째 글자는 상대방이다. 첫 번째 글자와 세 번째 글자는 항상 다르다.
 *
 * 두 번째 글자는 항상 ~이다.
 * 네 번째 글자는 다음 3개 중 하나이다. {=, <, >} 각각 같음, 미만, 초과를 의미한다.
 *
 * 다섯 번째 글자는 0 이상 6 이하의 정수의 문자형이며,
 * 조건에 제시되는 간격을 의미한다.
 * 이때 간격은 두 프렌즈 사이에 있는 다른 프렌즈의 수이다.
 */
public class _1835 {
    public static void main(String[] args) {
        _1835 obj = new _1835();
        String[] data = {"N~F=0", "R~T>2"};
        obj.solution(2, data);
        System.out.println(obj.answer);
    }

    int answer;
    char[] friends = {'A', 'C', 'F', 'J', 'M', 'N', 'R', 'T'};
    boolean[] visited;
    char[] output;

    public int solution(int n, String[] data) {
        int friendsSize = friends.length;
        visited = new boolean[friendsSize];
        output = new char[friendsSize];
        permutation(friends, visited, data, friendsSize, friendsSize, 0);

        return answer;
    }

    public void permutation(char[] friends, boolean[] visited, String[] conditions, int n, int r, int depth) {
        if (depth == r) {
            if (conditions(output, conditions)) {
                answer++;
            }
            return;
        }

        for (int i = 0; i < n; i++) {
            if (visited[i] != true) {
                visited[i] = true;
                output[depth] = friends[i];
                permutation(friends, visited, conditions, n, r, depth + 1);
                visited[i] = false;
            }
        }
    }

    public boolean conditions(char[] output, String[] conditions) {
        for (String c : conditions) {
            int a = -1;
            int b = -1;
            for (int i = 0; i < 8; i++) {
                if (c.charAt(0) == output[i]) a = i;
                if (c.charAt(2) == output[i]) b = i;
            }

            if (!conditions(c.charAt(3), c.charAt(4) - '0', a, b)) {
                return false;
            }
        }

        return true;
    }

    public boolean conditions(char sign, int distance, int a, int b) {
        int result = Math.abs(a - b);
        switch (sign) {
            case '=':
                return result == distance + 1;
            case '>':
                return result > distance + 1;
            case '<':
                return result < distance + 1;
            default:
                return false;
        }
    }
}
