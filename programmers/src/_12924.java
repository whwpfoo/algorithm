/**
 * 숫자의 표현: https://programmers.co.kr/learn/courses/30/lessons/12924
 */
public class _12924 {
    public int solution(int n) {
        int answer = 0;

        for (int i = 1; i <= n; i++) {
            int temp = 0;
            int j = i;

            while (temp <= n) {
                temp += j++;

                if (temp == n) {
                    answer += 1;
                    break;
                }
            }
        }

        return answer;
    }
}
