import java.awt.print.Pageable;
import java.util.Arrays;
import java.util.stream.Collectors;
/**
 * 체육복: https://programmers.co.kr/learn/courses/30/lessons/42862
 *
 * n = 전체학생수
 * lost = 체육복을 잃어버린 학생의 인덱스 번호
 * reserve = 여벌 체육복을 가져온 학생 인덱스 번호
 *
 * 여벌 학생 인덱스의 앞 또는 뒤 인덱스 에게만 빌려줄 수 있음
 *
 * 결과는 체육수업을 들을 수 있는 학생 수
 */
public class _42862 {

    public static void main(String[] args) {

    }

    public int solution(int n, int[] lost, int[] reserve) {
        int[] students = new int[n + 1]; // 길이 11

        for (int i = 1; i < students.length; i++) {
            students[i] = 1;
        }

        for (int i = 0; i < reserve.length; i++) {
            students[reserve[i]]++;
        }

        for (int i = 0; i < lost.length; i++) {
            students[lost[i]]--;
        }

        int answer = 0;

        for (int i = 1; i < students.length; i++) {
            int quantity = students[i];
            if (quantity > 1) {
                try {
                    if (i != 1 && students[i - 1] == 0) {
                        students[i - 1]++;
                        students[i]--;
                    } else if (i != students.length - 1 && students[i + 1] == 0) {
                        students[i + 1]++;
                        students[i]--;
                    }
                } catch (Exception e) {
                    continue;
                }
            }
        }

        for (int i = 1; i < students.length; i++) {
            if (students[i] >= 1) {
                answer += 1;
            }
        }

        return answer;
    }
}
