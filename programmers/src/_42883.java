/**
 * 큰 수 만들기: https://programmers.co.kr/learn/courses/30/lessons/42883
 */
public class _42883 {
    public static void main(String[] args) {
        _42883 obj = new _42883();
        String result = obj.solution("1924", 2);
        System.out.println(result);
    }

    public String solution(String number, int k) {
        StringBuilder answer = new StringBuilder();
        int idx = 0;
        int comp;

        for(int i = 0; i < number.length() - k; i++){
            comp = 0;

            for(int j = idx; j <= i + k; j++){
                if(comp < number.charAt(j) - '0'){
                    comp = number.charAt(j) - '0';
                    idx = j + 1;
                }
            }

            answer.append(comp);
        }

        return answer.toString();
    }
}
