import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// https://programmers.co.kr/learn/courses/30/lessons/86048
public class _86048 {
    public static void main(String[] args) {
        _86048 obj = new _86048();
//        int[] enter = {1, 4, 2, 3};
//        int[] leave = {2, 1, 3, 4};
//        int[] enter = {1, 3, 2};
//        int[] leave = {1, 2, 3};
        int[] enter = {1, 4, 2, 3};
        int[] leave = {2, 1, 4, 3};
//        기댓값 〉	[2, 2, 0, 2]

        System.out.println(Arrays.toString(obj.solution(enter, leave)));
    }


    public int[] solution(int[] enter, int[] leave) {
        int peopleCount = enter.length;

        Map<Integer, Integer> enterIndexMap = new HashMap<>();
        for (int i = 0; i < peopleCount; i++) {
            enterIndexMap.put(enter[i], i);
        }

        int[][] meetingTable = new int[peopleCount + 1][peopleCount + 1];
        boolean[] leavedPeople = new boolean[peopleCount + 1];

        for (int i = 0; i < peopleCount; i++) {
            int enterPeople = enter[i];
            int beforeEnterPeopleIndex = getBeforeEnterPeopleIndex(enterPeople, leave, leavedPeople, enterIndexMap);

            for (int j = 0; j <= beforeEnterPeopleIndex; j++) {
                int beforeEnterPeople = enter[j];
                if (leavedPeople[beforeEnterPeople] == false) {
                    contact(meetingTable, enterPeople, beforeEnterPeople);
                }
            }
            leavedPeople[enterPeople] = true;
        }

        return answer(meetingTable, peopleCount);
    }

    public void contact(int[][] meetingTable, int people, int otherPeople) {
        meetingTable[people][otherPeople] = meetingTable[otherPeople][people] = 1;
    }

    public int getBeforeEnterPeopleIndex(int enterPeople, int[] leave, boolean[] leavedPeople, Map<Integer, Integer> enterIndexMap) {
        int beforeLeavedIndex = 0;
        for (int i = 0; i < leave.length; i++) {
            if (enterPeople == leave[i]) {
                beforeLeavedIndex = i - 1;
                break;
            }
        }
        if (beforeLeavedIndex == -1) {
            return enterIndexMap.get(leave[0]);
        }
        int beforeEnterIndex = Integer.MIN_VALUE;
        for (int i = beforeLeavedIndex; i >= 0; i--) {
            if (leavedPeople[leave[i]]) {
                continue;
            }
            beforeEnterIndex = Math.max(beforeEnterIndex, enterIndexMap.get(leave[i]));
        }
        return beforeEnterIndex;
    }

    public int[] answer(int[][] meetingTable, int peopleCount) {
        int[] answer = new int[peopleCount];
        for (int i = 1; i < peopleCount + 1; i++) {
            for (int j = 1; j < peopleCount + 1; j++) {
                if (i == j) {
                    continue;
                }
                if (meetingTable[i][j] == 1) {
                    answer[i - 1]++;
                }
            }
        }
        return answer;
    }
}
