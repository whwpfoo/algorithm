import java.util.stream.Stream;

/**
 * 같은 숫자는 싫어: https://programmers.co.kr/learn/courses/30/lessons/12906
 */
public class _12906 {
    public int[] solution(int []arr) {
        StringBuilder sb = new StringBuilder();
        int beforeValue = arr[0];
        sb.append(beforeValue);

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] == beforeValue) {
                continue;
            }

            sb.append(arr[i]);
            beforeValue = arr[i];
        }

        return Stream.of(sb.toString().split("")).mapToInt(Integer::parseInt).toArray();
    }
}
