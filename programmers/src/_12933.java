import java.util.Arrays;

/**
 * 정수 내림차순으로 배치하기: https://programmers.co.kr/learn/courses/30/lessons/12933
 */
public class _12933 {

    public static void main(String[] args) {

    }

    public long solution(long n) {
        String[] integerArr = String.valueOf(n).split("");
        Arrays.sort(integerArr);

        StringBuilder sb = new StringBuilder();
        for (int i = integerArr.length - 1; i >= 0 ; i--) {
            sb.append(integerArr[i]);
        }

        return Long.valueOf(sb.toString());
    }
}
