import java.util.HashMap;
import java.util.Map;

// https://school.programmers.co.kr/learn/courses/30/lessons/131127
public class _131127 {

    public static void main(String[] args) {

        _131127 obj = new _131127();
        String[] want = new String[] {"banana", "apple", "rice", "pork", "pot"};
        int[] number = new int[] {3, 2, 2, 2, 1};
        String[] discount = new String[] {"chicken", "apple", "apple", "banana", "rice", "apple", "pork", "banana", "pork", "rice", "pot", "banana", "apple", "banana"};
        System.out.println(obj.solution(want, number, discount));
    }

    public int solution(String[] want, int[] number, String[] discount) {

        Map<String, Integer> map = getCountMap(want);
        int result = 0;

        for (int i = 0; i < 10; i++) {
            map.put(discount[i], map.getOrDefault(discount[i], 0) + 1);
        }

        if (matched(want, number, map)) {
            result++;
        }

        for (int i = 1; i <= discount.length - 10; i++) {
            String before = discount[i - 1];
            String next = discount[i + 9];
            map.put(before, map.getOrDefault(before, 0) - 1);
            map.put(next, map.getOrDefault(next, 0) + 1);
            if (matched(want, number, map)) {
                result++;
            }
        }

        return result;
    }

    private Map<String, Integer> getCountMap(String[] want) {

        Map<String, Integer> map = new HashMap<>();

        for (int i = 0; i < want.length; i++) {
            map.put(want[i], 0);
        }

        return map;
    }

    private boolean matched(String[] want, int[] number, Map<String, Integer> map) {

        for (int i = 0; i < want.length; i++) {
            if (map.get(want[i]) != number[i]) {
                return false;
            }
        }

        return true;
    }
}
