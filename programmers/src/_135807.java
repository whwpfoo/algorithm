// https://school.programmers.co.kr/learn/courses/30/lessons/135807
public class _135807 {

    public int solution(int[] arrayA, int[] arrayB) {

        int valueA = getGCD(arrayA);
        int valueB = getGCD(arrayB);

        int resultA = 0;
        int resultB = 0;


        if (valueA != 1 && !MOD(valueA, arrayB)) {
            resultA = valueA;
        }

        if (valueB != 1 && !MOD(valueB, arrayA)) {
            resultB = valueB;
        }

        if (resultA == 0 && resultB == 0) {
            return 0;
        }

        return resultA > resultB ? resultA : resultB;
    }

    private boolean MOD(int value, int[] array) {

        for (int a : array) {
            if (a % value == 0) {
                return true;
            }
        }

        return false;
    }

    private int getGCD(int[] array) {

        int value = array[0];

        for (int i = 1; i < array.length; i++) {
            if (value > array[i]) {
                value = GCD(value, array[i]);
            } else {
                value = GCD(array[i], value);
            }
        }
        return value;
    }

    public int GCD(int big, int small) {

        if (big % small == 0) {
            return small;
        }

        return GCD(small, big % small);
    }
}
