import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * https://leetcode.com/problems/destination-city/
 */
public class DestinationCity {
    public String destCity(List<List<String>> paths) {
        Map<String, String> pathMap = new HashMap<>();
        for (List<String> letters : paths) {
            pathMap.put(letters.get(0), letters.get(1));
        }

        for (String letter : pathMap.values()) {
            String city = pathMap.get(letter);
            if (city == null) return letter;
        }

        return null;
    }
}
