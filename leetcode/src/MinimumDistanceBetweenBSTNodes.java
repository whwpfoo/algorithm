/**
 * https://leetcode.com/problems/minimum-distance-between-bst-nodes/
 */
public class MinimumDistanceBetweenBSTNodes {

    public static void main(String[] args) {
        MinimumDistanceBetweenBSTNodes mdbb = new MinimumDistanceBetweenBSTNodes();
        TreeNode one = new TreeNode(1, null , null);
        TreeNode three = new TreeNode(3, null , null);
        TreeNode two = new TreeNode(2, one , three);
        TreeNode five = new TreeNode(5, null , null);
        TreeNode six = new TreeNode(6, five , null);
        TreeNode root = new TreeNode(4, two , six);
        mdbb.minDiffInBST(root);
    }

    Integer previous = null;
    int ans = 0;

    public int minDiffInBST(TreeNode root) {
        ans = Integer.MAX_VALUE;
        previous = null;
        dfs(root);
        return ans;
    }

    private void dfs(TreeNode root) {
        if (root == null) {
            return;
        }
        dfs(root.left);
        if (previous != null) {
            ans = Math.min(ans, root.val - previous);
        }
        previous = root.val;
        dfs(root.right);
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() {}
        TreeNode(int val) { this.val = val; }
        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
