import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode.com/problems/add-two-numbers/
 */
public class AddTwoNumbers {
    public class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        StringBuilder l1Value = new StringBuilder();
        while (l1 != null) {
            l1Value.insert(0, l1.val);
            l1 = l1.next;
        }

        StringBuilder l2Value = new StringBuilder();
        while (l2 != null) {
            l2Value.insert(0, l2.val);
            l2 = l2.next;
        }

        BigInteger tempValue = new BigInteger(l1Value.toString()).add(new BigInteger(l2Value.toString()));
        String[] tempArray = String.valueOf(tempValue.toString()).split(""); // [8, 0, 7];

        List<ListNode> listNodes = new ArrayList<>();
        for (int i = tempArray.length - 1; i >= 0; i--) {
            listNodes.add(new ListNode(Integer.parseInt(tempArray[i])));
        }

        for (int i = 0; i < listNodes.size() - 1; i++) {
            listNodes.get(i).next = listNodes.get(i + 1);
        }

        return listNodes.get(0);
    }
}
