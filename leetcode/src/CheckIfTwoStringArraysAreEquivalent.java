import java.util.Arrays;

/**
 * https://leetcode.com/problems/check-if-two-string-arrays-are-equivalent/
 */
public class CheckIfTwoStringArraysAreEquivalent {
    public boolean arrayStringsAreEqual(String[] word1, String[] word2) {
        String let1 = Arrays.stream(word1).reduce((s1, s2) -> s1 + s2).get();
        String let2 = Arrays.stream(word2).reduce((s1, s2) -> s1 + s2).get();

        return let1.equals(let2);
    }
}

