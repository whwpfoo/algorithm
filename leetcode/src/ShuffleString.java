import java.util.Arrays;
import java.util.stream.Stream;

/**
 * https://leetcode.com/problems/shuffle-string/
 */
public class ShuffleString {
    public static void main(String[] args) {
        String s = "codeleet";
        int[] indices = {4,5,6,7,0,2,1,3};
        String result = restoreString(s, indices);
        System.out.println(result);
    }
    public static String restoreString(String s, int[] indices) {
        char[] arr = new char[indices.length];

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int pos = indices[i];
            arr[pos] = c;
        }

        return String.valueOf(arr);
    }
}
