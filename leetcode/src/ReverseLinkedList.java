// https://leetcode.com/problems/reverse-linked-list/
public class ReverseLinkedList {
    public static void main(String[] args) {
        ReverseLinkedList obj = new ReverseLinkedList();
        ListNode root = obj.new ListNode(1);
        root.add(obj.new ListNode(2));
        root.add(obj.new ListNode(3));
        root.add(obj.new ListNode(4));
        root.add(obj.new ListNode(5));
        System.out.println(obj.reverseList(root));

    }

    ListNode result;

    public ListNode reverseList(ListNode head) {
        reverse(head, null);
        return result;
    }

    public void reverse(ListNode node, ListNode prev) {
        if (node == null) {
            result = prev;
            return;
        }
        reverse(node.next, node);
        node.next = prev;
    }
    
    public class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }

        public void add(ListNode node) {
            if (next != null) {
                ListNode temp = next;
                while (temp.next != null) {
                    temp = temp.next;
                }
                temp.next = node;
            } else {
                next = node;
            }
        }

        @Override
        public String toString() {
            return "ListNode{" +
                    "val=" + val +
                    ", next=" + next +
                    '}';
        }
    }
}
