import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode.com/problems/score-after-flipping-matrix/description/?envType=daily-question&envId=2024-05-13
 *
 * You are given an m x n binary matrix grid.
 * A move consists of choosing any row or column and toggling each value in that row or column (i.e., changing all 0's to 1's, and all 1's to 0's).
 * Every row of the matrix is interpreted as a binary number, and the score of the matrix is the sum of these numbers.
 * Return the highest possible score after making any number of moves (including zero moves).
 */
public class ScoreAfterFlippingMatrix {
    Map<Integer, Integer> count = new HashMap<>();

    public int matrixScore(int[][] grid) {
        int rowSize = grid.length;
        int colSize = grid[0].length;

        for (int i = 0; i < rowSize; i++) {
            reverse(grid, i, colSize - 1, grid[i][0] == 0 ? true : false);
        }

        int answer = 0;
        for (int v : count.keySet()) {
            int value = count.get(v);
            answer += Math.pow(2, v) * (value <= (rowSize / 2) ? (rowSize - value) : value);
        }

        return answer;
    }

    private void reverse(int[][] grid, int start, int end, boolean reverse) {
        for (int i = 0; i <= end; i++) {
            if (reverse) {
                grid[start][i] = Math.abs(grid[start][i] - 1);
            }
            if (grid[start][i] == 1) {
                count.put(end - i, count.getOrDefault(end - i, 0) + 1);
            } else {
                count.put(end - i, count.getOrDefault(end - i, 0));
            }
        }
    }

    public static void main(String[] args) {
        ScoreAfterFlippingMatrix obj = new ScoreAfterFlippingMatrix();
        System.out.println(obj.matrixScore(new int[][] {{0, 0, 1, 1}, {1, 0, 1, 0}, {1, 1, 0, 0}}));
    }
}
