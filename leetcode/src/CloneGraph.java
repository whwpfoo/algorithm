import java.util.HashMap;
import java.util.List;
import java.util.Map;

// https://leetcode.com/problems/clone-graph/
public class CloneGraph {
    Map<Integer, Node> container = new HashMap<Integer, Node>();
    boolean[] visited = new boolean[101];

    public Node cloneGraph(Node node) {
        copy(node);
        return container.get(1);
    }

    public void copy(Node n) {
        if (n == null || visited[n.val]) {
            return;
        }

        visited[n.val] = true;
        Node node = container.getOrDefault(n.val, new Node(n.val));
        container.put(n.val, node);

        for (Node nn : n.neighbors) {
            copy(nn);
            if (!container.containsKey(nn.val)) {
                container.put(nn.val, new Node(nn.val));
            }
            node.neighbors.add(container.get(nn.val));
        }
    }

    class Node {
        int val;
        List<Node> neighbors;

        public Node() {};
        public Node(int val) {
            this.val = val;
        }
    }
}
