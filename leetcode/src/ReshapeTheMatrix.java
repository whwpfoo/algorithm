/**
 * https://leetcode.com/problems/reshape-the-matrix/
 */
public class ReshapeTheMatrix {
    public int[][] matrixReshape(int[][] nums, int r, int c) {
        if (nums.length * nums[0].length == r * c) {
            int[][] result = new int[r][c];
            int start = 0;
            int[] arr = new int[nums.length * nums[0].length];

            for (int i = 0; i < nums.length; i++) {
                for (int j = 0; j < nums[0].length; j++) {
                    arr[start++] = nums[i][j];
                }
            }

            start = 0;

            for (int i = 0; i < r; i++) {
                for (int j = 0; j < c; j++) {
                    result[i][j] = arr[start++];
                }
            }

            return result;
        }

        return nums;
    }
}
