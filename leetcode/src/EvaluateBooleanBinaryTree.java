/**
 * https://leetcode.com/problems/evaluate-boolean-binary-tree/description/?submissionId=1259169118
 */
public class EvaluateBooleanBinaryTree {
    public boolean evaluateTree(TreeNode root) {
        if (root.left == null || root.right == null) {
            return root.val == 0 ? false : true;
        }
        int condition = root.val;
        boolean l = evaluateTree(root.left);
        boolean r = evaluateTree(root.right);

        return condition == 2 ? l || r : l && r;
    }

    private class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() {}
        TreeNode(int val) { this.val = val; }
        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}