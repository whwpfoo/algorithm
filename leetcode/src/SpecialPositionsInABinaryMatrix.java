/**
 * https://leetcode.com/problems/special-positions-in-a-binary-matrix/
 *
 * (i,j) 위치 값이 1 이고 (i,j) 기준으로 i행과 j열 모두 0 인 경우를 만족하는 (i,j) 위치 갯수를 리턴
 */
public class SpecialPositionsInABinaryMatrix {
    public int numSpecial(int[][] mat) {
        int result = 0;

        int row = mat.length;
        int col = mat[0].length;

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (mat[i][j] == 1) {
                    if (ValidSpecialPosition(mat, i, j, row, col)) {
                        result++;
                    }
                }
            }
        }

        return result;
    }

    private boolean ValidSpecialPosition(int[][] mat, int rowIndex, int colIndex, int row, int col) {
        for (int  i = 0; i < col; i++) {
            if (i == colIndex)
                continue;

            if (mat[rowIndex][i] != 0) {
                return false;
            }
        }
        for (int i = 0; i < row; i++) {
            if (i == rowIndex)
                continue;

            if (mat[i][colIndex] != 0) {
                return false;
            }
        }

        return true;
    }
}

// 1. 위치가 1 인경우 if (mat[i][j] == 1)
// 2. 모든 열 검증 if (mat[i][0~n-1] == 0)
// 3. 모든 행 검증 if (mat[0~n-1][j] == 0)
// 위 3 경우를 만족하면 i,j 리턴턴