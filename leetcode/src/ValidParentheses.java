import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * https://leetcode.com/problems/valid-parentheses/
 */
public class ValidParentheses {
    public boolean isValid(String s) {
        Map<Character, Character> map = new HashMap<>();
        map.put('(', ')');
        map.put('{', '}');
        map.put('[', ']');

        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            char bracket = s.charAt(i);
            if (bracket == 40 ||
                    bracket == 91 ||
                    bracket == 123) {
                stack.push(bracket);
            } else {
                if (stack.isEmpty()) return false;
                char openBracket = stack.pop();
                if (bracket != map.get(openBracket)) {
                    return false;
                }
            }
        }

        if (stack.size() > 0) return false;
        else return true;
    }
}
