public class LongestCommonSubsequence {
    public static void main(String[] args) {
        String text1 = "abcde";
        String text2 = "ace";

        LongestCommonSubsequence obj = new LongestCommonSubsequence();
        obj.longestCommonSubsequence(text1, text2);
    }

    // 두 문자열의 동일한 부분 문자열 중 길이가 가장 큰 값을 구하는 문제
    // 단, 문자열의 문자가 연속적이지 않아도 됨
    public int longestCommonSubsequence(String text1, String text2) {
        int text2Length = text2.length();
        int text1Length = text1.length();
        int[][] dp = new int[text2Length + 1][text1Length + 1];

        for (int i = 1; i <= text2Length; i++) {
            for (int j = 1; j <= text1Length; j++) {
                if (text2.charAt(i - 1) == text1.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i][j - 1], dp[i - 1][j]);
                }
            }
        }

        return dp[text2Length][text1Length];
    }
}
