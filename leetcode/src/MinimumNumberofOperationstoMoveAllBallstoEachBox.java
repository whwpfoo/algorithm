import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

// https://leetcode.com/problems/minimum-number-of-operations-to-move-all-balls-to-each-box/
public class MinimumNumberofOperationstoMoveAllBallstoEachBox {
    public static void main(String[] args) {
        MinimumNumberofOperationstoMoveAllBallstoEachBox o = new MinimumNumberofOperationstoMoveAllBallstoEachBox();
        o.minOperations("110");
    }

    public int[] minOperations(String b) {
        List<Integer> fullBoxList = IntStream.range(0, b.length())
                .filter(idx -> b.charAt(idx) == '1')
                .boxed()
                .collect(Collectors.toList());

        return IntStream.range(0, b.length())
                .map(i -> fullBoxList.stream().mapToInt(Integer::intValue)
                        .filter(idx -> idx != i)
                        .map(idx -> Math.abs(idx - i))
                        .sum())
                .toArray();
    }
}
