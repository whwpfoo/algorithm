import java.util.*;

// https://leetcode.com/problems/course-schedule-ii/
public class CourseScheduleII {
    public int[] findOrder(int numCourses, int[][] prerequisites) {
        ArrayList<Integer>[] preCourseList = new ArrayList[numCourses];

        for (int i = 0; i < numCourses; i++) {
            preCourseList[i] = new ArrayList<>();
        }

        int[] degree = new int[numCourses];

        for (int[] pre : prerequisites) {
            degree[pre[0]]++;
            preCourseList[pre[1]].add(pre[0]);
        }

        Queue<Integer> q = new LinkedList<>();

        for (int i = 0; i < numCourses; i++) {
            if (degree[i] == 0) {
                q.offer(i);
            }
        }

        if (q.size() == 0) {
            return new int[0];
        }

        int[] result = new int[numCourses];
        int idx = 0;

        while (!q.isEmpty()) {
            int pre = q.poll();
            result[idx++] = pre;

            for (int course : preCourseList[pre]) {
                degree[course]--;
                if (degree[course] == 0) q.offer(course);
            }
        }

        return idx == numCourses ? result : new int[0];
    }
}
