// https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
public class BestTimeToBuyAndSellStock {
    public int maxProfit(int[] prices) {
        int buy = 0, sell = 1;
        int max = 0;

        while (sell < prices.length) {
            if (prices[buy] <= prices[sell]) {
                max = Math.max(max, prices[sell] - prices[buy]);
            } else {
                buy = sell;
            }
            sell += 1;
        }

        return max;
    }
}
