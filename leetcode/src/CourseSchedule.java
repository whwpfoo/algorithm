import java.util.*;

// https://leetcode.com/problems/course-schedule/
public class CourseSchedule {
    /**
     * @param numCourses    수강해야 하는 코스 카운트
     * @param p 선수강 정보
     *                      [i][1] 수강하기 위해선 [i][0] 을 먼저 수강
     * @return numCourses 만큼 수강할 수 있다면 true 아니면 false
     */
    public boolean canFinish(int numCourses, int[][] p) {
        // 위상정렬 문제: 그래프가 순환하는지의 여부를 알 수 있음
        int[] degree = new int[numCourses]; // 각 노드의 진입 차수 정보
        ArrayList<Integer>[] adjList = new ArrayList[numCourses];

        for (int i = 0; i < numCourses; i++) {
            adjList[i] = new ArrayList<>();
        }

        // p[1] => p[0] 만족해야 함
        for (int[] pre : p) {
            degree[pre[0]]++;               // pre[0] 에 차수가 존재한다 == 카운트 증가
            adjList[pre[1]].add(pre[0]);    // 선수강과목에 연결된 과목을 추가
        }
        // 초기화 end

        // 들어오는 차수가 0 인 애들은 큐에 삽입
        Queue<Integer> q = new LinkedList<>();
        for (int i = 0; i < degree.length; i++) {
            if (degree[i] == 0) q.offer(i);
        }
        Set<Integer> set = new HashSet<>();
        while (q.isEmpty() == false) {
            int n = q.poll();
            set.add(n);
            for (int t : adjList[n]) {
                if (--degree[t] == 0) q.offer(t);
            }
            adjList[n].clear();
        }
        return set.size() == numCourses;
    }
}
