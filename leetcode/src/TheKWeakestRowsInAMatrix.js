/***
 * https://leetcode.com/problems/the-k-weakest-rows-in-a-matrix/
 */

/**
 * @param {number[][]} mat
 * @param {number} k
 * @return {number[]}
 */
var kWeakestRows = function(mat, k) {
    const result = [];

    mat.forEach((val, idx) => {
        const soldiers = val.filter((value) => value === 1);
        result.push([idx, soldiers.length]);
    });

    result.sort((a, b) => a[1] === b[1] ? a[0] - b[0] : a[1] - b[1]);
    return result.slice(0, k).map(el => el[0]);
};