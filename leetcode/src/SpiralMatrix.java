import java.util.ArrayList;
import java.util.List;

// https://leetcode.com/problems/spiral-matrix/
public class SpiralMatrix {
    boolean[][] visited;

    public List<Integer> spiralOrder(int[][] matrix) {
        visited = new boolean[matrix.length][matrix[0].length];
        List<Integer> result = new ArrayList<>();
        spiral(matrix, result, 0, 0, false);
        return result;
    }

    public void spiral(int[][] matrix, List<Integer> result, int i, int j, boolean up) {
        if (i < 0 || i >= matrix.length || j < 0 || j >= matrix[0].length) return;
        if (visited[i][j]) return;
        result.add(matrix[i][j]);
        visited[i][j] = true;

        if (up) {
            spiral(matrix, result, i - 1, j, true);
        }
        spiral(matrix, result, i, j + 1, false);
        spiral(matrix, result, i + 1, j, false);
        spiral(matrix, result, i, j - 1, false);
        spiral(matrix, result, i - 1, j, true);
    }
}
