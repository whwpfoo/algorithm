/**
 * https://leetcode.com/problems/add-binary/
 */
public class AddBinary {
    public String addBinary(String a, String b) {
        StringBuilder result = new StringBuilder();
        int carry = 0;
        int aLast = a.length() - 1;
        int bLast = b.length() - 1;

        for ( ; aLast >= 0 || bLast >= 0;) {
            int ac = 0, bc = 0;
            if (aLast >= 0) ac = a.charAt(aLast) - '0';
            if (bLast >= 0) bc = b.charAt(bLast) - '0';

            int sum = ac + bc;

            if (carry != 0) {
                sum += carry;
                carry = 0;
            }
            if (sum == 3) {
                carry += 1;
                result.insert(0, "1");
            } else if (sum == 2) {
                carry += 1;
                result.insert(0, "0");
            } else if (sum == 1) {
                result.insert(0, "1");
            } else {
                result.insert(0, "0");
            }
            aLast--; bLast--;
        }

        if (carry == 1) result.insert(0, "1");

        return result.toString();
    }
}
