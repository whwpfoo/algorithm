import java.util.*;
import java.util.stream.IntStream;

// https://leetcode.com/problems/generate-parentheses/
public class GenerateParentheses {
    public List<String> generateParenthesis(int n) {
        if (n == 1) return Arrays.asList("()");

        Set<String>[] dp = new HashSet[n];
        for (int i = 0; i < n; i++) {
            dp[i] = new HashSet<>();
        }
        dp[0].add("()");

        for (int i = 1; i < n; i++) {
            Set<String> before = dp[i - 1];
            for (String t : before) {
                StringBuilder sb = new StringBuilder(t); // 0 1
                for (int j = 0; j <= sb.length(); j++) {
                    sb.insert(j, "()");
                    dp[i].add(sb.toString());
                    sb.delete(j, j + 2);
                }
            }
        }

        return new ArrayList<>(dp[n - 1]);
    }
}
