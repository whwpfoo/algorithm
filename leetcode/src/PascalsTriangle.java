import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode.com/problems/pascals-triangle/
 */
public class PascalsTriangle {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new ArrayList<>();

        for (int i = 0; i < numRows; i++) {
            List<Integer> target = new ArrayList<>();
            result.add(target);
            for (int j = 0; j <= i; j++) {
                if (j == 0 || j == i) {
                    target.add(1);
                    continue;
                }

                List<Integer> beforeList = result.get(i - 1);
                for (int k = 0; k < beforeList.size() - 1; k++) {
                    target.add(beforeList.get(k) + beforeList.get(k + 1));
                }
                j = i - 1;
            }
        }

        return result;
    }
}
