import java.util.Arrays;

/**
 * https://leetcode.com/problems/check-if-a-word-occurs-as-a-prefix-of-any-word-in-a-sentence/
 */
public class CheckIfAWordOccursAsAPrefixOfAnyWordInASentence {
    public static void main(String[] args) {

    }

    public int isPrefixOfWord(String sentence, String searchWord) {
        String[] letters = sentence.split(" ");
        int findIndex = -1;

        for (int i = 0; i < letters.length; i++) {
            if (letters[i].startsWith(searchWord)) {
                findIndex = i;
                break;
            }
        }

        return findIndex != -1 ? findIndex + 1 : findIndex;
    }
}
