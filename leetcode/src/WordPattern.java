import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode.com/problems/word-pattern/
 */
public class WordPattern {
    public boolean wordPattern(String pattern, String s) {
        Map<String, Character> map = new HashMap<>();
        String[] letters = s.split(" ");

        int loopSize = letters.length;

        if (loopSize != pattern.length()) {
            return false;
        }

        for (int i = 0; i < loopSize; i++) {
            String letter = letters[i];
            char pat = pattern.charAt(i);

            Character findPat = map.get(letter);
            if (findPat != null) {
                if (pat != findPat) {
                    return false;
                }
            } else {
                for (char storedPattern : map.values()) {
                    if (storedPattern == pat) {
                        return false;
                    }
                }
                map.put(letter, pat);
            }
        }

        return true;
    }
}
