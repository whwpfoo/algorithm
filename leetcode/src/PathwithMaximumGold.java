/**
 * https://leetcode.com/problems/path-with-maximum-gold/description/?envType=daily-question&envId=2024-05-14
 */
public class PathwithMaximumGold {
    int answer = 0;

    public int getMaximumGold(int[][] grid) {
        boolean[][] visited = new boolean[grid.length][grid[0].length];

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 0)
                    continue;
                collect(grid, visited, i, j, 0);
            }
        }

        return answer;
    }

    private void collect(int[][] grid, boolean[][] visited, int i, int j, int gold) {
        if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length)
            return;
        if (grid[i][j] == 0 || visited[i][j])
            return;

        visited[i][j] = true;
        collect(grid, visited, i - 1, j, gold + grid[i][j]);
        collect(grid, visited, i + 1, j, gold + grid[i][j]);
        collect(grid, visited, i, j - 1, gold + grid[i][j]);
        collect(grid, visited, i, j + 1, gold + grid[i][j]);
        visited[i][j] = false;

        answer = Math.max(answer, gold + grid[i][j]);
    }

    public static void main(String[] args) {
        PathwithMaximumGold obj = new PathwithMaximumGold();
        obj.getMaximumGold(new int[][] {{0,6,0}, {5,8,7}, {0,9,0}});
    }
}


