import java.util.Arrays;

/**
 * https://leetcode.com/problems/water-bottles/
 */
public class WaterBottles {
    public int numWaterBottles(int numBottles, int numExchange) {

        int result = numBottles;
        int[] array = new int[0];
        Arrays.binarySearch(array, 1);
        while (numBottles >= numExchange) {
            int emptyBottles = numBottles / numExchange;
            result += emptyBottles;
            emptyBottles += (numBottles % numExchange);
            numBottles = emptyBottles;
        }

        return result;
    }
}
