import java.util.Stack;

/**
 * https://leetcode.com/problems/maximum-score-words-formed-by-letters/?envType=daily-question&envId=2024-05-24
 */
public class MaximumScoreWordsFormedByLetters {

    int max = 0;

    public int maxScoreWords(String[] words, char[] letters, int[] score) {
        int[] lets = new int[26];

        for (char l : letters)
            lets[l - 'a'] += 1;

        helper(words, lets, 0, words.length, new Stack<>(), score);

        return max;
    }

    public void helper(String[] words, int[] lets, int s, int e, Stack<String> state, int[] score) {
        if (s >= e) {
            int sum = 0;
            for (String word : state) {
                for (char l : word.toCharArray()) {
                    sum += score[l - 'a'];
                }
            }
            max = Math.max(max, sum);
            return;
        }

        String word = words[s];
        boolean pos = true;
        int[] copy = new int[26];
        System.arraycopy(lets, 0, copy, 0, lets.length);

        for (char l : word.toCharArray()) {
            copy[l - 'a']--;
            if (copy[l - 'a'] < 0) {
                pos = false;
                break;
            }
        }

        if (pos == false) {
            helper(words, lets, s + 1, e, state, score);
        } else {
            state.push(word);
            for (char l : word.toCharArray()) {
                lets[l - 'a']--;
            }
            helper(words, lets, s + 1, e, state, score);

            state.pop();
            for (char l : word.toCharArray()) {
                lets[l - 'a']++;
            }
            helper(words, lets, s + 1, e, state, score);
        }
    }
}
