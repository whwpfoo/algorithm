/**
 * https://leetcode.com/problems/reverse-only-letters/
 */
public class ReverseOnlyLetters {
    public static void main(String[] args) {
        String input = "?6C40E";
        String s = reverseOnlyLetters(input);
        System.out.println(s);
    }
    public static String reverseOnlyLetters(String S) {
        char[] letters = S.toCharArray();
        int head = 0;
        int tail = S.length() - 1;

        while (head < tail) {
            while (head < tail && !Character.isLetter(letters[head])) {
                head++;
            }
            while (head < tail && !Character.isLetter(letters[tail])) {
                tail--;
            }
            swap(letters, head, tail);
            head++;
            tail--;
        }

        return String.valueOf(letters);
    }

    public static void swap(char[] arr, int i, int j) {
        char temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
