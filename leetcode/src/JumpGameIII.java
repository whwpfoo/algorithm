import java.util.*;

// https://leetcode.com/problems/jump-game-iii/
public class JumpGameIII {
    boolean[] visited;

    public boolean canReach(int[] arr, int start) {
        visited = new boolean[arr.length];
        Queue<Integer> q = new LinkedList<>();
        q.offer(start);

        while (!q.isEmpty()) {
            int index = q.poll();

            if (arr[index] == 0) {
                return true;
            }

            visited[index] = true;
            int right = index + arr[index];
            int left = index - arr[index];

            if (right < arr.length && !visited[right]) q.offer(right);
            if (left >= 0 && !visited[left]) q.offer(left);
        }

        return false;
    }
}
