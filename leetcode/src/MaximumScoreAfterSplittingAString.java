/**
 * https://leetcode.com/problems/maximum-score-after-splitting-a-string/
 */
public class MaximumScoreAfterSplittingAString {
    public int maxScore(String s) {
        // 문자열을 왼쪽:오른쪽으로 구분한다
        // 왼쪽의 0의 갯수와 오른쪽의 1의 갯수 둘의 합이 가장 클 때의 합을 리턴하라
        int max = 0;

        for (int i = s.length() - 1; i > 0; i--) {
            String letter = s.substring(0, i);
            String letter2 = s.substring(i, s.length());
            int zeroCount = letter.replaceAll("1", "").length();
            int oneCount = letter2.replaceAll("0", "").length();
            max = Math.max(max, zeroCount + oneCount);
        }
        return max;
    }
}
