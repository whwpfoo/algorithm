import java.util.*;

// https://leetcode.com/problems/pacific-atlantic-water-flow/
public class PacificAtlanticWaterFlow {
    public static void main(String[] args) {
        int[][] hs = {{1,2,2,3,5},{3,2,3,4,4},{2,4,5,3,1},{6,7,1,4,5},{5,1,1,2,4}};
        PacificAtlanticWaterFlow obj = new PacificAtlanticWaterFlow();
    }

    int[][] dir = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};

    public List<List<Integer>> pacificAtlantic(int[][] heights) {
        int M = heights[0].length;
        int N = heights.length;

        List<List<Integer>> result = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                if (possible(heights, i, j, N, M)) {
                    result.add(Arrays.asList(i, j));
                }
            }
        }

        return result;
    }

    private boolean possible(int[][] heights, int i, int j, int rowLength, int colLength) {
        Queue<int[]> q = new LinkedList<>();
        boolean[][] visited = new boolean[rowLength][colLength];

        boolean isPacific = false;
        boolean isAtlantic = false;
        q.offer(new int[] {i, j});
        visited[i][j] = true;

        while (!q.isEmpty()) {
            int[] coord = q.poll();
            int x = coord[0];
            int y = coord[1];

            for (int[] d : dir) {
                int dx = x + d[0];
                int dy = y + d[1];

                if (dx < 0 || dy < 0) {
                    isPacific = true;
                } else if (dx >= rowLength || dy >= colLength) {
                    isAtlantic = true;
                } else if (visited[dx][dy] == false && heights[dx][dy] <= heights[x][y]) {
                    q.offer(new int[] {dx, dy});
                    visited[dx][dy] = true;
                }
            }
        }

        return isPacific && isAtlantic;
    }
}
