// https://leetcode.com/problems/implement-trie-prefix-tree/
public class ImplementTrie {
    // Trie 자료구조 구현
    TrieNode root;

    public ImplementTrie() {
        this.root = new TrieNode();
    }

    public void insert(String word) {
        TrieNode dummy = root;
        for (char c : word.toCharArray()) {
            if (dummy.children[c - 'a'] == null) {
                dummy.children[c - 'a'] = new TrieNode();
            }
            dummy = dummy.children[c - 'a'];
        }
        dummy.isWord = true;
    }

    public boolean search(String word) {
        TrieNode dummy = root;
        for (char c : word.toCharArray()) {
            if (dummy == null) {
                return false;
            }
            dummy = dummy.children[c - 'a'];
        }
        return (dummy != null && dummy.isWord);
    }

    public boolean startsWith(String prefix) {
        TrieNode dummy = root;
        for (char c : prefix.toCharArray()) {
            dummy = dummy.children[c - 'a'];
            if (dummy == null) {
                return false;
            }
        }
        return true;
    }

    static class TrieNode {
        boolean isWord;
        TrieNode[] children;

        public TrieNode() {
            this.isWord = false;
            this.children = new TrieNode[26];
        }
    }
}
