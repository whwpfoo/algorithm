import java.util.*;

// https://leetcode.com/problems/count-square-submatrices-with-all-ones/
public class CountSquareSubmatriceswithAllOnes {
    public int countSquares(int[][] matrix) {
        int M = matrix.length;
        int N = matrix[0].length;

        Map<Integer, List<int[]>> dp = new HashMap<>();

        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (matrix[i][j] == 1) {
                    List<int[]> list = dp.getOrDefault(1, new ArrayList<int[]>());
                    list.add(new int[] {i, j});
                    dp.put(1, list);
                }
            }
        }


        if (dp.size() == 0) return 0;


        int limit = Math.min(M, N);
        int start = 2;

        while (start <= limit) {
            List<int[]> coords = dp.get(start - 1);

            if (coords == null) break;

            List<int[]> newCoords = new ArrayList<>();

            for (int[] coord : coords) {
                int row = coord[0], col = coord[1];
                if (isSquare(start, row, col, matrix)) {
                    newCoords.add(new int[] {row, col});
                }
            }

            dp.put(start, newCoords);
            start++;
        }


        int count = 0;
        for (List<int[]> l : dp.values()) {
            count += l.size();
        }


        return count;
    }

    public boolean isSquare(int size, int row, int col, int[][] m) {
        if (row + size - 1 > m.length - 1 || col + size - 1 > m[0].length - 1) {
            return false;
        }
        for (int i = row; i < row + size; i++) {
            for (int j = col; j < col + size; j++) {
                if (m[i][j] == 0)
                    return false;
            }
        }
        return true;
    }
}
