// https://leetcode.com/problems/unique-paths-iii/
public class UniquePathsIII {

    int[][] d = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
    int result = 0;

    public int uniquePathsIII(int[][] grid) {
        int rowLength = grid.length;
        int colLength = grid[0].length;

        boolean[][] visited = new boolean[rowLength][colLength];
        int availablePathCount = 0;
        int startRow = 0, startCol = 0;

        for (int i = 0; i < rowLength; i++) {
            for (int j = 0; j < colLength; j++) {
                if (grid[i][j] == 0) {
                    availablePathCount++;
                } else {
                    if (grid[i][j] == 1) {
                        startRow = i; startCol = j;
                    }
                    visited[i][j] = true;
                }
            }
        }

        find(grid, visited, startRow, startCol, availablePathCount, 0);
        return result;
    }

    public void find(int[][] grid, boolean[][] visited, int row, int col, int availablePathCount, int moveCount) {
        for (int i = 0; i < 4; i++) {
            int dr = row + d[i][0];
            int dc = col + d[i][1];

            if (dr < 0 || dr >= grid.length || dc < 0 || dc >= grid[0].length) continue;
            if (grid[dr][dc] == 2) {
                if (availablePathCount == moveCount) result++;
                continue;
            }
            if (visited[dr][dc] || grid[dr][dc] == -1) continue;

            visited[dr][dc] = true;
            find(grid, visited, dr, dc, availablePathCount, moveCount + 1);
            visited[dr][dc] = false;
        }
    }
}
