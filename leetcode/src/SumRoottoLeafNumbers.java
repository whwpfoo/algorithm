// https://leetcode.com/problems/sum-root-to-leaf-numbers/
public class SumRoottoLeafNumbers {
    public int sumNumbers(TreeNode root) {
        return sumRootToLeaf(root, 0);
    }

    public int sumRootToLeaf(TreeNode node, int value) {
        if (node == null) return 0;
        if (node.left == null && node.right == null) return value * 10 + node.val;
        return sumRootToLeaf(node.left, value * 10 + node.val)
                + sumRootToLeaf(node.right, value * 10 + node.val);
    }

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() {}
        TreeNode(int val) { this.val = val; }
        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
