import java.util.*;

/**
 * https://leetcode.com/problems/sort-array-by-increasing-frequency/
 */
public class SortArrayByIncreasingFrequency {
    public int[] frequencySort(int[] nums) {
        Map<Integer, List<Integer>> result = new HashMap<>();

        for (int num : nums) {
            List<Integer> temp = result.get(num);
            if (temp == null) {
                temp = new ArrayList<>();
                temp.add(num);
                result.put(num, temp);
            } else {
                temp.add(num);
            }
        }

        return result.entrySet().stream()
                .sorted((o1, o2) -> o1.getValue().size() == o2.getValue().size()
                        ? o2.getKey() - o1.getKey() : o1.getValue().size() - o2.getValue().size())
                .map(Map.Entry::getValue)
                .flatMap(Collection::stream)
                .mapToInt(Integer::intValue)
                .toArray();
    }
}
