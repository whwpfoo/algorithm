import java.util.Stack;

// https://leetcode.com/problems/product-of-array-except-self/
public class ProductOfArrayExceptSelf {
    public int[] productExceptSelf(int[] nums) {
        int[] forward = new int[nums.length];
        int[] back = new int[nums.length];

        int f = 1, b = 1;

        for (int i = 0; i < nums.length; i++) {
            f *= nums[i]; b *= nums[nums.length - 1 - i];
            forward[i] = f;
            back[nums.length - 1 - i] = b;
        }

        int[] answer = new int[nums.length];

        for (int i = 0; i < nums.length; i++) {
            if (i == 0) {
                answer[i] = back[i + 1];
                continue;
            }
            if (i == nums.length - 1) {
                answer[i] = forward[i - 1];
                continue;
            }
            answer[i] = forward[i - 1] * back[i + 1];
        }

        return answer;
    }
}
