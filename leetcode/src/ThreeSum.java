import java.util.*;

// https://leetcode.com/problems/3sum/
public class ThreeSum {
    public List<List<Integer>> threeSum(int[] nums) {
        if (nums.length < 3) {
            return Collections.emptyList();
        }

        Set<List<Integer>> result = new HashSet<>();
        Arrays.sort(nums);

        for (int i = 0; i < nums.length; i++) {
            int standard = -nums[i];
            int next = i + 1;
            int last = nums.length - 1;

            while (next < last) {
                if (nums[next] + nums[last] == standard) {
                    result.add(Arrays.asList(-standard, nums[next], nums[last]));
                    next++;
                    last--;
                } else if (nums[next] + nums[last] > standard) {
                    last--;
                } else {
                    next++;
                }
            }
        }

        return new ArrayList(result);
    }

    public static void main(String[] args) {
        ThreeSum obj = new ThreeSum();
        int[] nums = {-1,0,1,2,-1,-4};
        obj.threeSum(nums);
    }
}
