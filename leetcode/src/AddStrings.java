/**
 * https://leetcode.com/problems/add-strings/
 */
public class AddStrings {
    public String addStrings(String num1, String num2) {
        int num1Idx = num1.length() - 1;
        int num2Idx = num2.length() - 1;
        int carry = 0;

        StringBuilder result = new StringBuilder();

        for ( ; num1Idx >= 0 || num2Idx >= 0;) {
            int sum = carry;
            if (num1Idx >= 0) sum += num1.charAt(num1Idx--) - '0';
            if (num2Idx >= 0) sum += num2.charAt(num2Idx--) - '0';

            carry = sum > 9 ? 1 : 0;
            result.append(sum % 10);
        }

        if (carry != 0) result.append(carry);
        return result.reverse().toString();
    }
}
