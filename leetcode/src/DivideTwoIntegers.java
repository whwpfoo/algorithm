// https://leetcode.com/problems/divide-two-integers/
public class DivideTwoIntegers {
    public int divide(int dividend, int divisor) {
        // -2147483648 <= dividend, divisor <= 2147483647
        if (divisor == -1 && dividend == -2147483648) {
            return 2147483647;
        }
        return dividend / divisor;
    }
}
