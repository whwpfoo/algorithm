import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode.com/problems/path-crossing/
 */
public class PathCrossing {
    public boolean isPathCrossing(String path) {
        Set<Integer> positionSet = new HashSet<>();
        int xPos = 0, yPos = 0;
        positionSet.add(10000 * xPos + yPos);

        for (char c : path.toCharArray()) {
            switch (c) {
                case 'N':
                    yPos++;
                    break;
                case 'S':
                    yPos--;
                    break;
                case 'E':
                    xPos++;
                    break;
                case 'W':
                    xPos--;
                    break;
                default:
                    break;
            }

            if (positionSet.contains(10000 * xPos + yPos)) {
                return true;
            }

            positionSet.add(10000 * xPos + yPos);
        }

        return false;
    }
}
