import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/
 */
public class LongestSubstringWithoutRepeatingCharacters {
    public static void main(String[] args) {
        lengthOfLongestSubstring("abcabcbb");
    }
    public static int lengthOfLongestSubstring(String s) {
        if (s.length() == 0) return 0;
        if (s.length() == 1) return 1;
        Map<Character, Character> charMap = new HashMap<>();
        int result = Integer.MIN_VALUE;
        int index = 0;

        while (index < s.length()) {
            for (int i = index; i < s.length(); i++) {
                char target = s.charAt(i);
                if (charMap.get(target) == null) charMap.put(target, target);
                else break;
            }
            result = Math.max(result, charMap.size());
            index++;
            charMap.clear();
        }

        return result;
    }
}
