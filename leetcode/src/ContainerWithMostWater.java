// https://leetcode.com/problems/container-with-most-water/
public class ContainerWithMostWater {
    public int maxArea(int[] height) {
        int l = 0;
        int r = height.length - 1;
        int max = Integer.MIN_VALUE;

        while (l < r) {
            int lh = height[l], rh = height[r], width = 0;
            if (lh < rh) {              // 오른쪽 막대가 더 큰 경우
                width = (r - l) * lh;   // 왼쪽 막대를 기준으로 넓이를 구한다
                l++;                    // 오른쪽이 더 크기 때문에 작은 쪽인 왼쪽을 증가 시킨다
            } else if (lh > rh) {       // 왼쪽 막대가 더 큰 경우
                width = (r - l) * rh;   // 오른쪽 막대를 기준으로 넓이를 구한다
                r--;                    // 왼쪽이 더 크기 때문에 더 작은 오른쪽을 증가 시킨다
            } else {                    // 양쪽 막대의 길이가 같은 경우
                width = (r - l) * lh;   // 어느쪽을 높이를 기준으로 하든 상관 없음
                l++;                    // 양쪽 높이가 같다면 어느 한쪽을 줄여도
                r--;
            }
            max = Math.max(max, width);
        }

        return max;
    }
}
