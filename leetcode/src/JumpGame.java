import java.util.*;

// https://leetcode.com/problems/jump-game/
public class JumpGame {
    public boolean canJump(int[] nums) {
        List<Integer> zeroIndexList = new ArrayList<>();
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == 0) zeroIndexList.add(i);
        }
        main:
        for (int zero : zeroIndexList) {
            int jump = 2;
            for (int i = zero - 1; i >= 0; i--) {
                if (nums[i] >= jump) {
                    continue main;
                }
                jump++;
            }
            return false;
        }
        return true;
    }
}
