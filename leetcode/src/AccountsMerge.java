import java.util.*;

// https://leetcode.com/problems/accounts-merge/
public class AccountsMerge {
    public List<List<String>> accountsMerge(List<List<String>> accounts) {
        Map<String, Set<String>> emailRelationMap = new HashMap<>();
        Map<String, String> emailAndNameMap = new HashMap<>();

        for (List<String> account : accounts) {
            for (int i = 1; i < account.size(); i++) {
                if (!emailRelationMap.containsKey(account.get(i))) {
                    emailRelationMap.put(account.get(i), new HashSet<>());
                }
                emailAndNameMap.put(account.get(i), account.get(0));
                if (i == 1) continue;
                emailRelationMap.get(account.get(i - 1)).add(account.get(i));
                emailRelationMap.get(account.get(i)).add(account.get(i - 1));
            }
        }

        Set<String> visited = new HashSet<>();
        List<List<String>> answer = new ArrayList<>();

        for (String email : emailAndNameMap.keySet()) {
            List<String> list = new ArrayList<>();
            if (visited.add(email)) {
                dfs(emailRelationMap, visited, list, email);
                Collections.sort(list);
                list.add(0, emailAndNameMap.get(email));
                answer.add(list);
            }
        }

        return answer;
    }

    public void dfs(Map<String, Set<String>> map, Set<String> visited, List<String> list, String email) {
        list.add(email);
        for (String relationEmail : map.get(email)) {
                if (visited.add(email)) {
                dfs(map, visited, list, relationEmail);
            }
        }
    }
}
