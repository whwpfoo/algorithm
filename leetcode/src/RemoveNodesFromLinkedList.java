/**
 * https://leetcode.com/problems/remove-nodes-from-linked-list/description/?envType=daily-question&envId=2024-05-06
 */
public class RemoveNodesFromLinkedList {
    public ListNode removeNodes(ListNode head) {
        if (head.next == null) {
            return head;
        }

        ListNode node = removeNodes(head.next);

        if (head.val < node.val) {
            head = node;
        } else {
            head.next = node;
        }

        return head;
    }

    public class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }
}
