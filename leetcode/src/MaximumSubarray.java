import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// https://leetcode.com/problems/maximum-subarray/
// Kadane’s Algorithm 부분 배열의 최대합을 구하는 알고리즘
public class MaximumSubarray {
    public static void main(String[] args) {
        MaximumSubarray obj = new MaximumSubarray();
//        int[] nums = {1, -1, 1};
//        int[] nums = {1, -2, 3, 5, -4, 2, 5};
        int[] nums = {-2, -1};
        System.out.println(obj.maxSubArray(nums));
    }

    public int maxSubArray(int[] nums) {
        if (nums.length == 1) return nums[0];

        int store = nums[0];
        int max = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (store + nums[i] > nums[i]) {
                store += nums[i];
            } else {
                store = nums[i];
            }
            max = Math.max(max, store);
        }

        return max;
    }
}
