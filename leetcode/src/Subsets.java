import java.util.*;

/**
 * https://leetcode.com/problems/subsets/description/?envType=daily-question&envId=2024-05-21
 */
public class Subsets {
    List<List<Integer>> answer = new ArrayList<>();

    public List<List<Integer>> subsets(int[] nums) {
        subset(nums, 0, nums.length, new Stack<Integer>());
        return answer;
    }

    public void subset(int[] nums, int start, int end, Stack<Integer> state) {
        if (start >= end) {
            var l = new ArrayList<Integer>();
            for (int value : state) {
                l.add(value);
            }
            answer.add(l);
            return;
        }
        state.push(nums[start]);
        subset(nums, start + 1, end, state);
        state.pop();
        subset(nums, start + 1, end, state);
    }
}
