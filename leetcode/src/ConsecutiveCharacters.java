/**
 * https://leetcode.com/problems/consecutive-characters/
 */
public class ConsecutiveCharacters {
    public int maxPower(String s) {
        int len = s.length();
        int begin = 0, index = 0, max = 0;

        while (index < len) {
            while (index < len && s.charAt(index) == s.charAt(begin)) {
                max = Math.max(max, index - begin + 1);
                index++;
            }

            if (index == len) return max;

            begin = index;
        }

        return max;
    }
}
