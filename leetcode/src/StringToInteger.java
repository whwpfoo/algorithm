import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * https://leetcode.com/problems/string-to-integer-atoi/
 */
public class StringToInteger {
    public static void main(String[] args) {
    }

    public int myAtoi(String s) {
        Matcher m = Pattern.compile("^\\s*([+\\-])?(\\d+).*$").matcher(s);
        if (m.matches()) {
            boolean isPositive = "-".equals(m.group(1)) ? false : true;
            String letters = m.group(2);
            BigInteger num;

            if (isPositive) {
                num = new BigInteger(letters);
                return num.compareTo(new BigInteger("" + Integer.MAX_VALUE)) == 1 ? Integer.MAX_VALUE : num.intValue();
            } else {
                num = new BigInteger("-" + letters);
                return num.compareTo(new BigInteger("" + Integer.MIN_VALUE)) == -1 ? Integer.MIN_VALUE : num.intValue();
            }
        }

        return 0;
    }
}
