import java.util.Arrays;

public class LongestConsecutiveSequence {
    public int longestConsecutive(int[] nums) {
        if (nums.length == 0) return 0;

        int[] arr = Arrays.stream(nums)
                .distinct()
                .sorted()
                .toArray();
        int result = 0;
        int tempCnt = 1;

        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] + 1 == arr[i + 1]) {
                tempCnt++;
            } else {
                result = Math.max(result, tempCnt);
                tempCnt = 1;
            }
        }

        result = Math.max(result, tempCnt);
        return result;
    }
}
