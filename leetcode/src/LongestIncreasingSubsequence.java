import java.util.ArrayList;
import java.util.List;

// https://leetcode.com/problems/longest-increasing-subsequence/
public class LongestIncreasingSubsequence {
    public static void main(String[] args) {
        LongestIncreasingSubsequence obj = new LongestIncreasingSubsequence();
        int[] nums = {0,1,0,3,2,3};
        System.out.println(obj.lengthOfLIS(nums));
//        List<Integer> temp = Arrays.asList(7);
//        obj.getLowerBoundIndex(temp, 7);
    }

    // lower bound 를 이용해 풀어야 함
    public int lengthOfLIS(int[] nums) {
        List<Integer> LIS = new ArrayList<>();

        for (int i = 0; i < nums.length; i++) {
            if (LIS.size() == 0 || LIS.get(LIS.size() - 1) < nums[i]) {
                LIS.add(nums[i]);
                continue;
            }
            // nums[i] 가 LIS 의 마지막 값 보다 크다면 해당 값을 LIS lower bound 위치에 업데이트 해줘야 함
            int index = getLowerBoundIndex(LIS, nums[i]);
            LIS.set(index, nums[i]);
        }

        return LIS.size();
    }

    private int getLowerBoundIndex(List<Integer> lis, int target) {
        int start = 0;
        int end = lis.size();
        int mid;
        while (start < end) {
            mid = (start + end) / 2;
            if (lis.get(mid) < target) {
                start = mid + 1;
            } else {
                end = mid;
            }
        }
        return end;
    }
}
