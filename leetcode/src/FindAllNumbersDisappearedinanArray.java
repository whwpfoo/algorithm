import java.util.*;

// https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/
public class FindAllNumbersDisappearedinanArray {
    public List<Integer> findDisappearedNumbers(int[] nums) {
        int[] count = new int[nums.length + 1];

        for (int i = 0; i < nums.length; i++) {
            count[nums[i]]++;
        }

        List<Integer> result = new ArrayList<>();

        for (int i = 1; i < count.length; i++) {
            if (count[i] == 0)
                result.add(i);
        }

        return result;
    }
}
