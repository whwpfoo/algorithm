import java.util.Arrays;

/**
 * https://leetcode.com/problems/shortest-distance-to-a-character/
 */
public class ShortestDistanceToACharacter {
    public int[] shortestToChar(String S, char C) {
        int[] pos = findTargetCharacterPosition(S, C);
        int[] result = new int[S.length()];
        Arrays.fill(result, Integer.MAX_VALUE);

        for (int i = 0; i < S.length(); i++) {
            for (int j = 0; j < pos.length; j++) {
                if (pos[j] == i) {
                    result[i] = 0;
                    break;
                }

                result[i] = Math.min(result[i], Math.abs(pos[j] - i));
            }
        }

        return result;
    }

    public int[] findTargetCharacterPosition(String s, char c) {
        String temp = s.replaceAll(String.valueOf(c), "");
        int targetLength = s.length() - temp.length();
        int[] targetPosArray = new int[targetLength];
        int idx = 0;

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c)
                targetPosArray[idx++] = i;
        }

        return targetPosArray;
    }
}
