import java.util.stream.Stream;

/**
 * https://leetcode.com/problems/reverse-words-in-a-string-iii/
 */
public class ReverseWordsInAStringIII {
    public String reverseWords(String s) {
        String[] letters = s.split(" ");
        return Stream.of(letters).map(ReverseWordsInAStringIII::reverse).reduce((s1, s2) -> s1 + " " + s2).get().trim();
    }

    public static String reverse(String t) {
        StringBuilder sb = new StringBuilder();
        t.chars().forEach(c -> sb.insert(0, (char)c));
        return sb.toString();
    }
}
