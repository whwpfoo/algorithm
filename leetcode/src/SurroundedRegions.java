import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

// https://leetcode.com/problems/surrounded-regions/
public class SurroundedRegions {
    int[][] direct = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
    boolean[][] visited;

    public void solve(char[][] board) {
        int M = board.length;
        int N = board[0].length;
        visited = new boolean[M][N];

        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (board[i][j] == 'O' && visited[i][j] == false) {
                    flipe( isSurrounded( getOCoord(board, i, j), M, N ), board );
                }
            }
        }
    }

    public List<int[]> getOCoord(char[][] board, int row, int col) {
        List<int[]> coordList = new ArrayList<>();
        Queue<int[]> q = new LinkedList<>();

        q.offer(new int[] {row, col});
        visited[row][col] = true;

        while (q.isEmpty() == false) {
            int[] coord = q.poll();
            coordList.add(coord);

            for (int i = 0; i < 4; i++) {
                int drow = coord[0] + direct[i][0];
                int dcol = coord[1] + direct[i][1];

                if (drow < 0 || drow >= board.length || dcol < 0 || dcol >= board[0].length) continue;
                if (visited[drow][dcol]) continue;
                if (board[drow][dcol] == 'X') continue;

                q.offer(new int[] {drow, dcol});
                visited[drow][dcol] = true;
            }
        }

        return coordList;
    }

    public List<int[]> isSurrounded(List<int[]> coordList, int M, int N) {
        for (int[] coord : coordList) {
            if (coord[0] == 0 || coord[0] == M - 1 || coord[1] == 0 || coord[1] == N - 1) {
                return new ArrayList<int[]>();
            }
        }
        return coordList;
    }

    public void flipe(List<int[]> coordList, char[][] board) {
        for (int[] coord : coordList) {
            board[coord[0]][coord[1]] = 'X';
        }
    }
}
