/**
 * https://leetcode.com/problems/search-insert-position/
 */
public class SearchInsertPosition {
    public int searchInsert(int[] nums, int target) {
        int pos = 0;
        for ( ; pos < nums.length;) {
            if (nums[pos] < target) {
                pos++;
            } else {
                break;
            }
        }

        if (pos == 0) return 0;
        if (pos == nums.length) return nums.length;
        return pos;
    }
}
