import java.util.*;

// https://leetcode.com/problems/minimum-absolute-difference/
public class MinimumAbsoluteDifference {
    public List<List<Integer>> minimumAbsDifference(int[] arr) {
        Arrays.sort(arr);

        List<List<Integer>> result = new ArrayList<>();
        int minDiff = Integer.MAX_VALUE;

        for (int i = 1; i < arr.length; i++) {
            int diff = Math.abs(arr[i] - arr[i - 1]);

            if (diff > minDiff) {
                continue;
            } else if (diff < minDiff) {
                result.clear();
            }

            minDiff = diff;
            result.add(Arrays.asList(arr[i - 1], arr[i]));
        }

        return result;
    }
}
