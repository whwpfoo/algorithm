import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static java.util.Comparator.comparing;

// https://leetcode.com/problems/merge-intervals/
public class MergeIntervals {
    public int[][] merge(int[][] intervals) {

//        Arrays.sort(intervals, Comparator.<int[], Integer>comparing(arr -> arr[1])
//                        .thenComparing(Comparator.<int[], Integer>comparing(arr -> arr[0]).reversed()));

        Arrays.sort(intervals, Comparator.comparingInt(arr -> arr[1]));
        List<int[]> mergeList = new ArrayList<>();
        int[] target = intervals[0];

        for (int i = 1; i < intervals.length; i++) {
            if (target[1] >= intervals[i][0]) {
                target[0] = Math.min(target[0], intervals[i][0]);
                target[1] = Math.max(target[1], intervals[i][1]);
            } else {
                mergeList.add(target);
                target = intervals[i];
            }
        }

        mergeList.add(target);

        int[][] answer = new int[mergeList.size()][2];
        for (int i = 0; i < answer.length; i++) {
            answer[i] = mergeList.get(i);
        }

        return answer;
    }
}
