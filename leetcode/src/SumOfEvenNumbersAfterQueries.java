/**
 * https://leetcode.com/problems/sum-of-even-numbers-after-queries/
 */
public class SumOfEvenNumbersAfterQueries {
    public int[] sumEvenAfterQueries(int[] A, int[][] queries) {
        int sum = 0;
        for (int num : A) {
            if (num % 2 == 0) sum += num;
        }
        int[] result = new int[queries.length];

        for (int i = 0; i < queries.length; i++) {
            int val = queries[i][0]; int pos = queries[i][1];
            if (A[pos] % 2 == 0) sum -= A[pos];
            A[pos] += val;
            if (A[pos] % 2 == 0) sum += A[pos];
            result[i] = sum;
        }

        return result;
    }
}
