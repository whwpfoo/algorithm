import java.util.LinkedList;
import java.util.Queue;

// https://leetcode.com/problems/number-of-islands/
public class NumberOfIslands {
    int[][] direct = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

    public int numIslands(char[][] grid) {
        int result = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == '1') {
                    result++;
                    grid[i][j] = '0';
                    Queue<int[]> q = new LinkedList<>();
                    q.offer(new int[] {i, j});
                    while (q.isEmpty() == false) {
                        int[] coord = q.poll();
                        int x = coord[0];
                        int y = coord[1];
                        for (int[] d : direct) {
                            int dx = x + d[0];
                            int dy = y + d[1];
                            if (dx < 0 || dy < 0 || dx > grid.length - 1 || dy > grid[0].length - 1) continue;
                            if (grid[dx][dy] != '1') continue;
                            q.offer(new int[] {dx, dy});
                            grid[dx][dy] = '0';
                        }
                    }
                }
            }
        }
        return result;
    }
}
