use strict;
use warnings;


# RGB거리: https://www.acmicpc.net/problem/1149
sub main {
	chomp(my $n = <>);


	my @costs  = ();
	my @result = ();


	foreach (1 .. $n) {
		chomp(my $v = <>);
		push @costs, [split " ", $v]; 
		push @result, [0, 0, 0];
	}


	$result[0][0] = $costs[0][0];
	$result[0][1] = $costs[0][1];
	$result[0][2] = $costs[0][2];


	# Down-Top 상향식 접근방법
	# 빨강, 초록, 파랑을 각각 선택했을 때 최소비용 구성 (DP 테이블) 
	foreach (1 .. $#costs) {
		$result[$_][0] = min($result[$_ - 1][1], $result[$_ - 1][2]) + $costs[$_][0];
		$result[$_][1] = min($result[$_ - 1][0], $result[$_ - 1][2]) + $costs[$_][1];
		$result[$_][2] = min($result[$_ - 1][0], $result[$_ - 1][1]) + $costs[$_][2];
	}


	print min($result[$n - 1][0], min($result[$n - 1][1], $result[$n - 1][2]));
}

sub min {
	my ($a, $b) = @_;
	return $a > $b ? $b : $a;
}

main();
exit;
