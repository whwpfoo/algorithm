import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 바이러스 :https://www.acmicpc.net/problem/2606
 */
public class _2606 {
    private static int n;
    private static int v;
    private static int[][] nodes;
    private static boolean[] visited;

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        n = Integer.parseInt(br.readLine()); // 총 컴퓨터의 갯수
        v = Integer.parseInt(br.readLine()); // 노드간의 간선

        nodes = new int[n + 1][n + 1];
        visited = new boolean[n + 1];

        for (int i = 0; i < v; i++) {
            String foo = (br.readLine());
            String[] edges = foo.split(" ");
            nodes[Integer.parseInt(edges[0])][Integer.parseInt(edges[1])] =
            nodes[Integer.parseInt(edges[1])][Integer.parseInt(edges[0])] = 1;
        }

        dfs(1);

        int spreadCount = 0;

        for (int i = 2; i < visited.length; i++) {
            if (visited[i]) {
                spreadCount++;
            }
        }

        System.out.println(spreadCount);
    }

    public static void dfs(int n) {
        visited[n] = true;

        for (int i = 1; i < visited.length; i++) {
            if (nodes[n][i] == 1 && !visited[i]) {
                dfs(i);
            }
        }
    }
}