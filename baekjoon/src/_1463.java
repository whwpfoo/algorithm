import java.util.Arrays;
import java.util.Scanner;

/**
 * 1로 만들기 : https://www.acmicpc.net/problem/1463
 *
 * 정수 X에 사용할 수 있는 연산은 다음과 같이 세 가지 이다.
 *
 * X가 3으로 나누어 떨어지면, 3으로 나눈다.
 * X가 2로 나누어 떨어지면, 2로 나눈다.
 * 1을 뺀다.
 * 정수 N이 주어졌을 때, 위와 같은 연산 세 개를 적절히 사용해서 1을 만들려고 한다.
 * 연산을 사용하는 횟수의 최솟값을 출력하시오.
 *
 */
public class _1463 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        Arrays.fill(dp, -1);
        System.out.println(getMinOperation(N));
    }

    static int[] dp = new int[100001];

    public static int getMinOperation(int N) {
        if (N == 1) return 0;
        if (dp[N] != -1) return dp[N];

        int result = getMinOperation(N - 1) + 1;
        if (N % 3 == 0) result = Math.min(result, getMinOperation(N / 3 ) + 1);
        if (N % 2 == 0) result = Math.min(result, getMinOperation(N / 2 ) + 1);
        dp[N] = result;
        return result;
    }
}
