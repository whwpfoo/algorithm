import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 * 미로 탐색: https://www.acmicpc.net/problem/2178
 */
public class _2178 {
    static int[][] array;
    static int[][] visited;
    static int n, m;

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());

        n = Integer.parseInt(st.nextToken());
        m = Integer.parseInt(st.nextToken());

        array = new int[n + 1][m + 1];
        visited = new int[n + 1][m + 1];

        for (int i = 1; i <= n; i++) {
            String input = br.readLine();
            for (int j = 1; j <= m; j++) {
                array[i][j] = input.charAt(j - 1) - '0';
            }
        }
        bfs();
    }

    public static void bfs() {
        Queue<XY> q = new LinkedList<>();
        q.offer(new XY(1, 1));

        int[] xArray = {-1, 0, 1, 0};
        int[] yArray = {0, 1, 0, -1};

        visited[1][1] = 1;

        while (q.isEmpty() == false) {
            XY xy = q.poll();
            int x = xy.x;
            int y = xy.y;

            for (int i = 0; i < 4; i++) {
                int row = x + xArray[i];
                int col = y + yArray[i];

                if ((row < 1 || row > n || col < 1 || col > m) ||
                     visited[row][col] != 0 || array[row][col] == 0) {
                    continue;
                }

                q.offer(new XY(row, col));
                visited[row][col] = visited[x][y] + 1;
            }
        }
        System.out.println(visited[n][m]);
    }

    static class XY {
        int x;
        int y;
        public XY(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}




