import java.util.Scanner;

/**
 * 거스름돈 : https://www.acmicpc.net/problem/5585
 *
 * 입력
 * 입력은 한줄로 이루어져있고, 타로가 지불할 돈(1 이상 1000미만의 정수) 1개가 쓰여져있다.
 *
 * 출력
 * 제출할 출력 파일은 1행으로만 되어 있다. 잔돈에 포함된 매수를 출력하시오.
 *
 *  500엔, 100엔, 50엔, 10엔, 5엔, 1엔
 */
public class _5585 {
    public static void main(String[] args) {
        int[] changeMoney = new int[] {500, 100, 50, 10, 5, 1};

        Scanner sc = new Scanner(System.in);
        int money = 1000 - sc.nextInt();
        int chageCount = 0;

        for (int i = 0; i < changeMoney.length; i++) {
            if (money != 0) {
                chageCount += money / changeMoney[i];
                money = money % changeMoney[i];
            }
        }

        System.out.println(chageCount);
    }
}
