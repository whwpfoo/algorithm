import sun.awt.windows.WPrinterJob;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * DFS와 BFS: https://www.acmicpc.net/problem/1260
 */
public class _1260 {
    public static int[][] vertex;
    public static boolean[] visited;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int c = sc.nextInt();
        int root = sc.nextInt();

        sc.nextLine();

        vertex = new int[n + 1][n + 1];
        visited = new boolean[n + 1];

        for (int i = 0; i < c; i++) {
            int v1 = sc.nextInt();
            int v2 = sc.nextInt();

            vertex[v1][v2] = vertex[v2][v1] = 1;
            sc.nextLine();
        }

        dfs(vertex, visited, root);
        System.out.println();
        Arrays.fill(visited, false);
        bfs(vertex, visited, root);

    }

    public static void dfs(int[][] array, boolean[] visited, int root) {
        visited[root] = true;

        System.out.print(root + " ");

        for (int i = 1; i < visited.length; i++) {
            if (array[root][i] == 1 && visited[i] == false) {
                dfs(array, visited, i);
            }
        }
    }

    public static void bfs(int[][] array, boolean[] visited, int root) {
        Queue<Integer> q = new LinkedList<>();

        q.add(root);
        visited[root] = true;

        while (q.isEmpty() == false) {
            root = q.poll();
            System.out.print(root + " ");

            for (int i = 1; i < visited.length; i++) {
                if (array[root][i] == 1 && visited[i] == false) {
                    q.offer(i);
                    visited[i] = true;
                }
            }
        }
    }
}
