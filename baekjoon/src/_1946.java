import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;

/**
 * 신입 사원: https://www.acmicpc.net/problem/1946
 *
 */
public class _1946 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int testCaseNumber = Integer.parseInt(br.readLine());

        for (int i = 0; i < testCaseNumber; i++) {
            int memberCount = Integer.parseInt(br.readLine());
            MemberScore[] memberScoreArray = new MemberScore[memberCount];

            for (int j = 0; j < memberCount; j++) {
                String[] score = br.readLine().split(" ");
                int pageScore = Integer.parseInt(score[0]);
                int interviewScore = Integer.parseInt(score[1]);
                MemberScore member = new MemberScore(pageScore, interviewScore);
                memberScoreArray[j] = member;
            }

            Arrays.sort(memberScoreArray, new Comparator<MemberScore>() {
                @Override
                public int compare(MemberScore o1, MemberScore o2) {
                    return o1.page - o2.page;
                }
            });

            int passCount = memberCount;
            int interviewSocre = memberScoreArray[0].interview;

            for (int j = 1; j < memberCount; j++) {
                if (memberScoreArray[j].interview > interviewSocre) {
                    passCount--;
                    continue;
                }
                interviewSocre = memberScoreArray[j].interview;
            }

            System.out.println(passCount);
        }
    }

    public static class MemberScore {

        public int page;
        public int interview;

        public MemberScore(int page, int interview) {
            this.page = page;
            this.interview = interview;
        }
    }
}
