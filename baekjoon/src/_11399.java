import java.util.Arrays;
import java.util.Scanner;

/**
 * ATM : https://www.acmicpc.net/problem/11399
 * 줄을 서 있는 사람의 수 N과 각 사람이 돈을 인출하는데 걸리는 시간 Pi가 주어졌을 때, 각 사람이 돈을 인출하는데 필요한 시간의 합의 최솟값을 구하는 프로그램을 작성하시오.
 * 입력: 첫째 줄에 사람의 수 N(1 ≤ N ≤ 1,000)이 주어진다. 둘째 줄에는 각 사람이 돈을 인출하는데 걸리는 시간 Pi가 주어진다. (1 ≤ Pi ≤ 1,000)
 * 출력: 첫째 줄에 각 사람이 돈을 인출하는데 필요한 시간의 합의 최솟값을 출력한다.
 */
public class _11399 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int peopleCount = sc.nextInt();
        int[] workTime = new int[peopleCount];

        for (int i = 0; i < peopleCount; i++) {
            workTime[i] = sc.nextInt();
        }
        Arrays.sort(workTime);

        int result = 0;

        for (int i = 0; i < peopleCount; i++) {
            for (int j = 0; j <= i; j++) {
                result += workTime[j];
            }
        }

        System.out.println(result);
        sc.close();
    }
}
