import java.util.Scanner;

/**
 * 설탕 배달: https://www.acmicpc.net/problem/2839
 * 입력: 첫째 줄에 N이 주어진다. (3 ≤ N ≤ 5000)
 * 출력: 상근이가 배달하는 봉지의 최소 개수를 출력한다. 만약, 정확하게 N킬로그램을 만들 수 없다면 -1을 출력한다.
 * 3킬로그램 봉지와 5킬로그램 봉지가 있다.
 */
public class _2839 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int totalSugar = sc.nextInt();

        int result = totalSugar / 5;
        totalSugar = totalSugar % 5;

        while (result >= 0 && totalSugar != 0) {
            if (totalSugar % 3 == 0 && totalSugar / 3 != 0) {
                result += totalSugar / 3;
                totalSugar = totalSugar % 3;
            } else {
                totalSugar += 5;
                result--;
            }
        }

        if (totalSugar == 0) {
            System.out.println(result);
        } else {
            System.out.println(-1);
        }

        sc.close();
    }
}
