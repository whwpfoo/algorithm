import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 전자레인지 : https://www.acmicpc.net/problem/10162
 *
 * 입력
 * 첫 번째 줄에는 요리시간 T(초)가 정수로 주어져 있으며 그 범위는 1 ≤ T ≤ 10,000 이다.
 *
 * 출력
 * 여러분은 T초를 위한 최소버튼 조작의 A B C 횟수를 첫 줄에 차례대로 출력해야 한다.
 * 각각의 횟수 사이에는 빈 칸을 둔다. 해당 버튼을 누르지 않는 경우에는 숫자 0을 출력해야한다.
 * 만일 제시된 3개의 버튼으로 T초를 맞출 수 없으면 음수 -1을 첫 줄에 출력해야 한다.
 *
 * 버튼 A, B, C에 지정된 시간은 각각 5분, 1분, 10초이다.
 * ex) 100 > 0 1 4
 */
public class _10162 {

    public static final int A = 300;
    public static final int B = 60;
    public static final int C = 10;

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int T = Integer.parseInt(br.readLine());

        StringBuilder sb = new StringBuilder();

        sb.append(T / A).append(" ");
        T = T % A;

        sb.append(T / B).append(" ");
        T = T % B;

        sb.append(T / C);
        T = T % C;

        if (T > 0) {
            System.out.println(-1);
        } else {
            System.out.println(sb);
        }
    }
}
