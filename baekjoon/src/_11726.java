import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * 2×n 타일링 : https://www.acmicpc.net/problem/11726
 */
public class _11726 {
    static long[] dp;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        dp = new long[N + 1];
        Arrays.fill(dp, -1);
        long result = fillTilingCount(N);
        System.out.println(result);


        int[] array = Arrays.stream(new int[1000]).map(v -> 1).toArray();
        int nums = 0;
        for (int num : array) {
            nums += num;
        }
        System.out.println(nums);

    }

    private static long fillTilingCount(int N) {
        if (N == 1 || N == 0) return 1;
        else if (N == 2) return 2;

        if (dp[N] != -1) return dp[N];

        dp[N] = fillTilingCount(N - 1) + fillTilingCount(N - 2);
        dp[N] %= 10007;
        return dp[N];
    }
}
