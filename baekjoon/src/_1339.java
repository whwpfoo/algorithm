import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 단어 수학: https://www.acmicpc.net/problem/1339
 */
public class _1339 {
    public static String[] numberArray = new String[] {"9", "8", "7", "6", "5", "4", "3", "2", "1", "0"};
    public static Map<String, Integer> alphabet2Number = new HashMap<>();

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder sb = new StringBuilder();
        int numberOfInputs = Integer.parseInt(br.readLine());

        for ( ; numberOfInputs > 0; ) {
            String word = br.readLine();
            String[] wordArray = word.split(""); // 단어를 잘랐다
            sb.append(word).append("|");
            int wordSize = wordArray.length;

            for (int i = 0; i < wordArray.length; i++) {
                if (alphabet2Number.containsKey(wordArray[i])) {
                    alphabet2Number.put(wordArray[i], alphabet2Number.get(wordArray[i]) + (int)Math.pow(10, wordSize - 1));
                } else {
                    alphabet2Number.put(wordArray[i], (int)Math.pow(10, wordSize - 1));
                }
                wordSize--;
            }
            numberOfInputs--;
        }

        List<Map.Entry<String, Integer>> list = alphabet2Number.entrySet().stream().sorted((o1, o2) -> o2.getValue() - o1.getValue()).collect(Collectors.toList());
        int index = 0;

        for (Map.Entry<String, Integer> entry : list) {
            alphabet2Number.put(entry.getKey(), Integer.parseInt(numberArray[index++]));
        }

        int result = 0;
        String[] wordArray = sb.toString().split("\\|");
        for (int i = 0; i < wordArray.length; i++) {
            String word = wordArray[i];
            String temp = "";
            for (int j = 0; j < word.length(); j++) {
                temp += alphabet2Number.get(String.valueOf(word.charAt(j)));
            }
            result += Integer.parseInt(temp);
        }

        System.out.println(result);
    }
}
