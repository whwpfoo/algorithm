import java.util.*;

// https://www.acmicpc.net/problem/23288 - 주사위 굴리기
public class _23288 {
    static int n;
    static int m;
    static int move;
    static int[][] map;

    public static void main(String[] args) {
        init();
        Dice dice = new Dice();
        for (int i = 0; i < move; i++) {
            dice.roll();
            dice.addScore(map);
            dice.changeDirect(map);
        }
        System.out.println(dice.score);
    }

    static class Dice {
        int direction = 1;
        int[][] direct = {{-1, 0}, {0, 1}, {1, 0}, {0, - 1}};
        int[] position = {1, 4, 5, 3, 2, 6};
        int[] pos = new int[] {0, 0};
        int score = 0;

        public Dice() {}

        public void roll() {

            if (direction == 0) {
                setPosition(new int[] {0, 4, 5, 2});
            } else if (direction == 1) {
                setPosition(new int[] {0, 3, 5, 1});
            } else if (direction == 2) {
                setPosition(new int[] {0, 2, 5, 4});
            } else if (direction == 3) {
                setPosition(new int[] {0, 1, 5, 3});
            }

            pos[0] += direct[direction][0];
            pos[1] += direct[direction][1];
        }

        public void setPosition(int[] positions) {
            List<Integer> s = new ArrayList<>();
            for (int p : positions) {
                s.add(position[p]);
            }
            position[positions[0]] = s.get(s.size() - 1);
            for (int i = 1; i <= positions.length - 1; i++) {
                position[positions[i]] = s.get(i - 1);
            }
        }

        public int[] changeDirect(int[][] map) {
            int value = map[pos[0]][pos[1]];
            if (position[5] > value) {
                direction = direction % 4 + 1;
                if (direction == 4) direction = 0;
            } else if (position[5] < value) {
                direction = direction % 4 - 1;
                if (direction < 0) direction = 3;
            }
            int row = pos[0] + direct[direction][0];
            int col = pos[1] + direct[direction][1];
            if (row < 0 || row >= n || col < 0 || col >= m) {
                direction = (direction == 0 || direction == 1) ? direction + 2 : direction - 2;
            }
            return direct[direction];
        }

        public void addScore(int[][] map) {
            int value = map[pos[0]][pos[1]];
            int result = 0;
            boolean[][] visited = new boolean[n][m];
            Queue<int[]> q = new LinkedList<>();
            q.offer(new int[]{pos[0], pos[1]});
            visited[pos[0]][pos[1]] = true;
            int moveCount = 0;
            while (!q.isEmpty()) {
                int[] poll = q.poll();
                for (int j = 0; j < 4; j++) {
                    int rrow = poll[0] + direct[j][0];
                    int ccol = poll[1] + direct[j][1];
                    if (rrow < 0 || rrow >= n || ccol < 0 || ccol >= m) continue;
                    if (visited[rrow][ccol]) continue;
                    if (map[rrow][ccol] != value) continue;
                    q.offer(new int[] {rrow, ccol});
                    visited[rrow][ccol] = true;
                    moveCount++;
                }
            }
            result = (moveCount + 1) * value;
            score += result;
        }
    }

    static void init() {
        Scanner input = new Scanner(System.in);
        String[] info = input.nextLine().split(" ");

        n = Integer.parseInt(info[0]);
        m = Integer.parseInt(info[1]);
        move = Integer.parseInt(info[2]);
        map = new int[n][m];

        for (int i = 0; i < n; i++) {
            String[] coords = input.nextLine().split(" ");
            for (int j = 0; j < m; j++) {
                map[i][j] = Integer.parseInt(coords[j]);
            }
        }
    }
}
