import java.util.Scanner;

public class _2752 {
    public static void main(String[] args) {
        int[] data = new int[3];
        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            data[i] = sc.nextInt();
        }

        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data.length - 1 -i; j++) {
                if (data[j] > data[j + 1]) {
                    int temp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = temp;
                }
            }
        }

        for (int result : data) {
            System.out.print(result + " ");
        }
    }
}
