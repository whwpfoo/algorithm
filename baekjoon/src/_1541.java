import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 잃어버린 괄호: https://www.acmicpc.net/problem/1541
 *
 * 입력
 * 첫째 줄에 식이 주어진다. 식은 ‘0’~‘9’, ‘+’, 그리고 ‘-’만으로 이루어져 있고,
 * 가장 처음과 마지막 문자는 숫자이다.
 * 그리고 연속해서 두 개 이상의 연산자가 나타나지 않고,
 * 5자리보다 많이 연속되는 숫자는 없다. 수는 0으로 시작할 수 있다.
 *
 * 출력
 * 첫째 줄에 정답을 출력한다.
 */
public class _1541 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String calculate = br.readLine();
        String[] calculateArr = calculate.split("-");
        int answer = 0;

        for (int i = 0; i < calculateArr.length; i++) {
            String[] number = calculateArr[i].split("\\+");
            int temp = 0;
            for (int j = 0; j < number.length; j++) {
                temp += Integer.parseInt(number[j]);
            }
            if (i != 0) {
                temp = temp * -1;
            }
            answer += temp; // 55
        }

        System.out.println(answer);
    }
}
