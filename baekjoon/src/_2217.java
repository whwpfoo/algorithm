import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * 로프: https://www.acmicpc.net/problem/2217
 *
 * 입력
 * 첫째 줄에 정수 N이 주어진다. 다음 N개의 줄에는 각 로프가 버틸 수 있는 최대 중량이 주어진다. 이 값은 10,000을 넘지 않는 자연수이다.
 *
 * 출력
 * 첫째 줄에 답을 출력한다.
 */
public class _2217 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int ropeCount = Integer.parseInt(br.readLine());
        int[] ropeWeightArray = new int[ropeCount];

        for (int i = 0; i < ropeCount; i++) {
            ropeWeightArray[i] = Integer.parseInt(br.readLine());
        }

        Arrays.sort(ropeWeightArray); // 10 15 20 25 30...... 오름차순

        int result = 0;
        int temp = 0;
        for (int i = ropeWeightArray.length - 1; i >= 0; i--) {
            temp = ropeWeightArray[i] * (ropeCount - i);
            if (temp > result) {
                result = temp;
            }
        }

        System.out.println(result);
    }
}
