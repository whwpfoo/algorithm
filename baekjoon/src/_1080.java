import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * 행렬 : https://www.acmicpc.net/problem/1080
 *
 * 입력
 * 첫째 줄에 행렬의 크기 N M이 주어진다. N과 M은 50보다 작거나 같은 자연수이다.
 * 둘째 줄부터 N개의 줄에는 행렬 A가 주어지고, 그 다음줄부터 N개의 줄에는 행렬 B가 주어진다.
 *
 * 출력
 * 첫째 줄에 문제의 정답을 출력한다. 만약 A를 B로 바꿀 수 없다면 -1을 출력한다.
 * ex
 * 3 4
 * 0000
 * 0010
 * 0000
 *
 * 1001
 * 1011
 * 1001
 */
public class _1080 {
    private static int[][] A;
    private static int[][] B;

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] arraySize = Arrays.stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        A = new int[arraySize[0]][arraySize[1]];
        B = new int[arraySize[0]][arraySize[1]];

        for (int j = 0; j < arraySize[0]; j++) {
            A[j] = Arrays.stream(br.readLine().split("")).mapToInt(Integer::parseInt).toArray();
        }

        for (int j = 0; j < arraySize[0]; j++) {
            B[j] = Arrays.stream(br.readLine().split("")).mapToInt(Integer::parseInt).toArray();
        }

        int count = 0;

        for (int i = 0; A.length - i >= 3; i++) {
            for (int j = 0; A[0].length - j >= 3; j++) {
                if (A[i][j] != B[i][j]) {
                  flipArray(A, i, j);
                  count++;
                }
            }
        }

        if (Arrays.deepEquals(A, B)) {
            System.out.println(count);
        } else {
            System.out.println(-1);
        }

    }

    public static void flipArray(int[][] arr, int row, int column) {
        int tempRow = row;

        for (int i = 0; i < 3; i++) {
            int tempColumn = column;

            for (int j = 0; j < 3; j++) {
                arr[tempRow][tempColumn] = arr[tempRow][tempColumn] == 1 ? 0 : 1;
                tempColumn++;
            }

            tempRow++;
        }
    }
}
