import java.util.Scanner;

public class _2751 {
    public static void main(String[] args) {
        int[] data = new int[1000001];
        Scanner sc = new Scanner(System.in);
        int dataSize = sc.nextInt();


        for (int i = 0; i < dataSize; i++) {
            data[i] = sc.nextInt();
        }

        quickSort(data, 0, dataSize - 1);

        for (int i = 0; i < dataSize; i++) {
            System.out.println(data[i]);
        }

    }

    public static void quickSort(int[] data, int start, int end) {
        int pivot = partition(data, start, end);

        if (start < pivot - 1) {
            quickSort(data, start, pivot - 1);
        }
        if (pivot <= end) {
            quickSort(data, pivot, end);
        }
    }

    public static int partition(int[] data, int start, int end) {
        int left = start;
        int right = end;
        int pivot = data[(left+right) / 2];

        while (left <= right) {
            while (data[left] < pivot) {
                left++;
            }
            while (pivot < data[right]) {
                right--;
            }

            if (left <= right) {
                int temp = data[left];
                data[left] = data[right];
                data[right] = temp;
                left++;
                right--;
            }
        }
        return left;
    }
}
