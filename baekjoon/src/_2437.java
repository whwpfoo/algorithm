import java.util.Arrays;
import java.util.Scanner;

/**
 * 저울 : https://www.acmicpc.net/problem/2437
 *
 * 7
 * 3 1 6 2 7 30 1
 */
public class _2437 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int [] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = scan.nextInt();
        }

        Arrays.sort(array);

        int min = 1;

        for (int i = 0; i < n; i++) {
            if(min < array[i]) {
                break;
            }
            min += array[i];
        }

        System.out.println(min);
    }
}
