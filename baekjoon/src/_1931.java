import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 회의실배정 : https://www.acmicpc.net/problem/1931
 *
 * 입력
 * 첫째 줄에 회의의 수 N(1 ≤ N ≤ 100,000)이 주어진다.
 * 둘째 줄부터 N+1 줄까지 각 회의의 정보가 주어지는데 이것은 공백을 사이에 두고 회의의 시작시간과 끝나는 시간이 주어진다.
 * 시작 시간과 끝나는 시간은 231-1보다 작거나 같은 자연수 또는 0이다.
 *
 * 출력
 * 첫째 줄에 최대 사용할 수 있는 회의의 최대 개수를 출력한다.
 */
public class _1931 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int conferenceCount = Integer.parseInt(br.readLine());

        List<Conference> conferenceList = new ArrayList<>();
        for (int i = 0; i < conferenceCount; i++) {
            String[] time = br.readLine().split(" ");
            int start = Integer.parseInt(time[0]);
            int end = Integer.parseInt(time[1]);
            conferenceList.add(new Conference(start, end));
        }

        conferenceList = conferenceList.stream().sorted().collect(Collectors.toList());

        int end = -1;
        int result = 0;
        for (Conference conference : conferenceList) {
            if (conference.start >= end) {
                end = conference.end;
                result++;
            }
        }

        System.out.println(result);
    }

    public static class Conference implements Comparable {

        private int start;
        private int end;

        public Conference(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public int compareTo(Object o) {
            Conference target = (Conference) o;
            if (this.end == target.end) {
                return this.start - target.start;
            } else {
                return this.end - target.end;
            }
        }
    }
}
