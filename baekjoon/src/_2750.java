import java.util.Scanner;

public class _2750 {
    public static void main(String[] args) {
        int[] data = new int[1000];
        Scanner sc = new Scanner(System.in);

        int dataSize = sc.nextInt();

        for (int i = 0; i < dataSize; i++) {
            data[i] = sc.nextInt();
        }

        for (int i = 0; i < dataSize - 1; i++) {
            for (int j = i + 1; j < dataSize; j++) {
                if (data[i] > data[j]) {
                    int temp = data[i];
                    data[i] = data[j];
                    data[j] = temp;
                }
            }
        }

        for (int i = 0; i < dataSize; i++) {
            System.out.println(data[i]);
        }
    }
}
