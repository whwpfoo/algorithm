import java.util.*;

// https://www.hackerrank.com/challenges/minimum-loss/problem
public class MinimumLoss {
    public static void main(String[] args) {
        List<Long> prices = Arrays.asList(20L, 7L, 8L, 2L, 5L);
        int result = minimumLoss(prices);
        System.out.println(result);
    }

    public static int minimumLoss(List<Long> price) {
        Queue<Long> queue = new LinkedList<>();
        Map<Long, Integer> value2Index = new HashMap<>();
        long[] arr = new long[price.size()];
        for (int i = 0; i < price.size(); i++) {
            long value = price.get(i);
            arr[i] = value;
            value2Index.put(value, i);
            queue.offer(value);
        }
        Arrays.sort(arr);

        Set<Long> check = new HashSet<>();
        long result = Integer.MAX_VALUE;

        while (!queue.isEmpty()) {
            long target = queue.poll();
            int targetIndex = value2Index.get(target);
            int searchIndex = Arrays.binarySearch(arr, target);

            for (int i = searchIndex - 1; i >= 0; i--) {
                long beforeTarget = arr[i];
                if (check.contains(beforeTarget)) {
                    continue;
                }
                int  beforeTargetIndex = value2Index.get(beforeTarget);
                if (targetIndex < beforeTargetIndex && target > beforeTarget) {
                    result = Math.min(result, target - beforeTarget);
                    break;
                }
            }
            check.add(target);
        }
        return (int)result;
    }
}
