import java.util.*;

// https://www.hackerrank.com/challenges/an-interesting-game-1/problem
public class GamingArray {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(5, 2, 6, 3, 4);
        System.out.println(gamingArray(list));
    }

    private static final String ANDY = "ANDY";
    private static final String BOB = "BOB";

    public static String gamingArray(List<Integer> arr) {
        PriorityQueue<Integer> pq = new PriorityQueue<>(Comparator.reverseOrder());
        Stack<Integer> s = new Stack<>();

        arr.forEach(i -> { pq.offer(i); s.push(i); });
        int turn = 0;
        int max = pq.poll();
        while (!s.isEmpty()) {
            if (max != s.peek()) {
                int target = s.pop();
                pq.remove(target);
            } else {
                s.pop();
                if (!pq.isEmpty()) {
                    max = pq.poll();
                    turn++;
                }
            }
        }
        return turn % 2 == 0 ? BOB : ANDY;
    }
}
