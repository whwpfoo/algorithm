import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

// https://www.hackerrank.com/challenges/countingsort4/problem
public class TheFullCountingSort {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream("D:/testcase1")));
        int n = Integer.parseInt(bufferedReader.readLine().trim());
        List<List<String>> arr = new ArrayList<>();
        IntStream.range(0, n).forEach(i -> {
            try {
                arr.add(Stream.of(bufferedReader
                                    .readLine()
                                    .replaceAll("\\s+$", "")
                                    .split(" "))
                                    .collect(toList()));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });
        TheFullCountingSort.countSort(arr);
        bufferedReader.close();
    }

    static int startIdx = Integer.MAX_VALUE;
    static int endIdx = Integer.MIN_VALUE;

    public static void countSort(List<List<String>> arr) {
        StringBuilder[] map = toMap(arr);
        for (int i = startIdx; i <= endIdx; i++) {
            System.out.print(map[i]);
        }
    }

    public static StringBuilder[] toMap(List<List<String>> list) {
        StringBuilder[] sbArr = new StringBuilder[100];
        for (int i = 0; i < 100; i++) {
            sbArr[i] = new StringBuilder();
        }

        int mid = list.size() / 2 - 1;
        int end = list.size();

        for (int i = 0; i < end; i++) {
            List<String> s = list.get(i);
            int num = Integer.parseInt(s.get(0));

            startIdx = Math.min(num, startIdx);
            endIdx = Math.max(num, endIdx);

            if (i <= mid) {
                sbArr[num].append("- ");
            } else {
                sbArr[num].append(s.get(1)).append(" ");
            }
        }

        return sbArr;
    }
}
