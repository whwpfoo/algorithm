
// https://www.hackerrank.com/challenges/recursive-digit-sum/problem
public class RecursiveDigitSum {
    public static void main(String[] args) {
        System.out.println(superDigit("123", 3));
    }

    public static int superDigit(String n, int k) {
        if (n.length() == 1) {
            return Integer.parseInt(n);
        }
        return findSuperDigit(String.valueOf((long)n.chars().map(i -> i - '0').sum() * k));
    }

    public static int findSuperDigit(String number) {
        if (number.length() == 1) {
            return Integer.parseInt(number);
        }
        long sum = number.chars().map(i -> i - '0').sum();
        return findSuperDigit(String.valueOf(sum));
    }
}
