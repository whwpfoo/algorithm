
public class HackerRankInAString {
    public static void main(String[] args) {
        String letter = "hhhssshhaaaadsadcccckkkasdaskkasdeeeesadasrrrraaaanasdasnnnnasdsadkkk";
        System.out.println(hackerrankInString(letter));
    }

    public static String hackerrankInString(String s) {
        char[] arr = {'h', 'a', 'c', 'k', 'e', 'r', 'r', 'a', 'n', 'k'};
        int index = 0;
        s = s.replaceAll("[^acehknr]", "");

        for (int i = 0; i < s.length() && index < arr.length; i++) {
            if (arr[index] == s.charAt(i)) {
                index++;
            }
        }
        return index == arr.length ? "YES" : "NO";
    }
}
