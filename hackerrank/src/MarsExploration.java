import java.util.stream.IntStream;

public class MarsExploration {
    public static void main(String[] args) {
        String message = "SOSTOSQOQSOS";
        int result = marsExploration(message);
        System.out.println(result);
    }

    public static int marsExploration(String s) {
        char[] letter = new char[]{'S', 'O', 'S'};
        return (int) IntStream
                .range(0, s.length())
                .filter(index -> s.charAt(index) != letter[index % 3])
                .count();
    }
}
