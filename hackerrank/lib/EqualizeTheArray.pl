#!/usr/bin/perl

use strict;
use warnings;

#
# Complete the 'equalizeArray' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY arr as parameter.
#

sub equalizeArray {
	my ($ref) = @_;

	my @temp_array  = (0) x 101;
	my $temp_posi   = -1;


	foreach (0 .. $#$ref) {
		my $pos           = $ref -> [$_];
		my $current_count = $temp_array[$pos];
		my $temp_count    = $temp_array[$temp_posi];

		if ($current_count > 1) {
			$temp_posi = $pos if ($current_count > $temp_count);
			$temp_posi = $pos > $temp_posi ? $pos : $temp_posi if ($current_count == $temp_count);
		}

		$temp_array[$pos]++;
	}

	return $#$ref if ($temp_posi == -1);
	return $#$ref + 1 - $temp_array[$temp_posi];
}
