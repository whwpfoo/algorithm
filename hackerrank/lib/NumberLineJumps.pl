#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';

# https://www.hackerrank.com/challenges/kangaroo/problem?h_r=next-challenge&h_v=zen
# Complete the kangaroo function below.
#

sub kangaroo {
    my ($first, $fMove, $second, $sMove) = @_;

    return "YES"    if ($first == $second);
    return "NO"     if ($fMove <= $sMove);

    while ($first <= $second) {
        $first  += $fMove, $second += $sMove;
        return "YES" if ($first == $second);
    }

    return "NO";
}