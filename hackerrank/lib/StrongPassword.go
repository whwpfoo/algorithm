package main

import (
	"regexp"
)

func minimumNumber(n int32, password string) int32 {
	// Return the minimum number of characters to make the password strong
	requiredPasswordCount := 0
	matchString := []string{"[0-9]", "[a-z]", "[A-Z]", "[!@#$%\\^\\&\\*\\(\\)\\-\\+]"}
	for i := 0; i < len(matchString); i++ {
		isContain, _ := regexp.MatchString(matchString[i], password)
		if isContain == false {
			requiredPasswordCount++
		}
	}

	for len(password)+requiredPasswordCount < 7 {
		requiredPasswordCount++
	}

	return int32(requiredPasswordCount)
}
