use strict;
use warnings;

# Complete the larrysArray function below.
# https://www.hackerrank.com/challenges/larrys-array/problem
sub larrysArray {
    my ($array)    = @_;
    my $last_index = $#$array;

    foreach (0 .. $last_index - 2) {
        my $min_index = find_min_index($array, $_);

        while ($min_index != $_) {
            rotate($array, ($min_index < $last_index) ? $min_index - 1 : $min_index - 2);
            $min_index -= 1;
        }
    }

    my $a = $array -> [$last_index - 1];
    my $b = $array -> [$last_index];

    return $a < $b ? "YES" : "NO";
}

sub rotate {
    my ($array, $index) = @_;
    my $temp = $array -> [$index];

    $array -> [$index]     = $array -> [$index + 1];
    $array -> [$index + 1] = $array -> [$index + 2];
    $array -> [$index + 2] = $temp;
}

sub find_min_index {
    my ($array, $s) = @_;
    my $min_index = $s;
    $min_index = $array -> [$min_index] > $array -> [$_] ? $_ : $min_index foreach ($s .. $#$array);
    return $min_index;
}