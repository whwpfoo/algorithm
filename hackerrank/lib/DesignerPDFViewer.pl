#!/usr/bin/perl

use strict;
use warnings;
use List::Util qw(max);

# https://www.hackerrank.com/challenges/designer-pdf-viewer/problem
# Complete the designerPdfViewer function below.
sub designerPdfViewer {
    my ($arr, $letter) = @_;

    my $result = -1;
    $result = max $result, $arr -> [$_ - 97] foreach unpack "C*", $letter;

    return $result * length $letter;
}
