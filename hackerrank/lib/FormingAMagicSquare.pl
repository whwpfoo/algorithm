use strict;
use warnings;
use Data::Dumper;


sub formingMagicSquare {
    my ($s) = @_;

    my %magic_square = (
        1 => [[8, 1, 6], [3, 5, 7], [4, 9, 2]],
        2 => [[6, 1, 8], [7, 5, 3], [2, 9, 4]],
        3 => [[4, 9, 2], [3, 5, 7], [8, 1, 6]],
        4 => [[2, 9, 4], [7, 5, 3], [6, 1, 8]],
        5 => [[8, 3, 4], [1, 5, 9], [6, 7, 2]],
        6 => [[4, 3, 8], [9, 5, 1], [2, 7, 6]],
        7 => [[6, 7, 2], [1, 5, 9], [8, 3, 4]],
        8 => [[2, 7, 6], [9, 5, 1], [4, 3, 8]]
    );
    
    my $min_value = 9999;

    foreach (keys %magic_square) {
        my $key      = $_;
        my $solution = $magic_square{$key};
        my $sum      = 0;
        foreach (0 .. 2) {
           $sum += abs($solution -> [$_][0] - $s -> [$_][0]);
           $sum += abs($solution -> [$_][1] - $s -> [$_][1]);
           $sum += abs($solution -> [$_][2] - $s -> [$_][2]);
        }
       $min_value = $sum if ($min_value > $sum);
    }

    return $min_value;
}


formingMagicSquare();
exit;
