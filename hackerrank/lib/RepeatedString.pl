use strict;
use warnings;

#
# Complete the 'repeatedString' function below.
#
# The function is expected to return a LONG_INTEGER.
# The function accepts following parameters:
#  1. STRING s
#  2. LONG_INTEGER n
#

sub repeatedString {
    my ($s, $n) = @_;

    my $word_length  = length $s;
    my $a_word_count = length $s =~ s/[^a]//rg;

    if ($word_length < $n) {
        my $repeat = int($n / $word_length);
        my $remain = int($n % $word_length);

        my $remain_word    = substr($s, 0, $remain);
        my $remain_a_count = length $remain_word =~ s/[^a]//rg;

        $a_word_count *= $repeat;
        $a_word_count += $remain_a_count;
    }

    elsif ($word_length > $n) {
        my $substring = substr($s, $n);
        my $substring_a_count = length $substring =~ s/[^a]//rg;
        $a_word_count -= $substring_a_count;
    }

    return $a_word_count;
}

my $result = repeatedString('ababa', 3);

print "$result\n";
