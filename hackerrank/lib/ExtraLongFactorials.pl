use strict;
use warnings;
use List::Util qw(reduce);
use Math::BigInt;

# https://www.hackerrank.com/challenges/extra-long-factorials/problem
# Complete the extraLongFactorials function below.
sub extraLongFactorials {
    my ($number) = @_;
    return reduce { Math::BigInt -> new($a * $b) } (1 .. $number);
}