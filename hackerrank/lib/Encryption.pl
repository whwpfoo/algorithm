#!/usr/bin/perl

use strict;
use warnings;

# https://www.hackerrank.com/challenges/encryption/problem
# Complete the encryption function below.
# 1. input: 공백이 제거된 문자열 s
# 2. constraint: input length 의 루트 값의 2개의 정수
#                정수 사이의 row * col grid 형태의 배열 완성
# 3. 배열의 각 행의 문자열 사이 '공백' 을 기준으로 문자열 리턴
sub encryption {
    my ($string)                    = @_;
    my ($string_length, $grid_size) = (length $string, sqrt length $string);
    my ($row, $col)                 = (int $grid_size);

    # 실수인 경우
    if ($grid_size - $row) {
        $col = $row + 1;
    }
    # 정수인 경우
    else {
        $col = $row;
    }

    $row += 1 if ($row * $col < $string_length);

    my @array = split //, $string;
    my @result = ();

    foreach (0 .. $row - 1) {
        my @temp_array = ();

        foreach (0 .. $col - 1) {
            push @temp_array, shift @array;
        }

        push @result, \@temp_array;
    }

    my $encrypted_letter;

    foreach my $l (0 .. $col - 1) {
        foreach my $r (0 .. $row - 1) {
            if (defined $result[$r] -> [$l]) {
                $encrypted_letter .= $result[$r] -> [$l];
            }
        }

        $encrypted_letter .= " ";
    }

    return $encrypted_letter;
}