use strict;
use warnings;


sub jumpingOnClouds {
    my ($arr, $jump) = @_;


    my $energy = 100;
    my $length = scalar @$arr; 
    my $pos    = 0; 


    while (1) {
        $pos = ($pos + $jump) % $length;  
        $energy -= 1; 
        $energy -= 2 if ($arr -> [$pos] == 1);
        last if ($pos == 0);     
    }


    return $energy;
}
