#!/usr/bin/perl

use strict;
use warnings;

# https://www.hackerrank.com/challenges/counting-valleys/problem
#
# Complete the 'countingValleys' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER steps
#  2. STRING path
#

sub countingValleys {
    my ($n, $p) = @_;

    my @paths = split //, $p;
    my ($c, $result) = (0, 0);

    foreach my $i (@paths) {
        if ($i eq 'U') {
            $result++ if (++$c == 0);
        }

        else {
            --$c;
        }
    }

    return $result;
}
