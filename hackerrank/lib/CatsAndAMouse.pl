#!/usr/bin/perl
use strict;
use warnings;

# https://www.hackerrank.com/challenges/cats-and-a-mouse/problem
# Complete the catAndMouse function below.
sub catAndMouse {
    my ($pos_cat_a, $pos_cat_b, $mouse) = @_;
    my $pos = (abs $pos_cat_a - $mouse) - (abs $pos_cat_b - $mouse);

    if ($pos == 0) {
        return "Mouse C";
    }

    elsif ($pos > 0) {
        return "Cat B";
    }

    else {
        return "Cat A";
    }
}
