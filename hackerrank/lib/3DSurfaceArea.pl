use strict;
use warnigns;

# https://www.hackerrank.com/challenges/3d-surface-area/problem
# Complete the surfaceArea function below.
sub surfaceArea {
    my ($arr) = @_;
    my ($up_down, $left_right, $front_back) = (0, 0, 0);

    foreach my $row (0 .. $#$arr) {
        foreach my $col (0 .. $#{$arr -> [$row]}) {
            if ($row == 0) {
                $left_right += $arr -> [$row] -> [$col];
                $left_right += $arr -> [$#$arr - $row] -> [$col];
            }

            else {
                my $left_to_right = $arr -> [$row] -> [$col] - $arr -> [$row - 1] -> [$col];
                my $right_to_left = $arr -> [$#$arr - $row] -> [$col] - $arr -> [$#$arr - $row + 1] -> [$col];
                $left_right += $left_to_right if ($left_to_right > 0);
                $left_right += $right_to_left if ($right_to_left > 0);
            }

            if ($col == 0) {
                $front_back += $arr -> [$row] -> [$col];
                $front_back += $arr -> [$row] -> [$#{$arr -> [0]} - $col];
            }

            else {
                my $front_to_back = $arr -> [$row] -> [$col] - $arr -> [$row] -> [$col - 1];
                my $back_to_front = $arr -> [$row] -> [$#{$arr -> [0]} - $col] - $arr -> [$row] -> [$#{$arr -> [0]} - $col + 1];
                $front_back += $front_to_back if ($front_to_back > 0);
                $front_back += $back_to_front if ($back_to_front > 0);
            }
        }
    }

    $up_down = scalar(@$arr) * scalar(@{$arr -> [0]}) * 2;
    return $up_down + $left_right + $front_back;
}
         