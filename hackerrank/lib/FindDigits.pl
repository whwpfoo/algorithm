use strict;
use warnings;

# https://www.hackerrank.com/challenges/find-digits/problem
# Complete the findDigits function below.
sub findDigits {
    my ($digit) = @_;
    
    my @arr = split (//, $digit);
    my $count = grep { $_ && $digit % $_ == 0 } @arr;
    
    return $count;
}





