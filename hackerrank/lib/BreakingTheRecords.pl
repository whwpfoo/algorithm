#!/usr/bin/perl

use strict;
use warnings;

# https://www.hackerrank.com/challenges/breaking-best-and-worst-records/problem
# Complete the breakingRecords function below.
sub breakingRecords {
    my ($games) = @_;

    my $firstScore = shift @$games;
    my ($highScore, $lowScore, $hCount, $lCount) = ($firstScore, $firstScore, 0, 0);

    foreach my $score (@$games) {
        if ($score > $highScore) { $highScore = $score; $hCount++ }
        if ($score < $lowScore) { $lowScore = $score; $lCount++ }
    }

    return $hCount, $lCount;
}