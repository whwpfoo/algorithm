use strict;
use warnings;

# https://www.hackerrank.com/challenges/compare-the-triplets/problem
# Complete the compareTriplets function below.
sub compareTriplets {
    my ($alice, $bob) = @_;
    my @result        = (0, 0);
    my @calc          = map { $alice -> [$_] - $bob -> [$_] } (0 .. 2);
    
    return (scalar(grep { $_ > 0 } (@calc)), scalar(grep { $_ < 0 } (@calc)));
}
