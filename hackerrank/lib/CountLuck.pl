use strict;
use warnings;

# https://www.hackerrank.com/challenges/count-luck/problem
# Complete the countLuck function below.

my $answer = -1;

sub countLuck {
    my ($mat, $k)            = @_;
    my ($row, $col, $result) = ($#$mat, length($mat -> [0]) - 1, 0);
    my @grid                 = ();
    
    foreach (0 .. $row) {
        my @array = split(//, $mat -> [$_]);
        push @grid, \@array;
    }
    
    my ($arr, $start_row, $start_col) = (\@grid, 0, 0);

    root: 
    foreach my $i (0 .. $row) {
        foreach my $j (0 .. $col) {
            my $target = $arr -> [$i][$j];
            if ($target eq 'M') {
                ($start_row, $start_col) = ($i, $j);
                last root;
            }
        }
    }

    dfs($arr, $row, $col, $start_row, $start_col, 0);

    return $answer == $k ? 'Impressed' : 'Oops!';
}

sub dfs {
    my ($mat, $row_length, $col_length, $i, $j, $count) = @_;
    my $direction_count = get_direction_count($mat, $i, $j, $row_length, $col_length);
    
    
    if (is_destination($mat, $i, $j, $row_length, $col_length)) {
        $answer = $direction_count >= 1 ? $count + 1 : $count;
        return;
    }
    

    $mat -> [$i][$j] = 'X';
    $count++ if ($direction_count >= 2);

    
    map  {
            dfs($mat, $row_length, $col_length, $_ -> [0], $_ -> [1], $count);
         }
    grep { 
            $_ -> [0] >= 0 && $_ -> [0] <= $row_length && $_ -> [1] >= 0 && $_ -> [1] <= $col_length 
            && $mat -> [$_ -> [0]][$_ -> [1]] eq '.' 
         } 
         (
            [$i - 1, $j], [$i + 1, $j], [$i, $j - 1], [$i, $j + 1]
         );
}

sub get_direction_count {
    my ($mat, $row, $col, $row_length, $col_length) = @_;
    my $count = 0;
    $count++ if ($col + 1 <= $col_length && $mat -> [$row][$col + 1] eq '.'); 
    $count++ if ($col - 1 >= 0 && $mat -> [$row][$col - 1] eq '.'); 
    $count++ if ($row + 1 <= $row_length && $mat -> [$row + 1][$col] eq '.');
    $count++ if ($row - 1 >= 0 && $mat -> [$row - 1][$col] eq '.');
    return $count;
}

sub is_destination {
    my ($mat, $row, $col, $row_length, $col_length) = @_;
    return 1 if ($col + 1 <= $col_length && $mat -> [$row][$col + 1] eq '*'); 
    return 1 if ($col - 1 >= 0 && $mat -> [$row][$col - 1] eq '*'); 
    return 1 if ($row + 1 <= $row_length && $mat -> [$row + 1][$col] eq '*');
    return 1 if ($row - 1 >= 0 && $mat -> [$row - 1][$col] eq '*');
    return 0;
}


my @array = ();
push @array, "..........*";
push @array, ".XXXXXXXXXX";
push @array, "...........";
push @array, "XXXXXXXXXX.";
push @array, "M..........";

#print countLuck(\@array, 0);

my @array2 = ();
push @array2, "*.M";
push @array2, ".X.";

#print countLuck(\@array2, 1);


my @array3 = ();
push @array3, "*..";
push @array3, "X.X";
push @array3, "..M";

print countLuck(\@array3, 1);


