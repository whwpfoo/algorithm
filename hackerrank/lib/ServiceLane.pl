use strict;
use warnings;

# https://www.hackerrank.com/challenges/service-lane/problem
sub serviceLane {
    # Write your code here
    my ($s, $c, $w) = @_;
    my @result      = ();
    foreach (0 .. $#$c) {
        my $min_value = 9999999;
        $min_value = ($min_value > $w -> [$_]) ? $w -> [$_] : $min_value foreach ($c -> [$_][0] .. $c -> [$_][1]);
        push @result, $min_value;
    }
    return @result;
}
