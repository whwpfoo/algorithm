#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';

# https://www.hackerrank.com/challenges/time-conversion/problem
# Complete the timeConversion function below.
sub timeConversion {
    my ($timeTo12Hour) = @_;

    my @times = split /:/, $timeTo12Hour;
    my $type = substr $times[2], 2, 2, '';

    # AM 인 경우
    $times[0] = 00 if ($type eq 'AM' && $times[0] == 12);

    # PM 인 경우
    $times[0] = $times[0] + 12 if ($type eq 'PM' && $times[0] < 12);

    return join ':', @times;
}
