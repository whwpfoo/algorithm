package main

import (
	"sort"
)

func missingNumbers(arr []int32, brr []int32) []int32 {
	arrayMap := make(map[int32]int)

	for _, key := range arr {
		value, exist := arrayMap[key]
		if exist {
			arrayMap[key] = value + 1
		} else {
			arrayMap[key] = 1
		}
	}

	for _, key := range brr {
		value, exist := arrayMap[key]
		if exist {
			arrayMap[key] = value - 1
		} else {
			arrayMap[key] = 1
		}
	}

	var result []int32
	for key, value := range arrayMap {
		if value != 0 {
			result = append(result, key)
		}
	}

	sort.Slice(result, func(i, j int) bool {
		return result[i] < result[j]
	})

	return result
}
