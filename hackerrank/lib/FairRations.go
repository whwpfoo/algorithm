package main

import "strconv"

//1. Every time you give a loaf of bread to some person ,
//   you must also give a loaf of bread to the person immediately in front of or behind them in the line (i.e., persons  or ).
//2. After all the bread is distributed, each person must have an even number of loaves.
//   https://www.hackerrank.com/challenges/fair-rations/problem

func fairRations(B []int32) string {
	var total int32 = 0
	for _, bread := range B {
		total += bread
	}

	if total%2 != 0 {
		return "NO"
	}

	result := 0
	peopleLength := len(B)
	for i := 0; i < peopleLength-1; i++ {
		bread := B[i]
		if bread%2 != 0 {
			B[i] += 1
			B[i+1] += 1
			result += 2
			continue
		}
	}

	return strconv.Itoa(result)
}
