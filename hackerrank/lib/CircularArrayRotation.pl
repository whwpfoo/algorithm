use strict;
use warnings;

sub circularArrayRotation {
    my ($array, $k, $q) = @_;

    my @a = @$array;
    my @q = @$q;

    foreach (0 .. $k - 1) {
        my $last = pop @a;
        unshift @a, $last; 
    }
    
    return map { $a[$q[$_]] } (0 .. $#q); 
}
