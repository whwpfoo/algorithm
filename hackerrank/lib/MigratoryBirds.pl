#!/usr/bin/perl

use strict;
use warnings;
use List::Util qw(max first);

# https://www.hackerrank.com/challenges/migratory-birds/problem
# Complete the migratoryBirds function below.
sub migratoryBirds {
    my ($birds) = @_;
    my @result = (0) x @$birds;         # 초기 '0' 으로 할당

    $result[$_]++ foreach (@$birds);    # birds 카운트를 인덱스로 사용하여 카운팅

    my $maxCount = max @result;         # max 값 추출

    # max 값을 가지고 있는 birds 카운트 추출
    return first { defined $_ } grep { $result[$_] == $maxCount } 0 .. $#result;
}
