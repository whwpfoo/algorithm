use strict;
use warnings;
use 5.010;

# https://www.hackerrank.com/challenges/greedy-florist/problem
# Complete the getMinimumCost function below.
sub getMinimumCost {
     my ($k, $costs)  = @_;
     my @sorted_costs = sort { $a <=> $b } @$costs;
     my $plus         = 0;
     my $result       = 0;
     
     foreach my $index (0 .. $#sorted_costs) {
         $result += ($plus + 1) * $sorted_costs[$#sorted_costs - $index];
         $plus++ if ($index % $k + 1 == $k);
     }
     
     return $result;
}