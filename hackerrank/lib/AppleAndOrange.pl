#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';

# https://www.hackerrank.com/challenges/apple-and-orange/problem
# Complete the countApplesAndOranges function below.
# 사과 나무와 오렌지 나무에서 떨어진 '열매' 의 수를 구한다
# 단, 열매가 떨어진 위치가 집 (start, end) 영역에 해당하는 것만 취한다
sub countApplesAndOranges {
    # $s, $t, $a, $b, \@apples, \@oranges;
    my ($hStart, $hEnd, $appleTree, $orangeTree, $apples, $oranges) = @_;

    my $appleCount = grep { isIncluded($_, $hStart, $hEnd) } map { calcFruitDistance($_, $appleTree) } @$apples;
    my $orangeCount = grep { isIncluded($_, $hStart, $hEnd) } map { calcFruitDistance($_, $orangeTree) } @$oranges;

    print $appleCount, "\n", $orangeCount;
}

sub calcFruitDistance {
    my ($fruit, $tree) = @_;
    return $fruit < 0 ? $tree - abs $fruit : $tree + $fruit;
}

sub isIncluded {
    my ($fruit, $start, $end) = @_;
    return $fruit >= $start && $fruit <= $end;
}