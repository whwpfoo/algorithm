#!/usr/bin/perl
use strict;
use warnings;
use List::Util qw(min);

#
# Complete the getMoneySpent function below.
#
sub getMoneySpent {
    my ($k, $d, $budget) = @_;

    my $min_price_keyboard = min @$k;
    my $min_price_driver   = min @$d;

    return -1 if ($min_price_keyboard + $min_price_driver > $budget);

    my $result = -1;

    foreach my $keyboard (@$k) {
        foreach my $driver (@$d) {
            my $sum = $keyboard + $driver;
            $result = $sum if ($sum <= $budget && $result < $sum);
        }
    }

    return $result;
}