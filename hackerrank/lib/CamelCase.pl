package main;
use strict;
use warnings;

# https://www.hackerrank.com/challenges/camelcase/problem
sub camelcase {
    # Write your code here
    # return: word count
    # upperCase Word Unicode Range: 65 - 90

    my ($letters) = @_;
    my @letter_array = split //, $letters;
    return scalar(grep { ord($_) >= 65 && ord($_) <= 90 } @letter_array) + 1;

    # 정규표현식을 이용한 답
    # return scalar(my @result = $_[0] =~ /[A-Z]/g) + 1;
}

my $test_1 = "saveChangesInTheEditor";
#print camelcase($test_1);
# my @matches = $string =~ /pattern/g
my @result = $test_1 =~ /[A-Z]/g;
print scalar(@result);

