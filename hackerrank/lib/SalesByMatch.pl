#!/usr/bin/perl

use strict;
use warnings;
use List::Util qw(sum);

# https://www.hackerrank.com/challenges/sock-merchant/problem
# Complete the sockMerchant function below.
sub sockMerchant {
    my ($total, $array) = @_;
    my %color = ();

    $color{"$_"}++ foreach (@$array);

    my @filter = map {int $color{"$_"} / 2} keys %color;

    return sum @filter;
}