#!/usr/bin/perl

use strict;
use warnings;

# https://www.hackerrank.com/challenges/divisible-sum-pairs/problem
# Complete the divisibleSumPairs function below.
sub divisibleSumPairs {
    my ($len, $k, $array) = @_;
    my $result = 0;

    foreach my $i (0 .. $len - 1) {
        foreach my $j ($i + 1 .. $len - 1) {
            my $sum = $array -> [$i] + $array -> [$j];
            $result++ if ($sum % $k == 0);
        }
    }

    return $result;
}