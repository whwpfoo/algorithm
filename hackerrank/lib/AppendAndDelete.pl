use strict;
use warnings;

sub appendAndDelete {
    my ($c1, $c2, $op_count) = @_;

    
    my @c1_arr  = split //, $c1;
    my @c2_arr  = split //, $c2;
    my $c1_size = length $c1; 
    my $c2_size = length $c2;


    return "Yes" if ($c1_size + $c2_size <= $op_count);


    my $min_size         = min($c1_size, $c2_size) - 1;
    my $match_word_count = 0;


    foreach (0 .. $min_size) {
        last unless ($c1_arr[$_] eq $c2_arr[$_]);
        $match_word_count++;
    }


    my $rest_size = ($c1_size + $c2_size) -  (2 * $match_word_count);

    return "Yes" if ($op_count >= $rest_size && ($op_count - $rest_size) % 2 == 0); 
    return "No";
}

sub min {
    my ($num1, $num2) = @_;
    return $num1 > $num2 ? $num2 : $num1;
}
