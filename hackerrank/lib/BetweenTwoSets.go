package main

// https://www.hackerrank.com/challenges/between-two-sets/problem
func getTotalX(a []int32, b []int32) int32 {
	lcm := Remove(&a, 0)
	for _, value := range a {
		lcm = GetLCM(lcm, value)
	}

	var array []int32
	size := b[0]

init:
	for i := int32(1); i <= size; i++ {
		for _, value := range b {
			if value%i != 0 {
				continue init
			}
		}
		array = append(array, i)
	}

	count := int32(0)
	for _, value := range array {
		if value%lcm == 0 {
			count++
		}
	}

	return count
}

func Remove(arr *[]int32, index int32) int32 {
	target := (*arr)[0]
	*arr = append((*arr)[:index], (*arr)[index+1:]...)
	return target
}

func GetGCD(a, b int32) int32 {
	for b != 0 {
		a, b = b, a%b
	}
	return a
}

func GetLCM(a, b int32) int32 {
	return a * b / GetGCD(a, b)
}
