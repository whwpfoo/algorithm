use strict;
use warnings;
use List:Util qw(sum);
use 5.010;

# https://www.hackerrank.com/challenges/sherlock-and-anagrams/problem
# Complete the sherlockAndAnagrams function below.

sub sherlockAndAnagrams {
    my ($let)  = @_;
    my $limit  = length($let) - 1;
    my %result = ();
    
    foreach my $size (1 .. $limit) {
        
        foreach my $start (0 .. $limit) {
            my $str = substr($let, $start, $size);
            
            if ($size == 1) {
                $result{$str}++;
            }

            else {
                $result{join "", sort split //, $str}++;
            }

            # next position check
            my $next_pos = $start + 1;
            last if ($next_pos + $size > length($let));
        }
    }
    
    return sum(map { $_ * ($_ - 1) / 2 } values %result);
}