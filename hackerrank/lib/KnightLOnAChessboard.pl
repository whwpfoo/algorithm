use strict;
use warnings;
use Data::Dumper qw(Dumper);

# https://www.hackerrank.com/challenges/knightl-on-chessboard/problem
# Complete the knightlOnAChessboard function below.
sub knightlOnAChessboard {
    my ($n) = @_;

    my $len    = $n - 1;
    my @result = ();
    
    foreach my $row (1 .. $len) {
        my @temp = ();

        foreach my $col (1 .. $len) {
            my $dis = bfs($row, $col, $len);
            push @temp, $dis; 
        }
        
        push @result, \@temp;
    }

    return @result;
}

sub bfs {
    my ($i, $j, $len) = @_;
    
    # init visit array && queue
    my @visited = ();
    my @queue   = ();

    # input start value ($row_pos, $col_pos, $dis)
    push @queue, [0, 0, 0];
    $visited[0][0] = 1;

    while (@queue) {
        my $start   = shift @queue;
        my $cur_row = $start -> [0];
        my $cur_col = $start -> [1];
        my $cur_dis = $start -> [2];

        return $cur_dis if ($cur_row == $len && $cur_col == $len);

        map  { 
                push @queue, [$_ -> [0], $_ -> [1], $cur_dis + 1]; 
                $visited[$_ -> [0]][$_ -> [1]] = 1; 
             } 
        grep { 
                $_ -> [0] <= $len && $_ -> [0] >= 0 && $_ -> [1] <= $len && $_ -> [1] >= 0 && !defined($visited[$_ -> [0]][$_ -> [1]])
             } (
                [$cur_row - $i, $cur_col + $j], [$cur_row - $i, $cur_col - $j],
                [$cur_row + $i, $cur_col + $j], [$cur_row + $i, $cur_col - $j],
                [$cur_row + $j, $cur_col + $i], [$cur_row + $j, $cur_col - $i], 
                [$cur_row - $j, $cur_col - $i], [$cur_row - $j, $cur_col + $i]
              );
    }

    return -1;
}

print Dumper(knightlOnAChessboard(30));