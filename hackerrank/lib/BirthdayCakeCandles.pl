#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';
use List::Util qw/max/;

# https://www.hackerrank.com/challenges/birthday-cake-candles/problem
#
# Complete the 'birthdayCakeCandles' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY candles as parameter.
#

sub birthdayCakeCandles {
    my ($ref) = @_;
    my $tallestCandle = max @$ref;

    return scalar grep { $_ eq $tallestCandle } @$ref;
}

