use strict;
use warnings;

# Complete the saveThePrisoner function below.
sub saveThePrisoner {
    my ($prisoner, $candy, $start) = @_;

    my $answer = ($start + $candy - 1) % $prisoner;

    if ($answer == 0) {
        $answer = $prisoner;
    }

    return $answer;
}