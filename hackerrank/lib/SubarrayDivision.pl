use strict;
use warnings;
use List::Util qw(sum);
use 5.010;

# https://www.hackerrank.com/challenges/the-birthday-bar/problem
# Complete the birthday function below.
sub birthday {
    my ($choco, $day, $month) = @_;
    my $count = 0;

    foreach my $index (0 .. $#$choco - $month + 1) {
        my $len = sum @{$choco}[$index .. $index + $month - 1];
        $count++ if ($len == $day);
    }

    return $count;
}