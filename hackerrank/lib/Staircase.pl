#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';

# https://www.hackerrank.com/challenges/staircase/problem
# Complete the staircase function below.
sub staircase {
    my ($len) = @_;
    print " " x ($len - $_), "#" x $_, "\n" foreach (1 .. $len);
}
