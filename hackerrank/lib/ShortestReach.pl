use strict;
use warnings;
use 5.010;
use Data::Dumper qw(Dumper);

# https://www.hackerrank.com/challenges/bfsshortreach/problem
sub bfs {
    my ($node_count, $edge_count, $edge_array, $start_node) = @_;

    my @graph    = ();
    my @visited  = (0) x $node_count;
    my @distance = (0) x $node_count;

    push @graph, [(0) x $node_count] foreach (0 .. $node_count - 1);

    foreach (@{$edge_array}) {
        $graph[$_ -> [0] - 1][$_ -> [1] - 1] = 1;
        $graph[$_ -> [1] - 1][$_ -> [0] - 1] = 1;
    }

    my @queue = ();
    my $start = $start_node - 1;

    $visited[$start] = 1;
    push @queue, $start;

    while (@queue){
        my $value = shift @queue;

        foreach (0 .. $node_count - 1) {
            if ($graph[$value][$_] == 1 && !$visited[$_]) {
                $visited[$_] = 1;
                push @queue, $_;
                $distance[$_] = $distance[$value] + 6;
            }
        }
    }

    return map { $visited[$_] == 1 ? $distance[$_] : -1 } grep { $_ != $start } (0 .. $node_count - 1);
}