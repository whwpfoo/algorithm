#!/usr/bin/perl

use strict;
use warnings FATAL;

# https://www.hackerrank.com/challenges/drawing-book/problem
#
# Complete the pageCount function below.
#
sub pageCount {
    my ($current, $target) = @_;

    $target     = $target * 2 + ($target % 2 == 0 ? 1 : -1);
    $current    = $current * 2 + ($current % 2 == 0 ? 1 : -1);

    my $back    = int ($current - $target) / 4;
    my $front   = int ($target - 1) / 4;

    return ($back < $front ? $back : $front);
}
