use strict;
use warnings;
use List::Util qw(max min);

# https://www.hackerrank.com/challenges/angry-children/problem
sub maxMin {
    my ($limit, $arr) = @_;
    my @temp          = sort { $a <=> $b } @$arr;
    my $temp_arr_len  = scalar(@temp);
    my $result        = 1000000000;
    
    foreach my $start (0 .. $temp_arr_len - $limit) {
        my @target_arr = @temp[$start, $start + $limit - 1];
        my $target_val = max(@target_arr) - min(@target_arr);
        $result        = min(($result, $target_val));
    }
    
    return $result;
}

