#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';

# https://www.hackerrank.com/challenges/simple-array-sum/problem
sub simpleArraySum {
    my $arr = shift @_;
    my $result = 0;
    foreach my $i (@$arr) {
        $result += $i;
    }
    return $result;
}

my $array = [1,2,3,4,5,6,7,8,9,10];
my $result = simpleArraySum($array);
print $result;