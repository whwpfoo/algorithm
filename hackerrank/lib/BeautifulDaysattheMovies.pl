use strict;
use warnings;

# https://www.hackerrank.com/challenges/beautiful-days-at-the-movies/problem
# Complete the beautifulDays function below.
sub beautifulDays {
    my ($start, $end, $divisor) = @_;
    my $result = 0;
    foreach ($start .. $end) {
        my $num = ($_ - int (reverse ($_))) / $divisor;
        
        next if ($num - int ($num));
        $result++;
    }
    
    return $result;
}
