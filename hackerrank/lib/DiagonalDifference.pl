#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';

# https://www.hackerrank.com/challenges/diagonal-difference/problemsub simpleArraySum {
#
# Complete the 'diagonalDifference' function below.
#
# The function is expected to return an INTEGER.
# The function accepts 2D_INTEGER_ARRAY arr as parameter.
#

sub diagonalDifference {
    my ($array) = @_;
    my $right = 0, my $left = 0, my $start = 0, my $end = scalar @{$array -> [0]} - 1;

    foreach my $itemRef (@$array) {
        $left = $left + $itemRef -> [$start++];
        $right = $right + $itemRef -> [$end--];
    }

    return abs $right - $left;
}