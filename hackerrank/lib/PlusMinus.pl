#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';

# https://www.hackerrank.com/challenges/plus-minus/problem
# Complete the plusMinus function below.
sub plusMinus {
    # 양수 갯수 / 전체길이, 음수 갯수 / 전체 길이, 0 갯수 전체길이 출력
    my ($array) = @_;
    my @sortArr = sort { $a <=> $b } @$array;

    printf "%.6f\n", (int scalar grep { $_ > 0 } @sortArr) / (int scalar @sortArr);
    printf "%.6f\n", (int scalar grep { $_ < 0 } @sortArr) / (int scalar @sortArr);
    printf "%.6f\n", (int scalar grep { $_ == 0 } @sortArr) / (int scalar @sortArr);
}