#!/usr/bin/perl

use strict;
use warnings;

# https://www.hackerrank.com/challenges/bon-appetit/problem
# Complete the bonAppetit function below.
sub bonAppetit {
    my ($bill, $k, $b) = @_;
    my ($total, $bactual) = (0, 0);

    $total += int $_ foreach (@$bill);
    $bactual = int ($total - $bill -> [$k]) / 2;

    if ($bactual == $b) {
        print "Bon Appetit";
    }

    else {
        print $b - $bactual;
    }
}
