#!/usr/bin/perl

use strict;
use warnings;
use List::Util qw(max);

# https://www.hackerrank.com/challenges/the-hurdle-race/problem
# Complete the hurdleRace function below.
sub hurdleRace {
    my ($can, $height_ref) = @_;

    my $max_height = max @$height_ref;

    if ($can >= $max_height) {
        return 0;
    }

    else {
        return $max_height - $can;
    }
}