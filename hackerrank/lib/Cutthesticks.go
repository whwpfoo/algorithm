package main

import (
	"fmt"
	"sort"
)

// https://www.hackerrank.com/challenges/cut-the-sticks/problem
/*
 * Complete the 'cutTheSticks' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

func cutTheSticks(arr []int32) []int32 {
	// Write your code here
	result := make([]int32, 0)

	sort.Slice(arr, func(i, j int) bool {
		return arr[i] < arr[j]
	})

	for _, value := range arr {
		if value == 0 {
			continue
		}

		count := 0
		for i := 0; i < len(arr); i++ {
			if arr[i] <= 0 {
				continue
			}
			arr[i] -= value
			count += 1
		}

		result = append(result, int32(count))
	}

	return result
}

func main() {
	result := cutTheSticks([]int32{5, 4, 4, 2, 2, 8})
	fmt.Println(result)
}
