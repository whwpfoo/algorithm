use strict;
use warnings;
use Data::Dumper qw(Dumper);

# https://www.hackerrank.com/challenges/connected-cell-in-a-grid/problem
# Complete the connectedCell function below.
sub connectedCell {
    my ($grid) = @_;
    my result_count = 0;

    foreach my $row (0 .. $#$gird) {
        foreach my $col (0 .. $#{$grid -> [0]}) {
            if ($grid -> [$row][$col]) {
                my region = find_cell($grid, $row, $col);
                result_count = result_count > region ? result_count : region;
            } 
        }
    }
}

sub find_cell {
    my ($m, $r, $c) = @_;

    return 0 if ($r < 0 || $c < 0 || $r > $#$m || $c > $#{$m -> [0]} || $m -> [$r][$c] == 0);
    
    my $count = 1;
    $m -> [$r][$c] = 0;

    foreach my $i ($r - 1 .. $r + 1) {
        foreach my $j ($c - 1 .. $c + 1) {
            $count += find_cell($m, $i, $j) if ($i != $r || $j != $c);
        }
    }

    return $count;
}