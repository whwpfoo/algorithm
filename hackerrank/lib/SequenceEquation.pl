use strict;
use warnings;

# Complete the permutationEquation function below.
sub permutationEquation {
    my ($seq)  = @_;
    my $hash   = {};
    my @answer = ();

    # key: target / value: index
    $hash -> {$seq -> [$_]} = $_ + 1 foreach (0 .. $#$seq);

    # n -> 1, 2, 3, ... n
    push(@answer, $hash -> {$hash -> {$_}}) foreach (1 .. @$seq);
    
    return @answer;
}