#!/usr/bin/perl

use strict;
use warnings;
use List::Util qw(max);

# https://www.hackerrank.com/challenges/picking-numbers/problem
#
# Complete the 'pickingNumbers' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY a as parameter.
#

sub pickingNumbers {
    # 요소간의 거리가 <= 1 인 가장 긴 배열 길이 리턴
    my ($array_ref) = @_;
    my %array_map   = ();

    $array_map{"$_"}++ foreach (@$array_ref);

    my $result = 0;

    foreach my $key (sort { $a <=> $b } keys %array_map) {
        if (defined $array_map{$key + 1}) {
            $result = max $result, $array_map{$key} + $array_map{$key + 1};
        }

        else {
            $result = max $result, $array_map{$key};
        }
    }

    return $result;
}
