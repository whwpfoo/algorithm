#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';

# https://www.hackerrank.com/challenges/grading/problem
#
# Complete the 'gradingStudents' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts INTEGER_ARRAY grades as parameter.
#
sub gradingStudents {
    my ($grades) = @_;

    my @adjustGrades = map { (int ($_ / 5) + 1) * 5 } @$grades;
    my @result = ();

    foreach my $i (0 .. $#adjustGrades) {
        if ($adjustGrades[$i] < 40) {
            push @result, $grades -> [$i];
            next;
        }

        my $sub = $adjustGrades[$i] - $grades -> [$i];

        if ($sub < 3) {
            push @result, $adjustGrades[$i];
        }

        else {
            push @result, $grades -> [$i];
        }
    }

    return @result;
}