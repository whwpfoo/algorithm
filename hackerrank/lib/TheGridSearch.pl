#!/usr/bin/perl

use strict;
use warnings;

# https://www.hackerrank.com/challenges/the-grid-search/problem
# Complete the gridSearch function below.
# 처음 일치하는 패턴의 index와 string length 가지고 판별
# 단, 일치하는 index 는 여러 개 있을 수 있으니 각각의 index 마다 판별해야 함
sub gridSearch {
    my ($G_ref, $P_ref) = @_;
    my $start_string = shift @$P_ref;

    _main:
    foreach my $source_index (0 .. scalar @$G_ref - 1) {
        my $source_string = $G_ref -> [$source_index];

        # 최초 일치 시작점
        if ($source_string =~ /$start_string/) {
            _sub:
            foreach my $offset (0 .. length $source_string - length $start_string - 1) {
                # 최초 일치 인덱스 부터 offset 을 늘려가며 검사 진행
                my $temp_index = $source_index + 1;
                my $pos = index $source_string, $start_string, $offset;

                next _main if ($pos == -1 || !defined $G_ref -> [$temp_index]);

                foreach my $target (@$P_ref) {
                    my $temp_source_string = substr $G_ref -> [$temp_index++], $pos, length $target;
                    next _sub if ($temp_source_string ne $target);
                }

                return "YES";
            }
        }
    }

    return "NO";
}
