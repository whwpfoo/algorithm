use strict;
use warnings;

# https://www.hackerrank.com/challenges/stockmax/problem
# Complete the 'stockmax' function below.
#
# The function is expected to return a LONG_INTEGER.
# The function accepts INTEGER_ARRAY prices as parameter.
#

sub stockmax {
    my ($prices) = @_;
    
    my $max_price = pop(@$prices);
    my $len_price = scalar(@$prices);
    my $result    = 0;

    foreach (reverse 0 .. $len_price - 1) {
       if ($max_price > $prices -> [$_]) {
           $result += $max_price - $prices -> [$_];
       } 
       
       else {
           $max_price = $prices -> [$_];
       } 
    }

    return $result;
}
