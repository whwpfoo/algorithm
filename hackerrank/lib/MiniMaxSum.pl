#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';
use List::Util qw(sum);

# https://www.hackerrank.com/challenges/mini-max-sum/problem
# Complete the miniMaxSum function below.
sub miniMaxSum {
    my ($numbers) = @_;
    my @sortedNumbers = sort { $a <=> $b } @$numbers;
    my $sum = sum @sortedNumbers;

    print $sum - pop(@sortedNumbers), " ", $sum  - shift(@sortedNumbers);
}
