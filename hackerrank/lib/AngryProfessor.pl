use strict;
use warnings;

# https://www.hackerrank.com/challenges/angry-professor/problem
# Complete the angryProfessor function below.
sub angryProfessor {
    my ($limit, $students) = @_;
    my $result = grep { $_ <= 0 } (@$students);
    
    if ($result >= $limit) {
        return "NO";
    }
    
    else {
        return "YES";
    }
}