package main

// https://www.hackerrank.com/challenges/strange-advertising/problem
func viralAdvertising(n int32) int32 {
	var peoples, share, result int32 = 5, 0, 0
	for i := int32(0); i < n; i++ {
		share += peoples / 2
		peoples = share * 3
		result += share
	}
	return result
}