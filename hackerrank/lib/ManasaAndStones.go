package main

import "math"

/*
 * Complete the 'stones' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER n
 *  2. INTEGER a
 *  3. INTEGER b
 */

func stones(n int32, a int32, b int32) []int32 {
	if n == 1 {
		return []int32{0}
	}

	maxNumber := (n - 1) * int32(math.Max(float64(a), float64(b)))
	minNumber := (n - 1) * int32(math.Min(float64(a), float64(b)))
	var increase int32

	if maxNuber == minNumber {
		increase = a
	} else {
		increase = int32(math.Abs(float64(a - b)))
	}

	var result []int32

	for i := minNumber; i <= maxNumber; i += increase {
		result = append(result, i)
	}

	return result
}
