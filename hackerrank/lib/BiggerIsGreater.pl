use strict;
use warnings;
use 5.010;

# Complete the biggerIsGreater function below.
sub biggerIsGreater {
    my ($letter) = @_;


    return "no answer" if (length $letter == 1);
    

    my @letters    = split //, $letter;
    my $target_pos = -1;
    

    foreach (0 .. $#letters) {
        my $idx = $#letters - $_; # last .. first


        if (ord $letters[$idx] > ord $letters[$idx - 1]) {
            $target_pos = $idx - 1;
            last;
        }
    }


    return "no answer" if ($target_pos == -1);

    
    my $min_value = "z";
    my $min_index = 1000;

    
    foreach my $idx ($target_pos + 1 .. $#letters) {
        if (ord $letters[$target_pos] < ord $letters[$idx] && ord $letters[$idx] <= ord $min_value) {
            ($min_value, $min_index) = ($letters[$idx], $idx);
        }
    }


    @letters[$target_pos, $min_index] = @letters[$min_index, $target_pos];
    @letters[$target_pos + 1 .. $#letters] = sort @letters[$target_pos + 1 .. $#letters];


    return join "", @letters;
}


