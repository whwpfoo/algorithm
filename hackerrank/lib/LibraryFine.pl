use strict;
use warnings;

# 반납 예정일 보다
# 반납 일이 늦는 경우 위약금 지불
# > 같은 달 일자가 늦은 경우-> (일자 차이) x 15
# > 같은 년 달이 늦은 경우  -> (달 차이) x 500
# > 해가 다른 경우          -> 10000 지불
# 반납 예정일과 반납일이 같은 경우 -> 0

#
# Complete the 'libraryFine' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER d1
#  2. INTEGER m1
#  3. INTEGER y1
#  4. INTEGER d2
#  5. INTEGER m2
#  6. INTEGER y2
#

sub libraryFine {
    my ($day1, $month1, $year1, $day2, $month2, $year2) = @_;
    
    if ($year1 > $year2) {
       return 10000; 
    }

    elsif ($year1 == $year2) {
        return 0 if ($month2 > $month1);
        return ($month1 - $month2) * 500 if ($month2 < $month1);
        return 0 if ($day2 >= $day1); 
        return ($day1 - $day2) * 15;
    }
    
    return 0;  
}
